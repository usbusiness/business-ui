var userId = 1;
$(document).ready(function(){
	var data={keywoards:[{id:1,keyword:"MotorCycle",radius:5,message:"I was sell my motorcycle for $200"},
	                     //{id:2,keyword:"MotorCycle",radius:6,message:"I was sell my motorcycle for $250"},
	                     {id:2,keyword:"Mango",radius:3,message:"I was seeling mango $5 per kg."},
	                     //{id:2,keyword:"Mango",radius:4,message:"I was seeling mango $5 per kg."},
	                     {id:2,keyword:"Plumber",radius:3,message:"I am doing plumber related work for $10 per Hour."}]};
	getKeywordData();
	getMessageData();
	
	$('#submitKeyword').bind('click',function(){
		keywordSubmit();
		return false;
	});
	$('#messageUp').bind('click',function(){
		msgToogleUpShowHide(this);
	});
	$('#messageDown').bind('click',function(){
		msgToogleDownShowHide(this);
	});
});

function msgToogleUpShowHide(element){
	var curClass = $(element).attr('class');
	if(curClass == 'btn btn-sm btn-primary'){
		$(element).removeClass('btn-primary');
		$('[rel="messageUp"]').hide();
	}else{
		$(element).addClass('btn-primary');
		$('[rel="messageUp"]').show()
	}
		
}

function msgToogleDownShowHide(element){
	var curClass = $(element).attr('class');
	if(curClass == 'btn btn-sm btn-primary'){
		$(element).removeClass('btn-primary');
		$('[rel="messageDown"]').hide();
	}else{
		$(element).addClass('btn-primary');
		$('[rel="messageDown"]').show()
	}
}

function keywordSubmit(){
	if(validateAddKeyword()){
		var url = "http://localhost:8080/Business-core/send/keyword/add";
		$.ajax({
			url: url,
			contentType: "application/json; charset=utf-8",
			method: "POST",
			data: JSON.stringify(getJsonData()),
			dataType: 'json',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			//cache: false,
			success: function(data) {
				keywordSaveSuccessMsg(data)
			},
			error: function(data) {
				alert("fails"+data);
			},
			failure: function(errMsg) {
		        alert(errMsg);
		    }
		});
		
	}else{
		validationErrorMsg();
	}
}

function getJsonData(){
	
	var data = {};
	
	data['keyword'] = $('#keyword').val();
	data['kilometer'] = $('#radius').val();
	data['message'] = $('#message').val();
	
	return data;
}

function validateAddKeyword(){
	var valid = true;
	if(valid && ($('#keyword').val() == '' || $('#keyword').val() == undefined))
		valid = false;
	if(valid && ($('#radius').val() == '' || $('#radius').val() == undefined))
		valid = false;
	if(valid && $('#message').val() == '' || $('#message').val() == undefined)
		valid = false;
	
	return valid;	
		
}

function validationErrorMsg(){
	bootbox.alert("Please Enter the All fields");
}

function keywordSaveSuccessMsg(data){
	
	if(data>0){
		$('#keyword').val('');
		$('#radius').val('');
		$('#message').val('');
		bootbox.alert("Your Keywords is received by -"+data+" Users");
	}
	else
		bootbox.alert("Your Keywords is not received any users");
}

function getKeywordData(){
	//var url = "ajaxResponse/getTextkeywordsData.js";
	var url = "http://localhost:8080/Business-core/keyword/list";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: 123,
		success: function(data) {
			buildKeywoards(data)
		},
		error: function(data) {
			alert("fails"+data);
		},
		failure: function(errMsg) {
	        alert(errMsg);
	    }
	});
}

function getMessageData(){
	//var url = "ajaxResponse/getTextkeywordsData.js";
	var url = "http://localhost:8080/Business-core/msg/list";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: 123,
		success: function(data) {
			buildMessages(data)
		},
		error: function(data) {
			alert("fails"+data);
		},
		failure: function(errMsg) {
	        alert(errMsg);
	    }
	});
}

function buildMessages(data){
	var html = [];
	
	$(data).each(function(index,item){
		var arraows = 'fa fa-angle-double-up';
		var rel = "messageUp";
		if(userId == item.sender.userId){
			arraows = 'fa fa-angle-double-down';
			rel = "messageDown";
		}
		html.push('<div class="panel panel-default" rel="'+rel+'">');
		html.push('		<div class="panel-heading">');
		html.push('			<a href="#faq-3-'+index+'" data-parent="#faq-list-3" data-toggle="collapse" class="accordion-toggle collapsed">');
		html.push('				<i class="ace-icon fa fa-plus smaller-80" data-icon-hide="ace-icon fa fa-minus" data-icon-show="ace-icon fa fa-plus"></i>&nbsp;');
		html.push('				<img src="avatars/avatar.png" class="msg-photo" alt="Alexs Avatar">');
		html.push('				<span class="msg-body">');
		html.push('					<span class="msg-title">');
		html.push('						<span class="blue">'+item.receiver.userName+'</span>');
		html.push('						'+item.keyword.keywordName+' <span class="blue ace-icon '+arraows+' bigger-120"></span>');
		html.push('					</span>');
		html.push('				</span>');
		html.push('			</a>');
		html.push('		</div>');
		html.push('		<div class="panel-collapse collapse" id="faq-3-'+index+'">');
		html.push('			<div class="panel-body">');
		html.push(				item.message);
		html.push('			</div>');
		html.push('		</div>');
		html.push('	</div>');
	});
	
	$('#faq-list-3').html(html.join(''));
}

function buildKeywoards(data){

	var html = [];

	$(data).each(function(index,item){
		html.push('<div class="panel panel-default">');
		html.push('		<div class="panel-heading">');
		html.push('			<a href="#faq-2-'+index+'" data-parent="#faq-list-2" data-toggle="collapse" class="accordion-toggle collapsed">');
		html.push('				<i class="ace-icon fa fa-chevron-right smaller-80" data-icon-hide="ace-icon fa fa-chevron-down align-top" data-icon-show="ace-icon fa fa-chevron-right"></i>&nbsp;');
		html.push('				'+item.keyword.keywordName+'&nbsp;&nbsp;<span class="badge badge-primary">'+item.kiloMeter+'</span><span style="float:right"><i class="glyphicon glyphicon-pencil" keyword='+item.keyword.keywordName+' radius='+item.kiloMeter+' userKeywordId='+item.userKeywordId+' onclick="addKeyword(true,this);"></i>&nbsp;<i class="ace-icon fa fa-trash-o bigger-120 orange" keyword='+item.keyword.keywordName+' userKeywordId='+item.userKeywordId+' onclick="deleteKeyword(this);"></i></div>');
		html.push('			</a>');
		html.push('		</div>');
		html.push('	</div>');
	});
	

	$('#faq-list-2').html(html.join(''));
}

function addKeyword(isEdit,element){
	var body = buildAddKeywordPopup(isEdit,element);
	bootbox.dialog({
	    title: isEdit?"Update Keyword":"Add Keyword",
	    message: body,
	    buttons: {
	        confirm: {
	            label: '<i class="fa fa-check"></i> Add',
	            className: "btn-success",
	            callback: function() {
	            	if(validateAddNewKeyword()){
	            		saveOrUpdateKeyword(isEdit,element);
	            	}
	            }
	        },
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel',
	            className: "btn-default"
	        }
	    },
	    callback: function (result) {
	        console.log('This was logged in the callback: ' + result);
	    }
	    
	});
}

function validateAddNewKeyword(){
	var valid = true;
	if($('#popKeyword').val() == "" || $('#popKeyword').val() == null){
		alert("please Enter the Keyword");
		valid = false;
	}
	if($('#popRadius').val() == "" || $('#popRadius').val() == null){
		alert("please Enter the Radius");
		valid = false;
	}
	return valid;
	
}

function saveOrUpdateKeyword(isEdit,element){
	var data = {};
	if(isEdit)
		data['id'] = $(element).attr('userKeywordId');
	data['keyword'] = $('#popKeyword').val();
	data['kilometer'] = $('#popRadius').val();
	
	//var url = "ajaxResponse/getTextkeywordsData.js";
	var url = "http://localhost:8080/Business-core/keyword/add";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		//cache: false,
		success: function(data) {
			buildKeywoards(data)
		},
		error: function(data) {
			alert("fails"+data);
		},
		failure: function(errMsg) {
	        alert(errMsg);
	    }
	});
}

function buildAddKeywordPopup(isEdit,element){
	
	var keyWord = '';
	var radius = '';
	if(isEdit){
		userKeywordId = $(element).attr('userKeywordId');
		keyWord = $(element).attr('keyword');
		radius = $(element).attr('radius');
	}
	
	var html = [];
	
	html.push("<form class='HorizontalPositionCache'>");
	html.push("<div class='form-group'>");
	html.push("<label class='col-sm-3 control-label no-padding-right' for='form-field-1'> Enter the Keyword </label>");
	html.push("		<div class='col-sm-9'>");
	html.push("			<input type='text' id='popKeyword' value='"+keyWord+"' placeholder='Enter the Keyword' class='col-xs-10 col-sm-10'>");
	html.push("		</div>");
	html.push("</div>");
	html.push("<div class='form-group'>");
	html.push("		<label class='col-sm-3 control-label no-padding-right' for='form-field-1'> Enter the Radius </label>");
	html.push("		<div class='col-sm-9'>");
	html.push("			<input type='text' id='popRadius' value='"+radius+"' placeholder='Radius' class='col-xs-3 col-sm-3'>&nbsp;km");
	html.push("		</div>");
	html.push("</div>");
	html.push("</form>");
	return html.join("");
}

function deleteKeyword(element){
	var body = "Are you sure want delete this keyword";
	bootbox.dialog({
	    title: "Delete Keyword",
	    message: body,
	    buttons: {
	        confirm: {
	            label: '<i class="fa fa-check"></i> Delete',
	            className: "btn-success",
	            callback: function() {
	            	deleteCurrentKeyword(element);
	            }
	        },
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel',
	            className: "btn-default"
	        }
	    },
	    callback: function (result) {
	        console.log('This was logged in the callback: ' + result);
	    }
	    
	});
}

function deleteCurrentKeyword(element){
	var data = {};
	data['id'] = $(element).attr('userKeywordId');
	data['keyword'] = $('#popKeyword').val();
	
	var url = "http://localhost:8080/Business-core/keyword/delete";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			buildKeywoards(data)
		},
		error: function(data) {
			alert("fails"+data);
		},
		failure: function(errMsg) {
	        alert(errMsg);
	    }
	});
}