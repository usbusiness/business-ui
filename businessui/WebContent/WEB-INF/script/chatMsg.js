/**
 * char msg
 */
function inChatSendMsg(){
	if($.trim($('#inChatText').val()) != "") 
		sendCanversationMsg(true);
}

function outChatSendMsg(){
	if($.trim($('#outChatText').val()) != "") 
		sendCanversationMsg(false);
}

function callDirectChatAccess(element,inChat){
	startScreenStep(element,inChat,true,true,true);
	showHideOutHistoryPage();
	
	//direct call hide the home page content
	$('#outHeaderFirst').hide();
	$('[rel="profileIcon"]').hide();
	$('[rel="inKeywordName"]').html($(element).attr('keywordName'));
	$('#chatOutBackButton').attr('onClick','showOutHomeScreen()');
}

function showOutHomeScreen(){
	$('#outHeader').click();
}

function startScreenStep(element,inChat,directChat,callFromHome,initialCall){
	
	//setting hidden values in message master id
	if(inChat){
		$('#inChatText').attr('messageMasterId',$(element).attr('messageMasterId'));
		$('#inChatText').attr('keywordName',$(element).attr('keywordName'));
		$('#inChatText').attr('userName',$(element).attr('userName'));
		
		$('[rel="inKeywordName"]').html($(element).attr('keywordName')+" - "+$(element).attr('userName'));
		//$('.footer-inner').addClass('instyle');
		if(directChat)
			$('#chatInsertedTimeForIn').val($(element).attr('insertDate'));
	}
	else{
		$('#outChatText').attr('messageMasterId',$(element).attr('messageMasterId'));
		$('#outChatText').attr('keywordName',$(element).attr('keywordName'));
		$('#outChatText').attr('userName',$(element).attr('userName'));
		
		$('[rel="outChatKeywordName"]').html($(element).attr('keywordName')+" - "+$(element).attr('userName'))
		//$('.footer-inner').addClass('outstyle');
		if(directChat)
			$('#chatInsertedTimeForOut').val($(element).attr('insertDate'));
	}
	
	var url = "";
	if(callLocalData)
		url = "JsonData/inChatData.js";
	else
		url = domainUrl+"/Business-core/message/getConversations";
	
	var data = {};
	data['userId'] = userId;
	data['messageMasterId'] = $(element).attr('messageMasterId');
	

	 $.ajax({
		 url: url, 
		 
		 contentType: "application/json; charset=utf-8",
			method: "POST",
			data:JSON.stringify(data),
			dataType: 'json',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			success: function(data) {
				if(data.status){
					conversationList(data.conversations,inChat,directChat,callFromHome,initialCall);
				}
				else
					console.log("No conversation yet.");
			},
			error: function(data) {
				console.log("fails"+data);
			},
			failure: function(errMsg) {
		        console.log(errMsg);
		    }
	 });
	 
	var data = "";
	
	//refresh initial screen load event
	loadRefreshData();
}

function conversationList(data,inChat,directChat,callFromHome,initialCall){
	 
	var html = [];
	var imageId = "chatImageDiv"
	var style = '';
	var insertedTime = '';
	if(inChat){
		style = 'inChatSendStyle';
		insertedTime = $('#chatInsertedTimeForIn').val();
	}
	else{
		style = 'inChatSendStyle';//outChatSendStyle';
		insertedTime = $('#chatInsertedTimeForOut').val();
	}
	//html.push('<div style="padding:1%"></div>');
	
	if(data.length>0 && initialCall){
		var htmlTime = [];
		if(callFromHome)
			htmlTime.push('<div><center>'+getKeywordMonthAndDate(insertedTime)+'</center></div>');
		else
			htmlTime.push('<div><center>'+getKeywordMonthAndDate(parseInt(insertedTime))+'</center></div>');
	}
	
	$(data).each(function(index,item){

		var userName = item.userName

		if(item.userName)
		{
			userName = jsUcfirst(userName);
		}

		//$('[rel="inKeywordUserName"]').html(''+userName+'');
		
		if(index == 0){
			html.push('<div class="form-group col-xs-12 chatInitialMsgStyle">');
			if(item.fileName != null && item.fileName !=""){
				html.push('		<div class="form-group col-xs-4">');
				html.push('			<img src="'+item.fileName+'" class="ChatImageStyle" onclick="openImageZoomPopup(this)"/>');
				html.push('		</div>');
			}
			html.push('		<div class="form-group col-xs-8">');
			html.push('			<div style="font-size: small;">'+item.content+'</div>');
			html.push('		</div>');
			html.push('</div>');
		}else{
			html.push('<div class="form-group col-xs-12">');
			if(userId == item.userId){
				html.push('<div class="col-xs-8 '+style+' pull-right">'+item.content+'</div>');
			}else{
				html.push('<div class="col-xs-7 inChatReceiveStyle pull-left">'+item.content+'</div>');
			}
			html.push('</div>');
		}
	});
	if(inChat){
		//$('[rel="chatScreenStep2"]').html(html.join(''));
		$('#chatScreenStep2Content').html(html.join(''));
		if(initialCall)
			$('#chatScreenStep2Time').html(htmlTime.join(''));
		$('[rel="chatScreenStep1"]').hide();
		$('[rel="chatScreenStep2"]').show();
		$('[rel="chatscreeninputDic"]').show();
		$("#inHeaderThird").hide();
		$("#inHeaderFourth").show();
		getReceivingMessageInChat();
	}
	else{
		//$('[rel="chatScreenOutStep2"]').html(html.join(''));
		$('#chatScreenOutStep2Content').html(html.join(''));
		if(initialCall)
			$('#chatScreenOutStep2Time').html(htmlTime.join(''));
		
		$('[rel="chatScreenOutStep1"]').hide();
		$('[rel="chatScreenOutStep2"]').show();
		$('[rel="chatscreeninputOutDic"]').show();
		$('#addNewKeywordOut').hide();
		$("#outHeaderThird").hide();
		$("#outHeaderFourth").show();
		getReceivingMessageOutChat();
	}
	
	toBottom();
}

function toBottom()
{
	$('.bodyCenterContent').scrollTop( $(document).height() );
}

function getReceivingMessageInChat(){
	/* later */
	clearInterval(refreshIntervalId);
	if($('[rel="chatScreenStep2"]').is(':visible')){
		refreshIntervalId = window.setInterval(function(){
				startScreenStep($('#inChatText'),true,false,false,false);
			}, 5000);
	}else{
		clearInterval(refreshIntervalId);
	}
}

function getReceivingMessageOutChat(){
	/* later */
	clearInterval(refreshIntervalOutId);
	if($('[rel="chatScreenOutStep2"]').is(':visible')){
		refreshIntervalOutId = window.setInterval(function(){
				startScreenStep($('#outChatText'),false,false,false,false);
			}, 5000);
	}else{
		clearInterval(refreshIntervalOutId);
	}
}