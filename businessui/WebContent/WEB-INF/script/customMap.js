$(document).ready(function(){
	map = new OpenLayers.Map("mapdiv");
	map.addLayer(new OpenLayers.Layer.OSM());
	epsg4326 = new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
	projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)

	var lonLat = new OpenLayers.LonLat(80.2312667 , 12.928285).transform(epsg4326, projectTo);

	var zoom = 15;
	map.setCenter(lonLat, zoom);

	var vectorLayer = new OpenLayers.Layer.Vector("Overlay");

	var point = new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);


	var mycircle = OpenLayers.Geometry.Polygon.createRegularPolygon
	(
	    point,
	    1000,
	    40,
	    0
	);

	var featurecircle = new OpenLayers.Feature.Vector(mycircle);

	var featurePoint = new OpenLayers.Feature.Vector(
	    point,
	    { description: 'info' },
	    { externalGraphic: 'img/marker.png', graphicHeight: 25, graphicWidth: 21, graphicXOffset: -12, graphicYOffset: -25 }
	);
	vectorLayer.addFeatures([featurePoint, featurecircle]);

	map.addLayer(vectorLayer);
});