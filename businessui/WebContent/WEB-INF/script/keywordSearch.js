$(function() {

	$( "#outKeyword" ).autocomplete({
    	source: function(request, response){
    		var data = {};
    		data['keyword'] = $('#outKeyword').val();
    		data['kilometer'] = $('#milesOut').val();
    		data['userId'] = userId;
    		if($('#outKeyword').val().length > 1){
	            $.ajax({
	                type: "POST",
	                url: domainUrl+"/Business-core/keyword/searchKeyword",
	                data: JSON.stringify(data),
	                contentType: "application/json; charset=utf-8",
	                dataType: "json",
	                success: function (items) {
	                	if(items.length == 0){
	                		var html = "";
	                		if($('#milesOut').val() <30)
	    						html = "There are no tags for  '"+$('#outKeyword').val()+"' in your selected area, Please try a different tag or increase your radius";
	    					else
	    						html = "There are no tags for  '"+$('#outKeyword').val()+"' in your selected area, Please try a different tag";
	    				
	                		$('#countMatchingMsg').html(html);
	                	}
	                	response( items );
	                },
	                error: function (msg) {
	                    alert(msg.status + ' ' + msg.statusText);
	                }
	            })
    		}
        },
        minLength: 0,
        open: function() {},
        close: function() {},
        focus: function(event,ui) {},
        select: function (event, ui) {
            $("#outKeyword").val(ui.item.value);
            onChangeEventOutMiles(this);
            return false;
        }
    });
 });

//get matching count 
function onChangeEventOutMiles(elem,isEdit){
	
	if(!isEdit)
		loadMap(lang, lat,"mapdivOut",$('#milesOut').val());
	if(($('#outKeyword').val() != "" && $('#milesOut').val() != "") || isEdit){
		var url = domainUrl+"/Business-core/keyword/matchCount/addOut";
		$.ajax({
			url: url,
			contentType: "application/json; charset=utf-8",
			method: "POST",
			data: JSON.stringify(getJsonData(isEdit,elem)),
			dataType: 'json',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			//cache: false,
			success: function(data) {
				var html = "";
				var miles = "";
				if(!isEdit){
					miles = $('#milesOut').val();
				}else{
					miles = $('#milesHeaderOut').val();
				}
				if(data == 0){
					if(miles <30)
						html = "There are no tags for  '"+$('#outKeyword').val()+"' in your selected area, Please try a different tag or increase your radius";
					else
						html = "There are no tags for  '"+$('#outKeyword').val()+"' in your selected area, Please try a different tag";
				}
				else if(data == 1)
					html = "Your Keyword is matching with "+data+" User";
				else if(data > 1)
					html = "Your Keyword is matching with "+data+" Users";
				
				if(!isEdit)
					$('#countMatchingMsg').html(html);
				else
					$('#countMatchingMsgHeader').html(html);
			},
			error: function(data) {
				console.log("fails"+data);
			},
			failure: function(errMsg) {
		        console.log(errMsg);
		    }
		});
	}
}

function getJsonData(isEdit,element){
	
	var data = {};
	
	if(isEdit){
		data['keyword'] = $('#milesHeaderOut').attr('userkeyname');
		data['userOutKeywordId'] = $(element).attr('userkeywordid');
		data['kilometer'] = $('#milesHeaderOut').val();
		data['message'] = $('#milesHeaderOut').attr('keywordDesc');
	}else{
		data['keyword'] = $('#outKeyword').val();
		data['kilometer'] = $('#milesOut').val();
		data['message'] = $('#messageTextarea').val();
	}
	data['userId'] = userId;
	return data;
}

function outKeywoardChangeEvent(element){
	if($('#outKeyword').val().length == 0)
		$('#countMatchingMsg').html('');
}
