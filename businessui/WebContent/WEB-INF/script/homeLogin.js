/**
 * home view initial version 
 */
jQuery(function($) {
	
	$('#loginSubmit').unbind('click').bind('click',function(){
	 	
	 	var data = {};
		data['phoneNumber'] = $('#userName').val();
		data['password'] = $('#password').val();
		
		if($('#userName').val() == '' || $('#password').val() == '')
			bootbox.alert("Please fill the required fields");
		else if($('#userName').val() != '' && $('#password').val() != ''){
		
			var url = domainUrl+"/Business-core/user/login";
			$.ajax({
				url: url,
				contentType: "application/json; charset=utf-8",
				method: "POST",
				data:JSON.stringify(data),
				dataType: 'json',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				//cache: false,
				success: function(data) {
					if(data != '' && data != null){
						userId = data.userId;
						if(data.zipcode != null && data.zipcode != ''){
							lang = data.zipcode.longitude;
							lat = data.zipcode.latitude;
						}else{
							lang = data.longtitude;
							lat = data.latitude;
						}
						//main content
						$('#step2').show();
						//login content
						$('#step1').hide();
						//window.location.href = "main.html";
						loadInitializationApp();
						loadProfileData(data);
						
						localStorage.setItem("userId", userId);
						localStorage.setItem("lang", lang);
						localStorage.setItem("lat", lat);
						localStorage.setItem("userName", data.userName);
						if(data.phoneNumber != null)
							localStorage.setItem("phoneNumber", data.phoneNumber);
						if(data.emailId != null)
							localStorage.setItem("emailId", data.emailId);
						if(zipcode != null && zipcode != ""){
							localStorage.setItem("zipcode", data.zipcode.zipcode);
							localStorage.setItem("country", data.zipcode.county);
						}
					}
				},
				error: function(data) {
					alert("fails"+data);
				},
				failure: function(errMsg) {
			        alert(errMsg);
			    }
			});
		}
	 	
	 	/*if(userName == 'admin' && password == 'password')
	 		window.location.href = "main.html";
	 	*/
	 });
	 
	 if(localStorage.getItem("userId") != null && localStorage.getItem("userId") != ''){
		 userId = localStorage.getItem("userId");
		 lang = localStorage.getItem("lang");
		 lat = localStorage.getItem("lat");
		 var userName = localStorage.getItem("userName");
		 var phoneNumber = localStorage.getItem("phoneNumber");
		 var emailId = localStorage.getItem("emailId");
		 var zipcode = localStorage.getItem("zipcode");
		 var country = localStorage.getItem("country");
		 
		//main content
		$('#step2').show();
		//login content
		$('#step1').hide();
		//window.location.href = "main.html";
		loadInitializationApp();
		var data = [];
		data['userName'] = localStorage.getItem("userName");
		data['phoneNumber'] = (phoneNumber != null ? phoneNumber : '');
		data['emailId'] = (emailId != null ? emailId : '');
		var zipcode = [];
		zipcode['zipcode'] = zipcode;
		zipcode['country'] = country;
		data['zipcode'] = zipcode;
		
		loadProfileData(data);
	 }
	
	$('#signIn').unbind('click').bind('click', function(){
		$('#appHomeScreen').hide();
		$('#sineInScreen').show();
	});
	
	$('#signUp').unbind('click').bind('click', function(){
		$('#appHomeScreen').hide();
		$('#registrationScreen').show();
	});
	
	$('#registerButton').unbind('click').bind('click',function(){
		registrationUser();
	});
	
});
function onClickBackButton(showId,hideId){
	
	$('#'+showId+'').show();
	$('#'+hideId+'').hide();
}

function registrationUser(){
	var data = {};
	//data['emailId'] = $('#regEmailId').val();
	data['phoneNumber'] = $('#regPhoneNumber').val();
	data['password'] = $('#choosePassword').val();
	data['latitude'] = latitude||'';
	data['longtitude'] = longitude||'';
	
	if($('#regUserName').val() == '' || $('#choosePassword').val() == '' || $('#confirmPassword').val() == ''){
		alert("please fill the all fileds");
	}else if($('#choosePassword').val() != $('#confirmPassword').val()){
		alert("Confirm password should be same");
	}else{
	
		var url = domainUrl+"/Business-core/user/signup";
		$.ajax({
			url: url,
			contentType: "application/json; charset=utf-8",
			method: "POST",
			data:JSON.stringify(data),
			dataType: 'json',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			//cache: false,
			success: function(data) {
				if(data)
					onClickBackButton('appHomeScreen','registrationScreen');
			},
			error: function(data) {
				console.log("fails"+data);
			},
			failure: function(errMsg) {
		        console.log(errMsg);
		    }
		});
	}
}