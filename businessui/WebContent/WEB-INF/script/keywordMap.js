
function loadMap(lang,lat,id,radiusMiles){
	
	//$('#mapdivOut').empty();
	//$('#mapdiv').empty();
	
	var myCenter = new google.maps.LatLng(lat,lang);
	var mapCanvas = "";
	if(id == "mapdiv")
		mapCanvas = document.getElementById("mapdiv");
	else
		mapCanvas = document.getElementById("mapdivOut");
	
	var zoom = getZoomData(radiusMiles,id);
	
	var mapOptions = {center: myCenter, zoom: zoom};
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({position:myCenter,icon:"https://png.icons8.com/color/20/person-male.png"});
	marker.setMap(map);
    
	var myCity = new google.maps.Circle({
		center: myCenter,
	    radius: getMeters(radiusMiles),
	    strokeColor: "#ff6666",
	    strokeOpacity: 0.8,
	    strokeWeight: 2,
	    fillColor: "#ff6666",
	    fillOpacity: 0.4
	});
	myCity.setMap(map);
	
	var infowindow = new google.maps.InfoWindow({
      content:"<b>"+radiusMiles+" Mi</b>"
    });
	infowindow.open(map,marker);
	
	google.maps.event.addListener(map, 'zoom_changed', function() {
	    var infowindow = new google.maps.InfoWindow({
	      content:"<b>"+radiusMiles+" Mi</b>"
	    });
	  infowindow.open(map,marker);
	});
	
	/*var arrConversion = [];
    arrConversion['degrees'] = ( 1 / (60 * 1.1508) );
    arrConversion['dd'] = arrConversion['degrees'];
    arrConversion['m'] = ( 1609.344);
    arrConversion['ft'] = ( 5280  );
    arrConversion['km'] = ( 1.609344 );
    arrConversion['mi'] = ( 1 );
    arrConversion['inches'] = ( 63360 );
    
    // need to multiply by sqrt(2)/2 or 1.41421356/2  because
    // were passing in RADIUS and that's a diagonal when drawing the square.  so we have to 
    // adjust by root 2 so we get the actual sides in length that we want

    var measure = radiusMiles;
    var areaSquareMiles =  measure * 0.386102;
    var radius = 0.565352 * Math.sqrt(areaSquareMiles);
    
    map = new OpenLayers.Map(id);
	map.addLayer(new OpenLayers.Layer.OSM());
	epsg4326 = new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
	projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)

	var lonLat = new OpenLayers.LonLat(lang , lat).transform(epsg4326, projectTo);

	//var zoom = 10;
	var zoom = getZoomData(radiusMiles,id);
	map.setCenter(lonLat, zoom);

	var vectorLayer = new OpenLayers.Layer.Vector("Overlay");

	var point = new OpenLayers.Geometry.Point(lonLat.lon, lonLat.lat);


	var mycircle = OpenLayers.Geometry.Polygon.createRegularPolygon
	(
	    point,
	    getMeters(radiusMiles) ,
	    40,
	    0
	);

	var featurecircle = new OpenLayers.Feature.Vector(mycircle);

	var featurePoint = new OpenLayers.Feature.Vector(
	    point,
	    { description: 'info' },
	    { externalGraphic: $('#profileUpdateImgUrl').val(), graphicHeight: 25, graphicWidth: 21, graphicXOffset: -12, graphicYOffset: -25 }
	);
	vectorLayer.addFeatures([featurePoint, featurecircle]);

	map.addLayer(vectorLayer);*/
	
	
	
    
	/*map = new OpenLayers.Map(id);
    map.addLayer(new OpenLayers.Layer.OSM());
       
    var pois = new OpenLayers.Layer.Text( "My Points",
                    { location:textLocation,
                      projection: map.displayProjection
                    });
    map.addLayer(pois);
 // create layer switcher widget in top right corner of map.
    var layer_switcher= new OpenLayers.Control.LayerSwitcher({});
    map.addControl(layer_switcher);
    //Set start centrepoint and zoom    
    var lonLat = new OpenLayers.LonLat(lang, lat)
          .transform(
            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
            map.getProjectionObject() // to Spherical Mercator Projection
          );
    var zoom=13;
    
    var markerLonLat = new OpenLayers.LonLat( 80.1522807 ,12.9359301 )
    .transform(
      new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
      map.getProjectionObject() // to Spherical Mercator Projection
    );
    
    var markers = new OpenLayers.Layer.Markers( "Markers" );
    map.addLayer(markers);
    
    markers.addMarker(new OpenLayers.Marker(markerLonLat));
    
    map.setCenter (lonLat, zoom);*/
}

function getZoomData(radiusMiles,divId){
	var scale= 1.0
	var zoom = 16;
	var zoomOut = 10;
	var setZoomValue = 100;
	var setZoomOutValue = 100;
	if(radiusMiles == "0.1"){
		scale= 2.3;
		zoom = 16;
		setZoomValue = 300;
	}
	else if(radiusMiles == "0.2"){
		scale= 2.3;
		zoom = 15;
		setZoomValue = 150;
	}else if(radiusMiles == "0.3"){
		scale= 2.3;
		zoom = 15;
		setZoomValue = 100;
	}else if(radiusMiles == "0.4"){
		zoom = 15;
		setZoomValue = 150;
	}else if(radiusMiles == "0.5"){
		scale= 1.8;
		zoom = 14;
		setZoomValue = 120;
	}else if(radiusMiles == "0.6"){
		scale= 1.4;
		zoom = 14;
		setZoomValue = 100;
	}else if(radiusMiles == "0.7"){
		scale= 1.4;
		zoom = 14;
		setZoomValue = 170;
	}else if(radiusMiles == "0.8"){
		zoom = 14;
		setZoomValue = 150;
	}else if(radiusMiles == "0.9"){
		zoom = 14;
		setZoomValue = 130;
	}else if(radiusMiles == "1"){
		scale= 1.9;
		zoom = 13;
		setZoomValue = 120;
	}else if(radiusMiles == "1.1"){
		scale= 1.9;
		zoom = 13;
		setZoomValue = 110;
	}else if(radiusMiles == "1.2"){
		scale= 1.5;
		zoom = 13;
		setZoomValue = 100;
	}else if(radiusMiles == "1.3"){
		scale= 1.3;
		zoom = 13;
		setZoomValue = 180;
	}else if(radiusMiles == "1.4"){
		scale= 1.2;
		zoom = 13;
		setZoomValue = 170;
	}else if(radiusMiles == "1.5"){
		scale= 1.1;
		zoom = 13;
		setZoomValue = 150;
	}else if(radiusMiles == "1.6"){
		zoom = 13;
		setZoomValue = 150;
	}else if(radiusMiles == "1.7"){
		zoom = 13;
		setZoomValue = 140;
	}else if(radiusMiles == "1.8"){
		zoom = 13;
		setZoomValue = 130;
	}else if(radiusMiles == "1.9"){
		zoom = 13;
		setZoomValue = 120;
	}else if(radiusMiles == "2"){
		scale= 1.6;
		zoom = 12;
		setZoomValue = 120;
	}else if(radiusMiles == "2.1"){
		scale= 1.6;
		zoom = 12;
		setZoomValue = 110;
	}else if(radiusMiles == "2.2"){
		scale= 1.5;
		zoom = 12;
		setZoomValue = 110;
	}else if(radiusMiles == "2.3"){
		scale= 1.5;
		zoom = 12;
		setZoomValue = 100;
	}else if(radiusMiles == "2.4"){
		scale= 1.5;
		zoom = 12;
		setZoomValue = 100;
	}else if(radiusMiles == "2.5"){
		scale= 1.4;
		zoom = 12;
		setZoomValue = 180;
	}else if(radiusMiles == "2.6"){
		scale= 1.3;
		zoom = 12;
		setZoomValue = 170;
	}else if(radiusMiles == "2.7"){
		scale= 1.3;
		zoom = 12;
		setZoomValue = 170;
	}else if(radiusMiles == "2.8"){
		scale= 1.2;
		zoom = 12;
		setZoomValue = 170;
	}else if(radiusMiles == "2.9"){
		scale= 1.2;
		zoom = 12;
		setZoomValue = 160;
	}else if(radiusMiles == "3"){
		scale= 1.1;
		zoom = 12;
		setZoomValue = 160;
	}else if(radiusMiles == "3.1"){
		scale= 1.1;
		zoom = 12;
		setZoomValue = 150;
	}else if(radiusMiles == "3.2"){
		scale= 1.1;
		zoom = 12;
		setZoomValue = 150;
	}else if(radiusMiles == "3.3"){
		scale= 1.1;
		zoom = 12;
		setZoomValue = 140;
	}else if(radiusMiles == "3.4"){
		zoom = 12;
		setZoomValue = 140;
	}else if(radiusMiles == "3.5"){
		zoom = 12;
		setZoomValue = 130;
	}else if(radiusMiles == "3.6"){
		zoom = 12;
		setZoomValue = 130;
	}else if(radiusMiles == "3.7"){
		zoom = 12;
		setZoomValue = 130;
	}else if(radiusMiles == "3.8"){
		zoom = 12;
		setZoomValue = 120;
	}else if(radiusMiles == "3.9"){
		scale= 1.9;
		zoom = 11;
		setZoomValue = 120;
	}else if(radiusMiles == "4"){
		scale= 1.9;
		zoom = 11;
		setZoomValue = 120;
	}else if(radiusMiles == "4.1"){
		scale= 1.8;
		zoom = 11;
		setZoomValue = 110;
	}else if(radiusMiles == "4.2"){
		scale= 1.7;
		zoom = 11;
		setZoomValue = 110;
	}else if(radiusMiles == "4.3"){
		scale= 1.6;
		zoom = 11;
		setZoomValue = 110;
	}else if(radiusMiles == "4.4"){
		scale= 1.6;
		zoom = 11;
		setZoomValue = 110;
	}else if(radiusMiles == "4.5"){
		scale= 1.5;
		zoom = 11;
		setZoomValue = 100;
	}else if(radiusMiles == "4.6"){
		scale= 1.5;
		zoom = 11;
		setZoomValue = 100;
	}else if(radiusMiles == "4.7"){
		scale= 1.49;
		zoom = 11;
		setZoomValue = 100;
	}else if(radiusMiles == "4.8"){
		scale= 1.45;
		zoom = 11;
		setZoomValue = 100;
	}else if(radiusMiles == "4.9"){
		scale= 1.43;
		zoom = 11;
		setZoomValue = 100;
	}else if(radiusMiles == "5"){
		scale= 1.42;
		zoom = 11;
		setZoomValue = 180;
	}else if(radiusMiles == "5.1"){
		scale= 1.4;
		zoom = 11;
		setZoomValue = 180;
	}else if(radiusMiles == "5.2"){
		scale= 1.4;
		zoom = 11;
		setZoomValue = 180;
	}else if(radiusMiles == "5.3"){
		scale= 1.39;
		zoom = 11;
		setZoomValue = 180;
	}else if(radiusMiles == "5.4"){
		scale= 1.38;
		zoom = 11;
		setZoomValue = 170;
	}else if(radiusMiles == "5.5"){
		scale= 1.3;
		zoom = 11;
		setZoomValue = 170;
	}else if(radiusMiles == "5.6"){
		scale= 1.3;
		zoom = 11;
		setZoomValue = 170;
	}else if(radiusMiles == "5.7"){
		scale= 1.3;
		zoom = 11;
		setZoomValue = 170;
	}else if(radiusMiles == "5.8"){
		scale= 1.28;
		zoom = 11;
		setZoomValue = 160;
	}else if(radiusMiles == "5.9"){
		scale= 1.23;
		zoom = 11;
		setZoomValue = 160;
	}else if(radiusMiles == "6"){
		scale= 1.2;
		zoom = 11;
		setZoomValue = 160;
	}else if(radiusMiles == "6.1"){
		scale= 1.2;
		zoom = 11;
		setZoomValue = 160;
	}else if(radiusMiles == "6.2"){
		scale= 1.18;
		zoom = 11;
		setZoomValue = 150;
	}else if(radiusMiles == "6.3"){
		scale= 1.12;
		zoom = 11;
		setZoomValue = 150;
	}else if(radiusMiles == "6.4"){
		scale= 1.12;
		zoom = 11;
		setZoomValue = 150;
	}else if(radiusMiles == "6.5"){
		scale= 1.1;
		zoom = 11;
		setZoomValue = 150;
	}else if(radiusMiles == "6.6"){
		zoom = 11;
		setZoomValue = 140;
	}else if(radiusMiles == "6.7"){
		zoom = 11;
		setZoomValue = 140;
	}else if(radiusMiles == "6.8"){
		zoom = 11;
		setZoomValue = 140;
	}else if(radiusMiles == "6.9"){
		zoom = 11;
		setZoomValue = 140;
	}else if(radiusMiles == "7"){
		zoom = 11;
		setZoomValue = 140;
	}else if(radiusMiles == "7.1"){
		zoom = 11;
		setZoomValue = 130;
	}else if(radiusMiles == "7.2"){
		zoom = 11;
		setZoomValue = 130;
	}else if(radiusMiles == "7.3"){
		zoom = 11;
		setZoomValue = 130;
	}else if(radiusMiles == "7.4"){
		zoom = 10.5;
		setZoomValue = 130;
	}else if(radiusMiles == "7.5"){
		zoom = 10.5;
		setZoomValue = 130;
	}else if(radiusMiles == "7.6"){
		zoom = 10.5;
		setZoomValue = 120;
	}else if(radiusMiles == "7.7"){
		zoom = 10.5;
		setZoomValue = 120;
	}else if(radiusMiles == "7.8"){
		zoom = 10.5;
		setZoomValue = 120;
	}else if(radiusMiles == "7.9"){
		zoom = 10.5;
		setZoomValue = 120;
	}else if(radiusMiles == "8"){
		scale= 1.9;
		zoom = 10;
		setZoomValue = 120;
	}else if(radiusMiles == "8.1"){
		scale= 1.8;
		zoom = 10;
		setZoomValue = 120;
	}else if(radiusMiles == "8.2"){
		scale= 1.73;
		zoom = 10;
		setZoomValue = 110;
	}else if(radiusMiles == "8.3"){
		scale= 1.7;
		zoom = 10;
		setZoomValue = 110;
	}else if(radiusMiles == "8.4"){
		scale= 1.66;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "8.5"){
		scale= 1.6;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "8.6"){
		scale= 1.57;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "8.7"){
		scale= 1.55;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "8.8"){
		scale= 1.55;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "8.9"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "9"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "9.1"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "9.2"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "9.3"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 100;
	}else if(radiusMiles == "9.4"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 180;
	}else if(radiusMiles == "9.5"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 180;
	}else if(radiusMiles == "9.6"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 180;
	}else if(radiusMiles == "9.7"){
		scale= 1.5;
		zoom = 10;
		setZoomValue = 170;
	}else if(radiusMiles == "9.8"){
		scale= 1.48;
		zoom = 10;
		setZoomValue = 170;
	}else if(radiusMiles == "9.9"){
		scale= 1.45;
		zoom = 10;
		setZoomValue = 170;
	}else if(radiusMiles >= 10 && radiusMiles < 11){
		scale= 1.45;
		zoom = 10;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 11 && radiusMiles < 12){
		scale= 1.3;
		zoom = 10;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 12 && radiusMiles < 13){
		scale= 1.2;
		zoom = 10;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 13 && radiusMiles < 14){
		scale= 1.1;
		zoom = 10;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 14 && radiusMiles < 15){
		scale= 1;
		zoom = 10;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 15 && radiusMiles < 16){
		scale= 1;
		zoom = 9.5;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 16 && radiusMiles < 17){
		if(radiusMiles < 16.6)
			scale= 1.9;
		else
			scale= 1.7;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 17 && radiusMiles < 18){
		if(radiusMiles < 17.6)
			scale= 1.6;
		else
			scale= 1.6;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 18 && radiusMiles < 19){
		if(radiusMiles < 18.6)
			scale= 1.5;
		else
			scale= 1.4;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 19 && radiusMiles < 20){
		if(radiusMiles < 19.6)
			scale= 1.4;
		else
			scale= 1.4;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 20 && radiusMiles < 21){
		if(radiusMiles < 20.6)
			scale= 1.4;
		else
			scale= 1.3;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 21 && radiusMiles < 22){
		if(radiusMiles < 21.6)
			scale= 1.3;
		else
			scale= 1.3;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 22 && radiusMiles < 23){
		if(radiusMiles < 22.6)
			scale= 1.3;
		else
			scale= 1.2;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 23 && radiusMiles < 24){
		if(radiusMiles < 23.6)
			scale= 1.2;
		else
			scale= 1.2;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 24 && radiusMiles < 25){
		if(radiusMiles < 24.6)
			scale= 1.15;
		else
			scale= 1.15;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 25 && radiusMiles < 26){
		if(radiusMiles < 25.6)
			scale= 1.1;
		else
			scale= 1.1;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 25 && radiusMiles < 26){
		if(radiusMiles < 25.6)
			scale= 1.1;
		else
			scale= 1.1;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 26 && radiusMiles < 27){
		if(radiusMiles < 26.6)
			scale= 1.08;
		else
			scale= 1.08;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 27 && radiusMiles < 28){
		if(radiusMiles < 27.6)
			scale= 1.05;
		else
			scale= 1;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 28 && radiusMiles < 29){
		if(radiusMiles < 28.6)
			scale= 1;
		else
			scale= 1;
		zoom = 9;
		setZoomValue = 170;
	}
	else if(radiusMiles >= 29 && radiusMiles < 31){
		if(radiusMiles < 29.6)
			scale= 1;
		else
			scale= 1;
		zoom = 9;
		setZoomValue = 170;
	}
	
	//for out screen map
	/*if(divId == 'mapdivOut'){
		if(radiusMiles <= 1 && radiusMiles > 0){
			zoomOut = 14;
			setZoomOutValue = 100;
		}else if(radiusMiles <= 2 && radiusMiles > 0.9){
			zoomOut = 13;
			setZoomOutValue = 100;
		}else if(radiusMiles <= 3 && radiusMiles > 1.9){
			zoomOut = 12;
			setZoomOutValue = 120;
		}else if(radiusMiles <= 4 && radiusMiles > 2.9){
			zoomOut = 12;
			setZoomOutValue = 100;
		}else if(radiusMiles <= 5 && radiusMiles > 3.9){
			zoomOut = 11;
			setZoomOutValue = 140;
		}else if(radiusMiles <= 6 && radiusMiles > 4.9){
			zoomOut = 11;
			setZoomOutValue = 140;
		}else if(radiusMiles <= 7 && radiusMiles > 5.9){
			zoomOut = 11;
			setZoomOutValue = 100;
		}else if(radiusMiles <= 8 && radiusMiles > 6.9){
			zoomOut = 10;
			setZoomOutValue = 180;
		}else if(radiusMiles <= 9 && radiusMiles > 7.9){
			zoomOut = 11;
			setZoomOutValue = 100;
		}else if(radiusMiles <= 10 && radiusMiles > 8.9){
			zoomOut = 10;
			setZoomValue = 160;
			zoomOut = 10;
			setZoomOutValue = 100;
		}else{
			zoom = 10;
			setZoomValue = 100;
		}
	}*/
		
	$('#mapdiv').css({
        transform: 'scale('+(scale)+')', // set zoom
        transformOrigin: '50% 50%' // set transform scale base
    });
 
	$('#mapdivOut').css({
        transform: 'scale('+(scale)+')', // set zoom
        transformOrigin: '50% 45%' // set transform scale base
    });
	
	if(divId == 'mapdivOut')
		return zoom;//return zoomOut;
	else
		return zoom;
}

function getMiles(i) {
    return i*0.000621371192;
}
function getMeters(i) {
	//return 18*1609.344;
    return i*1609.344;
}
function myMap() {

	var latitude = lat;
	var longitude = lang;


}