/**
 * update location view initial version 
 */
var refreshIntervalLocation = "";
jQuery(function($) {
 	/*clearInterval(refreshIntervalLocation);
	refreshIntervalLocation = window.setInterval(function(){
		getPosition();
	}, 15000);*/
	
});
function getPosition() {
	//alert("getPosition start");
	//alert("longitude" +longitude);
	var data = {};
	data['latitude'] = 1.2323;
	data['longtitude'] = 454545;
	data['userId'] = 3;
	
	var url = domainUrl+"/Business-core/user/updateUserLocation";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		//cache: false,
		success: function(data) {
			//if(data)
				
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});

	
	/*var options = {
    	enableHighAccuracy: false, timeout: 300 * 1000, maximumAge: 0
	}
   	var watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);

   	function onSuccess(position) {
   
		alert("getPosition -- ");
   		
      	alert('Latitude: '          + position.coords.latitude          + '\n' +
         'Longitude: '         + position.coords.longitude         + '\n' +
         'Altitude: '          + position.coords.altitude          + '\n' +
         'Accuracy: '          + position.coords.accuracy          + '\n' +
         'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
         'Heading: '           + position.coords.heading           + '\n' +
         'Speed: '             + position.coords.speed             + '\n' +
         'Timestamp: '         + position.timestamp                + '\n');
         
   };

   function onError(error) {
      alert('code:  getPosition '    + error.code    + '\n' + 'message: ' + error.message + '\n');
   }*/
}

function watchPosition() {
   var options = {
      maximumAge: 3600000,
      timeout: 50000,
      enableHighAccuracy: true,
   }
   var watchID = navigator.geolocation.watchPosition(onSuccess, onError, options);

   function onSuccess(position) {
      alert('Latitude: '          + position.coords.latitude          + '\n' +
         'Longitude: '         + position.coords.longitude         + '\n' +
         'Altitude: '          + position.coords.altitude          + '\n' +
         'Accuracy: '          + position.coords.accuracy          + '\n' +
         'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
         'Heading: '           + position.coords.heading           + '\n' +
         'Speed: '             + position.coords.speed             + '\n' +
         'Timestamp: '         + position.timestamp                + '\n');
         
      
   };

   function onError(error) {
      alert('code: '    + error.code    + '\n' +'message: ' + error.message + '\n');
   }
}