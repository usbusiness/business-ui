var refreshIntervalId = "";
var refreshIntervalOutId = "";
var refreshIntervalINScreen = "";
var refreshIntervalOutScreen = "";
var refreshIntervalInHistoryScreen = "";
var refreshIntervalOutHistoryScreen = "";
jQuery(function($) {
 $(document).on('click', '.toolbar a[data-target]', function(e) {
	e.preventDefault();
	var target = $(this).data('target');
	$('.widget-box.visible').removeClass('visible');//hide others
	$(target).addClass('visible');//show target
 });
 
 	$('input').unbind('change').bind('change', function(){
 		console.log($(this).val());
 	});
 	
    $('#inHeader').unbind('click').bind('click', function(){
    	
    	//Update keyword in radius
    	callUpdateKeywordTrigger();

 		$('[rel="inScreenContent"]').show();
 		$('[rel="outScreenContent"]').hide();
 		$('[rel="profileHeaderContenet"]').hide();
 		$('#mainBodyContentInAndOut').show();
 		//chat section
 		$('#inHeaderFourth').hide();
 		$('[rel="chatScreenStep2"]').hide();
 		$('[rel="chatscreeninputDic"]').hide();
 		//chat refresh call
 		getReceivingMessageInChat();
 		//reload call	
 		getKeywordData();
 		
 		backButtonEventStep1();
 		
 		$('[rel="profileIcon" ]').show();
 		
 		$('#outHeader').addClass('selectedtab');
 		$('#inHeader').removeClass('selectedtab');
 	});
    
    $('#outHeader').unbind('click').bind('click', function(){
    	//Update keyword in radius
    	callUpdateKeywordTrigger();
    	
    	$('[rel="inScreenContent"]').hide();
 		$('[rel="outScreenContent"]').show();
 		$('[rel="profileHeaderContenet"]').hide();
 		$('#mainBodyContentInAndOut').show();
 		//chat section
 		$('#outHeaderFourth').hide();
 		$('[rel="chatScreenOutStep2"]').hide();
 		$('[rel="chatscreeninputOutDic"]').hide();
 		//chat refresh call
 		getReceivingMessageOutChat();
 		//reload call
 		backButtonOutEventStep1();
 		
 		$('[rel="profileIcon" ]').show();
 		
 		$('#inHeader').addClass('selectedtab');
 		$('#outHeader').removeClass('selectedtab');
 		
 		$('#outContentDiv').show();
 		
 		$('#outDeleteAction').attr('src','images/Trash unselected.png');
 	});
    
    $('#addNewKeyword').unbind('click').bind('click', function(){
 		$('#addNewkeywordContent').show();
 		$('#step2 .navbar-header').hide(); // add by sri
 		$('#inContent').hide();
 		$('#saveNewKeywordButton').show();
 		$('#addNewKeyword').hide();
 		$('#inHeaderSecond').show();
 		$('#inHeaderFirst').hide();
 		$('.footer').hide();
 		
 		//clearing input values
 		$('#inputInKeyword').val("");
 		$('#miles').val(30);
 		$('#rangeInput').val(30);
 		
 		keywordDeleteRadioButtonToogle('others');
 		loadMap(lang, lat,"mapdiv",$('#miles').val());

 		$('[rel="profileIcon" ]').hide();
		$('#addNewkeywordContent').addClass("fullscreen"); // added by sri
		toggleFullScreen($('#mapdiv')); // added by sri

 	});




    function toggleFullScreen(mapContainer) {
   
        $(mapContainer).addClass("fullscreen");
    
        //map.updateSize();
    }


    $('#keywordsContent').unbind('click').bind('click', function(){
 		$('#addNewkeywordContent').hide();
 			$('.footer').show();
 			$('#step2 .navbar-header').show(); // add by sri
 		$('#inContent').show();
 		$('#saveNewKeywordButton').hide();
 		$('#addNewKeyword').show();
 		$('#inHeaderSecond').hide();
 		$('#inHeaderFirst').show();
 		$('[rel="profileIcon" ]').show();
 	});
    
    $('#addNewKeywordOut').unbind('click').bind('click', function(){
 		$('#addNewkeywordContentOut').show();
 		$('#step2 .navbar-header').hide(); // add by sri
 		$('#outContent').hide();
 		$('#saveNewKeywordButtonOut').show();
 		$('#addNewKeywordOut').hide();
 		$('#outHeaderSecond').show();
 		$('#outHeaderFirst').hide();
 		
 		//clearing input values
 		$('#outKeyword').val("");
 		$('#milesOut').val(30);
 		$('#messageTextarea').val("");
 		$('#rangeOutInput').val(30);
 		$('#countMatchingMsg').html("");
 		
 		$('#outScreenImg').attr('str','');
 		$('#outScreenImg').hide();
 		
 		loadMap(lang, lat,"mapdivOut",$('#milesOut').val());
 		
 		$('[rel="profileIcon" ]').hide();
 		$('#addNewkeywordContentOut').addClass("fullscreen"); // added by sri
 		//toggleFullScreen($('#mapdivOut')); // added by sri
 	});

$(document).ready(function() {

    $('.sendCamera').bind("click", function (evt) {

    	openGallaryOrCameraPopup();
              
            });
    });

    $('#keywordsContentOut').unbind('click').bind('click', function(){
 		$('#addNewkeywordContentOut').hide();
 			$('#step2 .navbar-header').show(); // add by sri
 		$('#outContent').show();
 		$('#saveNewKeywordButtonOut').hide();
 		$('#addNewKeywordOut').show();
 		$('#outHeaderSecond').hide();
 		$('#outHeaderFirst').show();
 		$('[rel="profileIcon" ]').show();
 	});

 	 $('#hottag').unbind('click').bind('click', function(){
 	 	getHotTagList();
 		
 	});
    
     $(document).on("click", ".hottagbutton" , function() {
     // $('.hottagbutton').unbind('click').bind('click', function(){
 		 var a = $(this).parent().find('span').text();
 		 $('#outKeyword').val(a);
 		 $('#hottagModal').modal('hide');
 	});
    
    loadInitializationApp();
    
});

//you don't need this, just used for changing background
jQuery(function($) {
	 $('#btn-login-dark').on('click', function(e) {
		$('body').attr('class', 'login-layout');
		$('#id-text2').attr('class', 'white');
		$('#id-company-text').attr('class', 'blue');
		
		e.preventDefault();
	 });
	 $('#btn-login-light').on('click', function(e) {
		$('body').attr('class', 'login-layout light-login');
		$('#id-text2').attr('class', 'grey');
		$('#id-company-text').attr('class', 'blue');
		
		e.preventDefault();
	 });
	 $('#btn-login-blur').on('click', function(e) {
		$('body').attr('class', 'login-layout blur-login');
		$('#id-text2').attr('class', 'white');
		$('#id-company-text').attr('class', 'light-blue');
		
		e.preventDefault();
	 });
	 
});

function loadInitializationApp(){
	getKeywordData();
	getOutKeywordData();
	
	//refresh initial screen load event
	loadRefreshData();
	//getMessageData();
	
	//in content
	$('#saveNewKeyword').unbind('click').bind('click',function(){
		if(validateAddNewKeyword())
			saveOrUpdateKeyword(false);
		else
			validationErrorMsg(true);
		return false;
	});
	
	$('#inDeleteAction').unbind('click').bind('click',function(){
		keywordDeleteRadioButtonToogle();
	});
	
	$('#inHistoryDeleteAction').unbind('click').bind('click',function(){
		keywordHistoryDeleteRadioButtonToogle();
	});
	
	$('#inChatDeleteAction').unbind('click').bind('click',function(){
		deleteKeywordInHistory($('#inChatText'),true,true);
	});
	
	$('#outHistoryDeleteAction').unbind('click').bind('click',function(){
		keywordHistoryDeleteRadioButtonToogleOut();
	});
	
	$('#outChatDeleteAction').unbind('click').bind('click',function(){
		deleteKeywordInHistory($('#outChatText'),false,true);
	});
	
	$('#submitKeyword').unbind('click').bind('click',function(){
		keywordSubmit();		
	});
	
	$('#messageUp').unbind('click').bind('click',function(){
		msgToogleUpShowHide(this);
	});
	$('#messageDown').unbind('click').bind('click',function(){
		msgToogleDownShowHide(this);
	});
	$('#logout').unbind('click').bind('click',function(){
		location.reload();
	});
	
}

function keywordDeleteRadioButtonToogle(callFrom){
	
	if(!$('[rel="inRadioButton"]').is(':visible')){
		$('[rel="inRadioButton"]').show();
		$('#inDeleteAction').attr('src','images/Trash selected.png');
	}
	else{
		$('[rel="inRadioButton"]').hide();
		$('#inDeleteAction').attr('src','images/Trash unselected.png');
	}
	
	if(callFrom == 'others'){
		$('[rel="inRadioButton"]').hide();
		$('#inDeleteAction').attr('src','images/Trash unselected.png');
	}
}

function outKeywordDeleteRadioButtonToogle(callFrom){
	
	if(!$('[rel="outRadioButton"]').is(':visible')){
		$('[rel="outRadioButton"]').show();
		$('#outDeleteAction').attr('src','images/Trash selected.png');
	}
	else{
		$('[rel="outRadioButton"]').hide();
		$('#outDeleteAction').attr('src','images/Trash unselected.png');
	}
	
	if(callFrom == 'others'){
		$('[rel="outRadioButton"]').hide();
		$('#outDeleteAction').attr('src','images/Trash unselected.png');
	}
}


function keywordHistoryDeleteRadioButtonToogle(callFrom){
	
	if(!$('[rel="inHistoryRadioButton"]').is(':visible')){
		$('[rel="inHistoryRadioButton"]').show();
		$('#inHistoryDeleteAction').attr('src','images/Trash selected.png');
	}
	else{
		$('[rel="inHistoryRadioButton"]').hide();
		$('#inHistoryDeleteAction').attr('src','images/Trash unselected.png');
	}
	
	if(callFrom == 'others'){
		$('[rel="inHistoryRadioButton"]').hide();
		$('#inHistoryDeleteAction').attr('src','images/Trash unselected.png');
	}
}

function keywordHistoryDeleteRadioButtonToogleOut(callFrom){

	if(!$('[rel="outHistoryRadioButton"]').is(':visible')){
		$('[rel="outHistoryRadioButton"]').show();
		$('#outHistoryDeleteAction').attr('src','images/Trash selected.png');
	}
	else{
		$('[rel="outHistoryRadioButton"]').hide();
		$('#outHistoryDeleteAction').attr('src','images/Trash unselected.png');
	}
	
	if(callFrom == 'others'){
		$('[rel="outHistoryRadioButton"]').hide();
		$('#outHistoryDeleteAction').attr('src','images/Trash unselected.png');
	}
}

function msgToogleUpShowHide(element){
	var curClass = $(element).attr('class');
	if(curClass == 'btn btn-sm btn-primary'){
		$(element).removeClass('btn-primary');
		$('[rel="messageUp"]').hide();
	}else{
		$(element).addClass('btn-primary');
		$('[rel="messageUp"]').show()
	}
		
}

function msgToogleDownShowHide(element){
	var curClass = $(element).attr('class');
	if(curClass == 'btn btn-sm btn-primary'){
		$(element).removeClass('btn-primary');
		$('[rel="messageDown"]').hide();
	}else{
		$(element).addClass('btn-primary');
		$('[rel="messageDown"]').show()
	}
}

function keywordSubmit(isEdit,element){
	if(validateAddKeyword(isEdit,element)){
		
		var url = domainUrl+"/Business-core/keyword/addOut";
		if(isEdit)
			url = domainUrl+"/Business-core/keyword/updateOut";
		
		// Get form
	    var form = $('#fileUploadForm')[0];
	    
	    //var file_data = $("#file_image").prop("files")[0];
	    
	    var data = new FormData();
	    
	    var fileData = ""
	    
	    fileData ="/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCASAA2ADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDdooxS1AwoooFABSig0maAHUUwuB3ppmUd6AJM0ZqubkdqjNye1AFvNNLgd6pmZzTfnamBcMy0w3A7VAInNPWA96AFNwajLs1TiAU4RqKBFTyy3alFsTVvgUbqAIUgxUgTFBekLGgB/AoLCosmk5oAl8ylzuFQ4pVOKYxksOeabHJsOCKtKc0ySHd0oESI4YU8GqAZomq1FKGpATUtNpaYxaKBS0hCUUUtMYlFFFAAaBRS0AJS4oFBoASlFFFABRRRQAUhoooAKKWkNADadSUUAFLSCloASgUUUALRRRQAUCikoELSYzRSA0wF28U1kp9GaAKssW5cVFG207TV0jNVZ4zncKQySNsGpwapxvuFWI37GmIlFFJQaAFooooAKSnU3PNIYUGlFFACAUYpaKAENFLSUAA60tIaTNAC0d6KTvQIWiikoAWkpaDQAlFFFMANJinUlIBKSlopgFBopKACikpaAENIRS0UAJiig0UAFFIaKAFNQzdKlNQy9KAIx0p8X3qYOlLH96gCc0UZooAnyKTcB3qiZ2NN3O1TYZeMyjvUZuVFVhG7U5bYnrRYBzXR7UwzOalW2FSLCooAq5kalETmrYVRS8UAVltz3NSLbipC1JuouACJRTsKKZuNGTSAkyKaWptFAC7qTNLSUAJRTqKBjcUU6koAbRilopiExSYp1FAho4qRWplFMZI6BhVVkaNsirKtTjhhQIiiuM8GrA5HFU5YMcrRDOUOGoAujilpEdWHFOFIApaM0UAAooooAMUlFANMYdqKKKACiig0ABoFJRQAUGiigAooFIaAFFFJS0AJS0lGaAFoptLQAUUCigBaKSigBaTvRSUCHUlFFABTXXIpaWgCi6mJ8jpUit0NSypuFVVJRtpoGXkfcKdVZGxUw5oEPzRmkHNFABRQKWgAopDRQAtFFJQAtFJRQAGm85p1JmgAFBpM0ppgFLTaWgBc0UlFAC0U2igBaKTNJmgB1JSZpCaAHUlJmkNAC5pabQaAFpKQGloAO1JRmkJoAWkozSZoAM1HL0pxpj9KAGjpTovvUwdKWNtpoAsUVH5opplFAEiwqKcEUUzeaMmpGS8CjdUdLQA/dSZpKKQxaKKWgBuKTFOpaAG4paWigBKKWigAopaSgQUlLRQAlFLRQAlFLSUwEopaKBDaKWimMSgHFFFAEgOailhDcjrS09WoEUld4W56VeinWQe9MeNXGKpvG8LZU8UAalLVS3ug3DdatA5pAOpM0UUxhRRSUgsLRRSUwA0UUGgBaSjNJQAtBpKSgBaKSigBaSig0AFFFFABRSGloAKM80UUALRSUhoAWikpaBCZozRQaAClpuKWgBarTxZ5HWrNIRmgCnG/Y9asI/aq8yFG3CnI+RmgC0DS5piNkU+gBaKbRQA6kzRRQAUoptFAC0UmaTNMBTRSZooAWikzRmgBaM00tTd+aAJM0maZupN1IB5PvSbsd6YWppIoAl3ZFJmot9AegCXrQaj8ykMy+tAEtJUXnikM9AExNGarGVj0FJukPagCwfrRux3qviQ0vlOe9AEpcetJ5opgtz3NKLcUAIZhSGepBAtI0K9qAITcVFJdYOKfJbnHFUpI2DjNIDRR9y5phDM3FJF/qxUsXWqAaIWpRBVgUUAR4IpwNTNHUZTFSMQU6o+lODUhj6Wm06gBaKSigBRS0lLQAlLRRQAUUUUAJRS0UxCUClpKACiiigAooopgFFFFACUUUUAJRS0UAJSUtFAAGxSkBhzTaUcUxFaa3IO5KILoodr1bBz1qGa3DjI60gLSSBxkGnZrKWSS3bnpV+GdJRkHmkMnpDSUUxig0UUUAGaSiigQlLRSUALSUCigA7UCiigBaKSigANIKWigAoopKAFopKKBBS0lFMAoozSGgBaKSigBaM0UlAC5optLQA113Cqh/dSY7VczUcse9fegBqNiphzVNGwdpqeNscGgCaikzRSAXNGaaaM0AOFH403cKaXFAElJURmUd6aZhQBMTQDVczZ7GkEjdlNAFnNIT71XzKe1LslPegCbcKaWUVF5Lk8tTxbjuTQAGVfWmmUVILdKcsSDtQBW83PRTS5c/wANW9qjoKAKAKZSUmlELnvVvFJQBWFue5pfs471YxSYoAiEKU8RIO1PxRQAmxR2pcUUUWAQ0opKKYAaSlpKACjFFAoATFVp4R1q1UU33KVgK6cLUkXWo1PFPh60xFigUlKKBlsimFKkoqCiBoxUZTFWiKaUoAq8ilDVKyVGUxQAoNLUfSlDUAPpabS0ALRSUtMQtFJRQAtFJS0BcKKKKYwoopKAFpKKKACgUUtAhKKKKAEooooAKKWkoASilpKYhKcGptFACyRLItUnieBtydKuA4pxAYc0WAit7sPw3Bq3nNZ81tg7kpILpkO16Vho0aKYsgdcrS5oAWikFKKACiiigBaKKKBBRRRQAlFFFABRSUUALSUUGgAo7UgozQAtFJmjNABRmkJpNwpgOophkVe9Rm4QfxCkBPRVVr2MdDmmG9/uoTQBdpM1SNxM33YzTSbpugxQBeyKaXFUjFct1fFOW2fHzOaBjpQpbcCM0o5HWj7IvcmqzE282w8qaALgfaMZo8xuwqNeDVpcEUAQFpD0FG2U1ZoxQIr+S56tSi39TViigCEW605YVU9KkzRQAmxfSjaPSlop2AKKKO1ACYopaSgAFLSUtABRSUUALSCiigBaSiigApDRmg0AFFFFABTaWkoAWkopKAFopM0maAFqKX7tPJqOU/LQIrjpT4DzTO1PgHNAE4NLTaKBmjRRS1BQlFFFADSuaaUqSigCu0dRlMVcIphSgCr0pQ1StHURTFMQ6imdKUNTGPopuaWgQopabS0ALRSUUDFopKKAFopKKAFoopKAFpKKKBBRRRQAUlLSUAFJS0lMQUUUlABSZopKYDw2etRTW6uMjrTqVXoAqI727c9KvRTrIPemuiyDFVGieJsr0pWA0KWqsFznhqtAg0hjqKTNJuFAhaKTcKaXX1oAfSZphlUd6YbhRQBNRVY3HoKQzOegoAsZo3Cqu6U0FZWoGWd4pplQd6rmFz1NJ9m9WoAmNwg70w3S9qFt0709YUHagCH7Sx6LQZZT0FWQijoBQRQBWxO3fFH2eVvvPVkUtAFf7GD95iacLOIds1OKDQIjW3jXogp4RR0ApaKACkpaBQAgWjFLSUAJUNxCJF9xUxNGaAKMUnJRvvCrMEnODVW9iZXE0f406OTcoYUgL9LUUT7lqTvTQC0UhopgOpKTNFAC5opKKAFNFJRQAtIaKKAFpM0gpaAFpKTNFAC0lFGaACiiigAoopM0ALRTc0UALSUZooAKSikzQAUZpDSGgA3VHK3y06o5fu0IREp4qWGoAeKlhpiJ80lFFIo0qWiioKCiiigApKWigApKKKAEIzTSlSUUAV2jqNkIq3imlKYipyKUNUzR1EyYpgGaXNR8ilDUAPopuaM0AOopM0UALS02lzQAtFJRQAtFJRQAtJRSUALRSZpM0ALSZpM0maYh2aTNJg0bWouAUmad5Zo8qi4Dc03NSiIUvlj0ouBCHIqRfnHNPCinBfai4FOW35ypp8LlRgmpylV5ImU5FIBGuH3YAo3yNQjDPzDmrKYxSAr7ZWpRC56mrGKKYyutsd3JqUQJUmaM0ANESDtTto9KXNFADSKSn03FADcikzmn7aNtADcGnCnUUAMIoxmnUUCExRilooAQCilpM0AFFBpKAFFGaSkoAXNIaSg0wFppHNLRQAjAMpBrNObacqfuGtOobiESpjv2pDI4n2t7VbU5rNjJHyN1FW4HzwaALGaTPNJmlpiFzRmm0tAC5opKKAFozSUUALmikooAWjNJSUALRSUUALSUUUAFGaKKADNFFJQAUtJSZoAM0ZpKKAFpKTNFAATTaU0lADSaZL92pKjl+7QIgHSpoelQDoalh6UxE+aKSkzQUatLSUtZlAKKWigBtApaSgB1JRRQAUlLRQAUUUlAARTSlOozQBA0ftUTRn0q0SKaQKLiKhyKA9TMFqFkp3GLu96XNRqtSCOi4gzQDThHThHTAjzS5NSeXShBSAi5owam2Cl20AQbWo2Gp8UuKAIfLNAjqaigCMR0eWKkpaAGbR6Uu2lpaAExSYpaSgQYoxS0ZoATFGKSimMXFNIpaTNAEEsOeVqJXaM4PSrdRSRhx70AORwwp1Ujviap45Q1AE1FIDS5oAUUtJRQAUtJRQAUUlFMQtJmkpaACikzRSAWikpaAEzRRSZpgFFFFABRSUmaAFooooAbRRS0AJmg0UUAVrqM/wCsXrUSSdGFXWGRg1QdPLc+lIZfjcMKfVCGTa2O1XA2RQhD6KZmlzTAdRSUtABRRSUALRSUUALRSUUALSUUUAFFJRQAtFJSUwFopM0UgFpKKSgBaTNFJQAtJSZpKAFzSUUUAJmo5T8tPNRS/doEQZ4NSwGof4TUsFMCxmkpM0maBmzRRmkLCsyhaKZvFIZKAJKKhMlJ5hoAmzRmodxpuTQBNuo3iocGl2GgCQyimmWk8o0oioAaZTSb2qURUoiFAEBLUbWNWdo9KXAoAreWTR5RqzRQBSKFacsmOtWSoNRPD6UAKrBqdiqxVkNPSfs1AibFFIrg06mAlFLSUCCigUtACUhFLRQA2loooAKKSigApKKKACiiigApKSimMWkNLSUAJSZpaSgBjqGHNVJEaNsrV2mkA9aAIYp88N1qwrZqpNFjlabHMVOGoAvZpaiRtwp9MQ7NGaSigAopKKAFopKKACijNFIBKWkopgLmim0tABRRRQAUlFFABRRSUALRSUUAFFFFAATUMqb16c1LRRYDOxg4q3A+Rg024j/iAqBG2nNIDQpKYj7lpwbmmAo5p1JmigBaKSkoAXNBpKKAFpKM0ZoAWkzSZozQAtFNzSFqAH0UzePWkLj1oAfRmo/NWk81aAJM0VCZxTTP7UAT0lQGY+lJ5jUAWM0mag3PR89AExNJuFQ4ajY1AEpYVFKwIo2VHInFAhn8JpYpAtMHQ09IwaAJDMKb51L5S0eWtAzZyaMGpAgpcVBRFsNL5ZqYEU6gCHyqXyqlooAjEYpdgp1FABgUtJS0wCkopaAEoNLSGgQUlFFIYUUlFAhaSiigBCoPWoHgz0qeimBSw8ZqRJvWp2UN1qF4P7tAEobNLVXLIakSYHrQImopu7NLmmMKWikoAKKKTNAAaSlNJQAUGijNIQlLSUZoAKSjNFMYUUUlAC0UlFABimkU6igCPbUMkAbp1qzTTQBRVmibB6VZjlDCleIMOaqOjRNkUxF7NLVaGYNwetTg0AOopKM0ALRmkpKAHUmaSgUAGeaXNJRQAUUUmaAFopM0ZoAWikzRmgAopM0ZoAWikzSbhQA6im7xSGQUAPoqPzKN1ADzyKpSJtb2q1uNRuu4UMCOJ9hqyCDzVVkpY93SgC3mjcKhCNRsNAEm8etJ5gpnlUvlCgB3mikMwoEYpdi0AMMvtSeY3pUm0UuBQBFvakLPU2KSmBF85o2MalpRQBD5betHle9TUlAEXlD1oEQqWkoAZ5S0vlr6UtGaAE2j0owKWkzSAPwpKM00mgBaTNJmkJoAU1FL92nlqilb5aYiEHip4jxVcHip4T8tAEtFJSGgZ0OKQrTqWsyiI0makIppSgADUuaYVpuSKAJaKjD08MDQAtFFFMAooopAFFFJQAUUUUAFJS0lAgpKWkoAWkoopgFFFJQIayhqhaH0qxRQBS3OhqRJQetSsgbrUDwkdKYycNS1UDOlSJMD1oAnopobNLQAtJRSZoEJTTTs0hoGJg0AHNJmnA0hC0UlBpjClpKTNAhaSiimAlFFBoAKSikNAAaawDDBpc0maAKssBX5lpYp8cPVkmq8sIbkdaALCtmlzVWLevBqXzMUAS0VGr5pSxoAdRmo8mjDUAOLUu4VGUNHlmgBxcetJ5gpvle9HlUAL5opPNpRGKcI1FAEZlPpSeYxqXYPSlCigCDc9BMlT4ooAgCuad5R9ak4ozQA0R0oQU6imAm0UoApKWgBcUhWlpKAGMopu3Bp9GKAFU5FLTRwadSAKKSigAoopKYC5ozSUUAGaKKKAEzS0lFAC0CkNJQAtFJRmgApDRmkzQAtNNGaQmgAzSE0maTNAgJpCaCy+tMMi+tACmopfu04yr61FNKuzjmnYVyIH5TViA/LVDzuDxU8NwAvNFguXaKrG6FNN1Tswujr6KWkrE0CkopaAEppQGnUUAQlKYcirFNIBoAhDkU9ZAetDR+lRlCKALANFVtxWnLL60ATUU0ODS0ALSUUlAC0UlFABRRSUCFopKSmAtFJmkzQIWikzRmgAopM0ZpjEZFbrVeSDHSrGaN1AFQMyVIsoNPZQ1QPGR0oAsBqM1Xw22oizp2NAi5mlzVWOQtUgLUASnFHFR4al2mgB+4UbhTdho2UABaml6dto2imBHvbNPDUuBRQAmaKU0UANoxS0UAJto206koAbto206kzQAm0VG8dS0HpQBVD7DzUquGpssQYcVW+eNvahgXaWoY5Q3WpqEAUYozRmgBaKbRTAWjNFJSAWkozRQAUUlLTEJRRmkzQAoNLTaXNABRmkooC4uaWm7h60m9fWnYBxpAaYZV9ajadVosK5OelIrdqqG9w3FH2k5zT5WHMi9mk4qr9ppDcmjlYcyLdFUvtTUn2l6OVhzIu5ozWf9oek85zT5GLnRo5FJuHrWaZJPWje/rT5GLnRol19aaZFHes/c/rSfN60ezF7Q0DKvrTTOvrVHB9aNtP2YucuG4WmG7UVWKZppSn7MOcsG8FM+21B5Yo8uj2aFzsmN2fSmG6Y9qbspdlPkQudiee9NMzmnbaNtPlQczIy7nvSfN61Lto20cqFzMhIb1o2nvU2KTFOwXIdg9KNtS7aNtFhEeyk2VLikxQM7Oim5ozXEdYtFN3UuaAFopN1JuFAC0U3eKTeKAHUlJvFJvoAGQGomi9KkL0m+gCEgrQJCKlJzUTJnpQIesoPWn5FVdjA075gKBljIo3Cqu9qerZoAm3CjcKYFpdhoELvpC9Gyl2UwG7qTNP20u2mAyinYpKAG0c04UUAMpQKdSUAJijbS0UAN20ySIMKlooEVEj2GplYU4io2T0pgS0VAHK1IsgNAD6KTNJmgBaKbmjNFgFopM0maAHUlJmkzTELRmm7qTePWgB2aKZ5g9aTzRRYLj6SmecvrTTMKdhXJQaM1XM4pj3FPlYcyLWeKY6hqq/aCe9NMzUchPOOcbDkGpYp81UZmakUkUcgc5ohx60u4etZ5kalEjU+QOcv5HrSbh61S8xqTe3rRyMOcvbx60bx61Qy3rRlvWnyBzl7zF9aTzl9ap/N60mD60cgucuecvrTTOtVsGjbT5A5yx9oFJ9o9BUAWjbT5CecmNxSfaTUWKXFPlQczHee1N8x6MUYo5ULmYb2Peky3rSgUuKaQXG8+tNK5qSginZCuQiMZp2Pan4oxQAzFLinYpcUCI9tGKfijFMY3bSYqTFJigQ3bRtp1GKAG7aMU7FGKAGYoxT8UUAMxRinYoxQAzbRin4pMUAMIoxTsUmKAG4oxT6SgBmKTFOopAJikxTqKAGUYp1FMBmKMUtFIZ0fmmgS0pipmyuI6yQNS5NRgYp4cCgBeaTmnh1p2RQBHg0YNSUUAM2mjYafmjNADdlASnZooATaKNoozRmgBNopCgNOpKAI2iHaoypFWKQ0AQByKcsvrTmQGoWjI6UCJw4NOqlkrT1mI60wLVFRLKDTw1MBaSjNJmgAppNLmkNABuozTaTNAD80mabmkzQIfmjNRlsCoWnxTSuJuxayKTNUzOaQzn1qlEXMWXCnvUD8c5qIymmb2NPkFzEyz7eM1KJlxVEgmjmnyC5i6bhaabkVT2n1o2U+QXOWjdU03VQeXRsHpT5A5iU3Jppnb1pu0elG0U+VC5hfNak3tS4oxT5Rcw3c3rRlvWnYoxRYVxuDRinYpcUWAZikxT8UYpiGbaMU/FJigBuKMU7FGKdgG4oxTsUYosAmKMUtFACYpMU+igBtFOxRSASilopiG0U6ikMSilooASkp1JQAYopaKACilpKYhtLRSigYmKKWjFACUUtFACUUtFAhKKWigBtFLRQAlJS0UAJRS0UAJSUtGKAEpKdSUANNJTqSmMKSlooENopaKQDaKWkpjEpKdRSA6XNIaTNJmuI6xStRlDUmaTNAEfIoDkU+mlRQAolpwlFQlaacigCzvpd1VNxFOWU0AWc0ZqESinB80ASZozTM0ZoAfmkzTc0ZoAdmim5ozQAtJRmkzQAhUGomi9KmzTSaYiuVZaFkIqc1GyqaQxVn9aeJAe9VmUjoaiLle9AF7NGapC4296etwD1p3EWc0hNQecvrR5o9aAJSaQmojKKYZh600Ilc8VXbrTjKCaBzWsDOYzFGKdiitTMbikxTsUYoENxRinYoxQAzFLinYoxQMbijFOxRigBuKMUuKKAExRS0UAJRS0YoEJRS4opgNop1FADaKWigBuKKdRQAylpcUUAFFLRQA3FLS0UAJRS0UAJRS0tAhtLRS0ANopaWkMbRinUlACUUtLTEJSU6koAbS0UtAxMUU6igBtFLRQAlFLRQAlFLRQIbRS0UAJSU6koASilNFADaKWigBpop1JQA2kp1JTGJSU6koEJRS0UANpKdRQA2inUlIZv5pM1X82mm4xXEdZbzSZqqtwDUokB70AS5pM1HupN1AEuaQ1Fuo3UAPIFNK03dRuoAQ8Um/FLmmkZoAeJacJQarlKACKALPmD1o8yqhY0CQigC35lJ5lVxJ607dmgCbzKQy1FRigCTzaQyGmYpKAHGQ00uaKaaAEMlRM2aeRUbLQBXc9cVGZStSunWq8imgCNrsqetINQI6moJEzVaSMihCNH+0M96a1771kNuBphlYGmI24Los/WteA7kzXLWMuZa6i1/wBUK2gZzJaTFOxRitTMbikxT8UYoAZijFOxRigBuKMU6igQ3FGKdRQAzFFOooGNoxTqKBDcUU6kxQA2inUlMBKKdSUANop1FADcUUtGKAExRS0UAJRS0UAJRS0UAJRS0YoASinUUCG0U6igBlLSHrTqQwopaKYhtFLRQAUUtFADTRS0ooASilooGJRS0UCExRS0UANopaSkMKSlopiEopaSgBKKWigBtFLRQA2kp1JTGJSU6koEJikp1JQMSkp1JQAUUUUCEpKdTaQy7zTHBp2aK4jrIOQaerkU+kKCgByzGpBMO9VinpSfMKALodT3pRiqO8inLKRQBdoqss9PEwNAEtFM3g96XPvQAtFJmkzQAEA0wp6U7NGaAIiCKbuIqU00qDQA0SkU4SimFKYQRQBY3g0Zqtk0oc0AWM0maI1L1L9nNFmBBmkNT+Qab5NFgK5FRNHmrvk0GCiwGVJDVaSCtz7N7U1rQHqKYjm5IM9qqyW59K6lrEHtTP7NB7UxHO2MLCbpXU2oxCKij08I2cVcRNq4raBlMTFGKfiitCBmKKdijFACYpMU7FGKBDcUmKfikoAbilxS0UANxSGn4pppgJRS0YpAJRS0UAJSU7FFACYpMU6igBtGKWloAZiilopgJRS0UAJRilooASkp1FIBtLRS0AJRS0UxCYopaKAG4oxTqKAEopaKAEoxS0UAJRS0UANpQKKdQA3FFOpKAEopaKAEopaKAG0U6koASkp1JQAlJTqKAG0lOpKAEopaKAG0UuKKAG02nGimMbSU7FJQAUlOpKBCUUtJQAlFLSYpDJ80bqcUphSuI6x26jNN20oFAC5pKXFLigBhANNKVNto20AQFTScirGyjZQBXDGniU1IYxTTFQAqymneZTAlG2gB2+jdTdtLigBd1G6kooAN1JmlxSYoAQjNM281JijFJAW7McjNaQQFelZtovzCtqGLKVoiWUWQDtTCPatFreomgpgUSPakINXDBTTCKBFPmkOatGIUwxigZW5oGasbBSbBSAixxTcVKRxTMc1rEykNxSYp9JWhA2inYooEMop2KMUwG0lOoxQAlJTqKQDaSn0lADcUUtFADaKdSUAJRS0UAJSU6igBtLS0UANop1JQA2ilopgJRS0UAJRS0UgExRS0UxCUUtFACUUtJQAUUUtACUUtFACUUtFACUUtJQAmKWlFFACUUtFACUUtFADaKWigAFJS0UAJSU6koAbSU6igApKWkoASkpaKYxKKWkoASkp1JQISkp1JQAlJTqSgBKKWigBtJTjSUgLeaQ4qj9sWj7YK4jsRe4pKpfbRR9tFAFykzVP7aKPtYpCLm+lElUDdD1ppuqYzR3il3Csz7YaPth9aANPdSZrMN6fWkN6fWgDT3e9IXHrWWbw+tRNdt60AbBcetNMq+tYpu29aYbpvWgNTbMy+tNNwvrWIbhvemG4akBufal9aBdL61h+ax70B39aAN8XCHvQJ09awxK1OErbhzQB1diwZxiuhtxlK5PR2LOK663HyVoiWOK0xkqbFIVoAqtHUbJVwrUTJTApslRlatMtRMtAFcimkVKRTCKAIiKjNTMKjNaxMpDMUYp2KSrIExSU6kxQIbRTqTFMBKKdSUgG0UtFACUlOpKAG0lONJkUwEooyPWjIouFgxRijI9aTcPWkMXFJRuHrRuHrQAUtN3r60eYvrTAdikxTfMX1pPMT1ougsOopvmr60nnL60roLD6KZ5yetJ56etFwsySiovtC+tIbhfWi4WJqKr/aUpDdLRzIOVliiqpvFpPtq0uZByst4oFVkud7VZXkVQgxSU6koEAoopaAEopaKAEopaKAEFLRS0AJRS0lABSUtLQA2inUUANopaKAEpMU6koAbRS0UANooopjEpKdRQA2ilpKAEopaSgApKWigQ2ilooASkpaKAENIadSUgMHL+9LliKsbaAtedc7rFcb6X56sBBil2Ci4WRWAfFOw+OlT7eKXZxTuBXw1LtarGzijbRcCDY9HltVjbxQVpXCxWMbetHlH1qzto20XArmE+tIYPerW2kK0gK32cUfZxVrHtQRQBVNutRNEAavkcVWcfNQAiRLjpT/AClz0p8Y4p+3mmOxD5a56UqxLuHFS7eaAvzCgDa0hQHFdVB92uY0ofOK6eD7lbR2M2SUlLRTJGmmsKfSGmBXcVC4qywqB6BldhTCKlamEUxELCo6mcVEa0iZyG0hp1BFWjMZRTsUlMBKKWikA2kp1FADKKWimA2kp9JigCrcSbFrLkv2VutaV3901hzJl+lZVJcqNKceZlkX7HvTlvWPeqioKmRBXK67OpUUTm6am/an9abto2ipdeRXsYi/an9aQ3T+tGwUbBU+3kP2URv2iT1NNM8nqak2UbRR7aQeyiRiaSjzpKeVFJtFL20h+yiM82SjfJT8AUvHrR7WQeziMDSetGX9ak4pMil7WQeziIA/rSEN60/eKCwpe1kHIhmG9aOaduFJuFHPIfIhmw5p6QkmlDAmp0PempsHFE1tCBVzGKithU5r0KXwnBV3EoxS0VsZCUUtFACUtFFACUlOopAIKWiloASilooASilooASiiloAbiilooATFJinUlADaSnUlACUUtFADaKWkpjEpKdSUAJSUtFACUUtFAhtFLikoASilooASkNLRQBmbhSbhTOaXBrzDuHhhS7himBTS7TQA/cMUbhim7TRtagB24YpNwpNrUbTTAXeKXcKbtal2mgB24UbqTYc0bDSGKWpC3tS7D60mw+tABv9qC3tTdhzShDQApb2qEj5qmEdI0dMQiECnbhmmAUu0+lIY7cM0qsNwqPacdKVQdw4piN/SiN4rpoPu1zGkj5ga6eA/LW0djNklFFIaYgJphNIzUwtQhgzVC1OJphpgMNMYVIaY1MRE/SoTU79KiNaQM5DaQ06itEQNxSU6kxSEJSU6koAbSU6imA2kp1FADaSnUhoAo3fQ1jSj5zWzd9DWRJ9+ubEbHRQ3I3O0ClikzRKOBSwrxXAdhKDRmlUU7FAxuaTNP20mKQCc0nNSYpNtIZGFNGDUuKTFAEe2jbT8UmKAGbfel20/FJigBu33pNtONAFADdtJtqTFG2mA0LViFcmolFXLKPe4qo7ikW4E2rTyKneLYgqE16lNWiebUd2JRS0VoZiUUtFACUUtFACUUtFACUUtFIApKWloASilooAbRS0UAJSU6koASkp1JQA2ilNJTGFJTqKAGUUuKKAG0UtJQAlFLSUAFJTqSgQlFLRQAlJS0UAJSUtFAGbs9qcE46U7FOA4rzTuGbPal20/FGKAGbaNtPxTsUAR7aNtSYoxQIj20bafilxQAwLzRtp+OadigZHto21JijFAEG3mnBafjmlAoAYFprrUwFNccUAQKvNP28Uqjmn4460ARFeKRFyakI4pFGDQhmzpg+YV0UH3a53TvvCuhg+7WyMmS5pCaPxppNMQxqYac1MNMBppDSmkpjENMNPNNNAiFqiNTN0qLvWkTOQ3FGKWirIG0lOpMU7gJSUtFFxWG4oxTsUmKVwG0UtFO4WG0hp1JRcDPu+hrJcZeta96Gssj5q5a+x00CKUdKktx8tNnBGOKktx8tcNjrJlWnbaVRTsUrAM28UzbzU+OKZinYY3bRtqTFJilYCMrTQKlIpMUrDI8UYp+KCKLARkUYp200pU+lKzAhIpVFKUb0NOVG9DRZgJinEcUvlt/dNKUb+6apIRGBWto8e6UVmiNv7prX0YbZMnitILUiT0L1+u0AVn1e1CVWfg5qiCO1enHY8+W4UUZFGQasgKKWigAoopKACilooASlopaQBRS0lABSUtJQAUlLRQAlFLSUAJSUtFAxDSU6koASkp1JQAlJTqSmAlJTqSgBKSlooAKSlooEJRS0lACGkp1IeBmgBKKoTapHFJtNA1OMjioc0aqjPew/FKOlLSgcVwHSJRinYpcUCG4pcU7FLigBuKMU7FGKAGbfelx70/bSYpjG45pQKdjmlApCG4pMU/FGKAIsc0oFOxzSgUANApripQtNccUDRUZ9pNMN0PWm3I5NZskmzrVJG8Ipo0vtY9aT7WB3rLEhboKepY/wmmWoRNqDVRGRzV5PEYVeprmlVm6CneU4/hNPmD2cDpP8AhJfTNIfEp9zXN7GHbFVzOGlaPeqAdWboKadyZRprc6d/FOzqn61Xbxm+CUtQVHctiuWaFZJPkdnX+8wxn8KY8i8IpBVfSrOWTT2R058bSYz9lX/vqmf8JxLu5tV/76rlWO/kuo9hTSM8A5oIudnH4vM3AjVT6E1L/wAJDIeqfrXCEtuxUsN3LCfvZH900jWMo/aR2v8AbrkdDSHWWrnYL2OUAZ2t6GrixuwyBmlzNG6hTkav9sP6GnDV296zBG4H3TS+W/8AdNPnYeygaf8Aarehp66oxrMCv0K1IoOPu0e0F7KBo/2k1OXUG71mjOfu09mbH3aPaE+zgaYvSad9qNZaSsOq1J55x92n7QXs4mgLhqcJm9Ky/tTg/dNTpdEjlaPaC9nEuecaaZzVY3P+zTTOD/DS9oHs4ksp3ioBCM0pn/2aabj/AGaly5tyoxSEkhBpFQKKQzk/w00yn+7UWRaJelIWIqLzW/u0eY392iyGPMhFN8w0ws3900wu/wDcNFkPQnMhAppmqEmQ/wABpu2Q/wAJosg0LAkLVIfu5zVYb1/gNOaRyv3DRaIyQSc08tnoKqr5n9w1MpYDlKfLERPF15qwNtVFdx/BThK2fuGqtEll2MIe1WFjj9Kz0nK/wVKLxv7lO0SGaIjj25xTAYN3K1T+3Nj7lQSXRP8AAaEoiNbdbtwBUdziJB5fBNVLZmYbiMVNIS5UVVkJjrdGfBc5zU0tuIhuNWIFCKDjmobuXIxVkcpnTvtjYg1lx6g6yHJ4q5qD4iIFYm4AmmpDlBHQQX6SDmrasrDg8VygkIb5TVyC9kjxk1akc8qfY38UVSg1BH4ariurD5TmrMmrDqKKSgQop1NFPFADaKWikAUlLRQAlJS0UAJRRRQAUlLRQA2ilpKAEopaSgYlFLRQIbSUtFMYlFLSUAFFFFAhKKWkoASmyfcan1HMf3bfSgFucpeEm5f60oYqgxSXHMzH3pI45ppBHGMs3SuJvU9xK0TpdtKBxTsUuOKzOAbinAUuKXFIBMUuKXFLigBMUYp2KKaGNIoxTqXFADMc0Ac07HNLigQmKTFOxRigCPHNOApcc0oFIBuKRhUmKRhQMyrzjNYtwM1uXo4NYtwKo6YfCaul2cbw5Iq8LGPP3aZpI/0cVoqOaxb1E2QQ2MatnaKti3ix9wYpyDvWJrmtPBOtlZgNM3U/0pxvJkOVipqmohbw29rbtLIv8KLmsVv9SY/Kladjkg8Kp+nrU90qzSPIXZHC/P8AJgkeuf0qra28BhlM0nzY+VQev+cV1RVjmbuQLlW2l1564NOfbEv7py3uBxSxxRyNtZQoXqaZKkKt8mSKYhgKt0HNBJ9cfhTsp2X8hTTjPRsUBYYfXdmmmpCM9P5VGR7UhiqcdK3tE1oQOIbwboj0b+7XPjg0oNJq407HqMcUEiB0ClTyCKcbWP8AuiuQ8Na01tMtrO+Yn4Un+E1264YZFYSVjVSuVvssf90flTvssf8AdH5VYxRikVcr/ZYv7opPssX92rWKAKLiK32SL+6KPskX90VZxS4phcq/Y4v7ooFnF/dq1ijFICsbSL+6KT7HF/dq0RRincCr9ji/u0fY4v7tWcUYoDUrfY4v7tH2OL+6KtYpcUxFb7FF/dFJ9ji/uirdNNAFcWcP90U77FD/AHRU4FLQBCtlD/cFOFnF/dFTClpoLkAs4f7opfsUP9wVOKcKYrkAsof7gpfsUP8AcFWBQKAuQiyh/uCj7FD/AHBVgYophcrfYof7opy2MH9wVPTlpiuQfYYD/AKDYW/9wflVntSE4FMRk3KJEdqDAqAsqlTTr2X98arSuAoNaI0SL/2oL2qrdXQbotZ814QetVHvmfjFFy1TJrmXepyKy5Ld2G4VZdm2896uQIDCM0iZ6GHh4zyKcJsitaW2Vu1UJrL+7TUjMjR8cg1bgvXj71mtFJGaVJscNVqRDjc6O31BXGGq4kiv0Ncwkg7GrMV06HrxWikYuB0INPBrMg1AH71XoplccGqM2rEtFFFAgooFFIApKKKAEooooAKKKKAG0UUUxhRRRRYQlFFJRYApKWigYlFFFAhKBRRQAUhpaSgAqG6OLdvpU1VdQbbaOc9qT2Kpq8kc2T8zVoaMgOoD2U1mBsOK19D5vZT6JXCz256RNXFLjinYpccVB543FLilxS4oATFLilxS4oATFGKdikxQAmKMU7FGKAG45pcU7HNGOaAG4pMU/FGKYEWOacBS45pQKQCAUhFPApCKYzLvRwaxLjrW5e9DWJcDmn0OqGx0Gkf8e4rR21n6R/qBWmBWD3IYMwSMn0FcFcxNLdzPIjM28txXa6g+21YDq3FcdJdjeXIy7N+QFb0lpcwmyk25j/rHZR6ntToLaSZ8KPwrV0yxN20kw2/Ruhq49jIJB5QWJl9DWjZMY3KkegyFPmdcntUT+H5V9D9K6G1kvouJIUdf9mtFFEiZKbTWfMzXlRxLaRKg5j/SoHsZQfu13pRfSongjIOVFLmYciODazlA+6c1VlhdOoxXdS2kQB+QYrNnsU5OBT5w9mcgRzR2rbvdPUrlQARWM6bSRVxlcylHlFjOD16V6J4avjeaau85kT5Wrzj3FdL4MuzHfvCfuyL+opTWgRep3VFLRWJqFFL2ooAKKUUUAJS0lLQAlJTqKYCUUvakoEGKMUpqGW4ji+8wFAE1NNZlzrUEX8Qqo3iS2C8nmqsK5vZoDDNc8fEtsUODzSWeuxTOdzYosFzpRTqrW1wkyZVgasZpDFFPFMHWnVQhwopBRQIWnCkFFMQ6gUUCmAuabJ9006mOcIaEBzt/LifFUrqc7QKs3YzctWXePl8VaOqKIZZCWqLdSqu56fLbsozQbDgxZRWnDxEKzFwAoFasf+qFDOWruIaYRUhphoRiQvErdqpzWanpWgaY1MRjPbvH0qP7Q0fWtd1BFULiAEUc1gSuRpeL61oWF0xfg8VztwjRtwa1NFJYjNXGdyZwsjq45sipQ1UUOKmR63OUsZpc1GDS0APzRTKXNFgFopM0A0gFoozRQAlJS0UxiUUUUAJRS0lABSUtFAhKSlooASilooAbRS0UAJWfrBxZsK0aytcbbbAepqZ/Ca0FeojnwMtW54fX5pm9gKxUGTXQ6FHttpG/vNXCz2KvwmhijtTqMUjzxtLS4pQKADFLRTqAEopaKAEopaKADvRS96B1oASg0uKCKYDMc0uKO9OpANoIpe9BoGZd6ODWHcda3b7oaxLjqKfQ6obG9pI/0cVqLWbpQ/0cVpKKwZDKGuA/2c7A/d5ri5+XBAwBXf3kC3FtJE3QiuNaMLbuJAQTW9J6GE1qbfh6IfYzuHDVqRwgucjcPftRplqsNjHu4woJqdbq3DnJwKbKiSJEFXgUjVIL20bAEgBphdG5WoLIzzTGWpwRTW2juKQFOUZFU5VGKvyyQgcuv4mqc0sOPvCk0O5l3S5Q1zVyu2UiuplAbO2sTVYMASqPrVw0ImrmWB3rZ8MDGsR/Q1kqPlNdD4PtzJftIRxGtaS2MY7ndDpRRS1gbBQKKBQAtFFFABS0UgoAKDRRQAU0naCTTqr3efJbHpTEY+sa59nykXWuUu9VuZ2yzkU7UXP2l9xrMduTWsUZtj5J5G+85qAk+tKeabxVEic09ZXT7rEUnHamgZNAHQaJrUkMgR24rt7a7WWIMDXla5UgitzTtaeIBGNQ0WmegxyBqlzXPabqizEc1siXNSUWc0A1EHpQ1FwJwaM1GGpwNMQ/NKDTM0oNO4D6jlP7tjTs1FcNiJvpTA5q7lCztWRcNmQmtK7Iy7GsmVtxNUdsELEQXq67Dyeazon2yDNWrh/3YxTRbQ2PmQVrr/qxWPbHdIK2B90UM5Ku41qaaVqaaRiNNMNOJppoERtVeWrDVXl6GkyomPe/erR0VcYrNujl619HGAKunuFX4TZFKKQUtdJwkqvUqtVenK1MCwOaWo1anA0ALRRRQAUZopKLAGaXNJRQMXNGabRQA6im0ZoELRSZozQAtFJmigApKWigBKKWigBKxtfPyIK2qwdeb96orOp8J0YVXqoy4zg10ujD/QQfVjXMA811WlLt06L864pHqVvhLVApKUUzgDFOAo7Uo6UAGKXFFLQAlFOpKAEpaKWgBO9Hel70UAJSmig0AM708U3vThSATFIRT6aaYIy74cGsOf7wrdvuhrDufvCn0OuGx0Wlf8e4rTUVmaT/AMe4rTFc5mxdtczqlq32+O3A+VmBH4munqjdQrLqFse6tWlN6mclci1G6ljUwxLgetc7ezORtMoX+ddPqUTsjCIfOehrLsdMjijuBdLullUqJCN23PeruFjn4ZHhk3Biw74rodO1ESgDdmqdrpzR3u66ZZUVmYk4JYkY61dstPWG4d1X5SeM9aTKijTLkLnms3ULjMZAbBrVuHxFisK7XzSQOtQVYxZ5J2chZWanxLMp+Z8fWr8OmLJZzBztmIBQt0BqO004Dc1zuQKm0beuex4rS5FhqidPnUbh3ApZUFxbupB6U7TvNjcrIpxV2SIcmpuVY5LaAOfpXX+CkIgnfsWrlblNlxIg/vnFdj4SHkWTRSfLIzbgvtVzehjFanRUtFFZGgUClNJQAtLSCloAKSlooASiig0AIaz9XuPItGNXmNYXiSQ/ZwopoT2OXNvJeSMwqpPYyo2MV0unosFrvYc1XknjkkO7itW7EJXMSLTXdcnikfS5M8V0Kw7lynSlEJHUVHMzRQRz66YwHzdaja029q3pFI7VSkGSaOZhyozDFgdKrsdrcVoypwaz5B8xq4mctDQ0y7aKQc12FtdFowc15/G2GGK6/SZN9uBWVXQqB0EU+etWkYGsdWK1cgn9azjIpo0AaUGoVkBFSBuK1uSSZpwNRA08UwHZqG6bELVLVW+bbbsaaBbnLX7nLVlseKtXk+9iPeqhbirPQgtAHWrDsDGBVTNSDLUFNFm05mFbJ6fhWPYD99WweKbOGr8Qw0w080w0GI1qaac1NNMRG1VZuhq01VZ+hqWWjHuOZK3NJX5RWHLzKK6DSh8gNaUia2xpClzTaQk11HCO3Ck30yigVyYSU5ZPeq9OWgLloSUoaq4NPU0Bcm3UZqMGlHNFhjwaWmU4UDFopaSkAlJTqSgBKSlooASloooAKM0UUAGaXNNpaAFrndaO66xXQ1zWpndetis6vwnZgl+8Ka42111iu2ziH+yK5Pachcda7CIbYkX0Arhkd9fYKUUlKtUcKF7U4dKbTh06UAOopDS0AFFFFAC0lLRQAnejFL3ooAKDS0hpAN704Ug604UAFNanUjdKYIyr7vWHc/eFbd/3rEuPvCn0OuHwnRaT/wAe61prWbpP/HuK0x1rnM2KOtU4E/4nLdcY3Y/DFXB1qNcDUAf9iqgQyxKnOMVA8IPWrW7LGgrmrZSKBtE3c80hj+bCjpVxiEpsexA24/M/6CgDPuwRHWYOTmty5SJlY7ue1YzxMrkr0qSyzDGrLginm1/u0tvtIFW1GKBFE2m0dKrzLha0pm4NZ9x04/GkBhRxJ/beZBkFd3PrWvbL5d/HICc7sGsq4Rnv4tjFW5wa17T554geueaqRKOlHQUtIOgpaRIppKU0CgAFLSUtABRRRQAU1qdTGoAYawPEG7auBmtq5l8qFn9K51Ll7ydg/QVSEx1u37lVdaralDCMbfvVoyMoXGOlZssLSSZ7VpcmwW5eHbk5Wpbi8GML1qB0KYHaoAw+0e1SXqRTSXDPheKhdJl+ZquTO27cBzUEkjiQbuhp2JuVydymsyfIcitWQjcSBxWXcsGkoiKRHGCXGK6rRgyoAaxNJtvNmBI4rrYI1RQAKyqy6DgiY09GxUdOWsDQuRvVtGyKz4yc1dj6VpFiZYU1IKiWpAa0JHCqWqPttWq4OlZ2sn/RGqkOO5xc3zSMfemMMLSOfmNK5+WqPSSIxU8fOc1DENz1OVw/FMTLmnL++rWIrN0tfnNapFDZwVPiISKbipSOaYRRcyISKaalIqJhVCImqrcHg1afpVO4PympZUTKfmYV0WnDEdc8OZxXR2IxCK3omeI2LeaSiiuk4rhmjNSww+ZU4sqhzSKUGynSg1c+xGk+wml7RD5GVAaeGqx9gNKLE0e0Q/ZsgBp4NTixNOFkfWj2kQ9myAGng1KbTauah6GmpJicbDqKbmjdTEOpKaWpC1AD80ZqIvSeZQBLmiofMoElAEtLUHmUvmUATUtQeZR5lAEp4FcvfP8A6W7D1romk+U1zFwczufesa2x34Fe82Ohy9zGPVhXXgdK5LT033sQH96uuUc1xS3OuuNxSgUtKKs4h0abjV+K03L0qpb/AH63LX7lAmUDZe1NNl7VscelJx6UhXMc2Z9KQ2h9K2do9KTaPSgdzG+yGk+ymtravpSbF9KYXMU2ppPszVteWvpSeUnpSAxPs7Uhgatswoe1NMCGgZieS2aXym9K2PsyU37MvrQBj+W3pSMjelbBtlpptVoGcnqAIzmsO4+8K6nXIVjBxXLXP3hT6HXD4TpNK/49xWiKzdJ/49xWmKwMpDhVd/kvEP8AeBFWBVa93K0TgcBqqJJaD804vxVbccZpd2VqhoJGLHHasHUXmi1FHW+8qL+7/wDWqzqWrLaExxrulP6VgPbX1/LvYcHu1UkJs1rm/kMbMsqFP7w7Vlw3bJPuju5HBPzLK3FQtpd8smxQHRjyytx+NNm0y4hYMm1hnqpzT5ULmZ0sMmFBq9HNlOtcna6nJA4jnTj2rbtblZU3IcrUNWLTuXZXyKpTn5TU7txVS5b92akZjuxOoRBeDnrW3p8RN2gXkA5NZFvC0moJtXd7Cup0y0aIs7jBPSqkQjSpabS1IgoFFApgLS0lLQAUlKaKAEprU6mmgCjqI/0R6wtLTlvrW7qP/Hq4FYtkPLRqqIE8y85zVYsoJycCm3F1tJzWfPOJVIANUFzTliVo9wINZmCs2e1V2nmRNgY1FbyuXw79aVg5jXeMMu4CqkifNV63dVQKTmo7kL1FIoz2Ax0rPktTJKdtaL1HG21jTTJaLGiRhGKnrW8OKyNKTdIXrXrCe5SHU9aYKctQBYh61fjHFUYV5q8nStIiY8U8GoxT6sQ+snXWxamtXtWJ4hfFviqRVP4jkm5ekenKMmmufmqz0R9sMvVoDqaitVAUk08tiM00RIv6WOSa0zWdpQ+StA1LOCe400w04000IzIz1qN6kNRNVoRE/eqN0flNXpKz7v7ppMuJnxc3ArprQfuRXNWwzcCuntxiEV0UTHEklFLRXQcRessCrtU7RMjNWs4wK457nXDYcKWmhqCeKgoWlFM3UqHNAxxbaM0sbbhTGzmlTOaAFlcbCM81nHqavTRfKTmqPc1vSMagUmaKStjIDTDTjTTQAw0lKaSgYUlFFABRRRQAUUUUCGynEbH2rnJOZG+tdBOf3D/SueP3zzXPWPTwK0bL+jJnUE9ua6qPlq5rQRm8P+yua6WL71cctzav8Q2lFJilArQ4yaD74rctf9XWHB98VuW3+r/Cglk1FFFIQUlFFAwooooEFJRRRcoQ0UGikMSkpaSgBKSlpKBnOeIjgGuSufvCut8ScA1yM5ywq+h10/hOl0n/AI9xWkKztKH+jrWjXMZSHDrTbhN0DCnCnN0poRRR8qKfn5Tio5UMch/unkU0PgVYGC6N9sLOpY7umOtW0iu7lgrlLdOgU8/nVwwqZvMI5pJ51iBYjKitExWKbWFwqMgmjYH8Kry2lzbru81f+A1K2sWwJ+9UbajDNjCsaoLmfdLNKm5ogSvVlq1o25dwP3SM1dUK0ZyOD2pYEVF4rNsaROW4xVe46Yp5IqvMxJCj71QNlvQYd928v90Yro6o6Va/ZbVQfvHk1eo6khRQaXtQAlKKKKAFpKWigQUUUUAIaYxp5qNqYEFxH5sZWsaWLySVrdrK1OMg7qcQOevw2/5e9OtrVzGT0p8o3SVO2RD8p5FaklWS3ccnBrOmT5jxg1eaWbODUbjPLYpAVYrmRWCGrzOSvNVwibt1S7s0mNET1Fg9qlc5NWbW0Ljcaluwy3pibIsmtAVDCmxQKmFYN3KHCnrTBTxUgW4atr0qhCeauK2a0iJkop9RqafVIQtYPiI/ucVvdq5zxI/y4qkaUviOfxtpmNz0qKWp6fK9aHeTKNsRoc4iAodxtxTWbcAKaIZp6XxHV4mqenDEWatk0NHny3G5prGlPWmk0WIGk1GaeaYaoRFJ0rOuz8taEnSs276VLNIFaz/19dNCP3QrmrEZnrpYh+7FdNHY5sTuSUlLS1uchctZlQc1N9pjrNxQBmsnTuaqdjT+0R0faUrP8vijZU+yRXtC/wDaI6VbiMVn7PelC+9HskHtDSF1H60ouozWbsFO2Cj2SD2heluEZMCqeabspQKuMeUlyuFFFJVEiGmmnGmmgBhpKU0UAJSUtJQAUUUUAFFFJQBDeHFq9YBHJrb1Fgtq3uaxMZNc1bc9bAr92bHh5MTyt/s10UPesLw8OJm+grfi+7XHLcqr8RFThSUorU40SQH5xW5a/wCrrDg++K3LX7lAmT0UUUhCUUUUhhRRRTAQ0lLSUhoSlpKKBhSUtJQAlJS0lAznPEv3TXITfeFdb4nOFrkZDlxVdDsp/AdRpX/HuK0aztLH+jrWjXOzKQ4U400VIiGRgopohla5AMbcVQWQNnnp1qG91R/7aSxXiP5s+5ANQXO+M+Yh47itVEVzQjZWOKJrZJEIPQ1iNqDQ/OvKd8VI+sIVBB7U7BzCXOk27EhDjnrTLewRT8vUVWfU8v3xRHf7c4b9aeoXRsCPApjOFYrVWG/DLyarJcmSVmbgDiosVzF8txUNs/najGi8gOM1nXN4zN5UPJNXLHZYlJX/AITuY0WFc7QcUtMjcOoYdDTqkBaUU2lpALQKKKYBS0lLQIWm0UCgANRNUpqJqAG1DdRiSFsiphzWdq92YISqdaqKuxNmFcL5cpx0qJ7gbetKZjIuWqlPgng1qQTNOMVXkm96gYN60zBJ5NAXJRJubFSl8DFQDaKs2tu9xIFXpRYdx1vE0r8CtmCPYgFWLSwW3j5HJpHXaTWEy4sQU8CmKafmsihwpw60xacp5pAW4RVpRVaCrS1aAetPpq06rRIvauZ8Q/NKFrpT0rmNcObkVUdzWl8RjhCj+1DD5+KlkbmolbL1qdo1l+an4psjfNSg0IUtjZsB+5qwahsuIBUxpnmPcYaQ04000yWNNRmpD0phpiIJehrMu+lacvSsu8PGKzZrAZpozPXSoMKK57SRmWulC/KK66Wxy4n4htKKdtoArU5hMUqrzTwKcBQNCdqKWikMSgUUUAKKcKaKcKACilooAaaQ06mmgBpppp5phoAYaSlNJQAlFFLQAlJS0UAFJS0UAUdVP+jqPescVrasfkQVk57VyVfiPawa/dI6Dw+P9GkPq9bkf3ayNCXFhn1Y1sxj5a5XuZ1fiZDSjpTaUVqcqJoPvitu1+5WFAfnFbtr9ygTJ6SlpKQBRRRSAKKKKYCGkNKaQ0hiGig0UhoKSlpKAEpDS0lAzmPFJwK5J/8AWCus8V9K5Nvvj61XQ7afwHVaWP8AR1rQ7VQ03/j2WtFEZ/uqTWJhJgoycDrWnDB5SYwN2Oc020tfLYM/J9PSpJjsjkcx5H1rWEbas55zvojznUXMPi9Gb7pz+uRW20fHsay/EtqfPt7hfQoT+OR/WtWxl861Rj1xzTky6eqMe+05ly8H3T1WsxrcOcHKGuwki9KoXNmJP4cGlzFuJzy2WVOGyaY1jJ/Cc1qSWjRt1wKVIz/EaOYXKZ8EPlck802WRidka8nvWk0JYbVFSW9mqnOMtS5gUSrZWQiXe3LnrS36kw+WOrnbWltVFqtEnn3uT92MZoTHLRHURELhASRt71PVdTuMfz7jjvxUqv2PB9KJRMoSH96UdKb3pw6VBqLSUGigAooopgGaXNAUmlK4GTQkK40imlT6VKozjFJcNtjarUCHIzrrUIrXhjWPfyrcKXXvT9RtmmRmfgDvRDaK+nZU5Na01ZkydzIgAbctRXNq2MrTQzRXToeKtCbIweamW5aWhjvFID3poim/umt2OLzT8qZq9FZLGu51yfSmhMxNP0ie6fLAqldLZ6fHaqAo+arFpNHIgiQBTVqNPnZ+y0cxBD5ReXHYdarmAyynHSrwyImbu1Mtl2l2bpU2HcrfYjmo5LV16Vphh5ecdTSsPmCgc0nBMakzGwV605OtaDwK56VC9rtOVrKVOxopDoaspVeNSOtTrxSGTLS0xTTgaYDj0rldZ5u66lj8tcpqjf6YaqO5rS+IzZPvUiJ3pZPvUsZwprY7CJuZBThw4pv/AC1qQcyChCnsbdoMQipjUdsP3IqQ0zzGNNNNONNNMkYaYakNRtTEQTdDWReHk1rTdKx73g1m9zamWtGX94K6UDiuf0NctXRCuun8JyV/iExSAU6gVoYWCloooAKKKKAEopaKAFFLSCloAWkpaSgBKQ04009KYDTTDTzTDSAaaSlNJQAlJRRQAGiiigAooopiMzVjyv0rMHJq9rDfvQPas9WxXFV+I9/DK1KJ1mjrt0+L35rVThaz9PTbZQj/AGRWiv3RXL1Oae7Mr7copRfpVMadJUi6bJWnOjDlLlvfIZAK6axkDx5FclFYPHIDXTaYdseDRzXE4mjSUUUXJCiiigBKWiigBDSUpptIYUUGkoGFFFFAxKSlNNphc5fxX045rnbbTrq8lCwRMx9ewrv7nTYb1wZTkD+H1qzFBHCmyOJVA/uiqSNPb8sbIzbDSvssMYuG574rUij2HCY2GnBMYBctmn4C7j0qlFI5pSctxM8E5JHtVW72i1b7y5qd5FVBmUAmobnc0ICuHB/Wgi5z+o2v2i1kQgbgmV98c1maY/lExt0PSuilj27xt7Y2msB4fLlO3sazmb0WaamnGNWFQQvuQVKGIFZnQQTwcHIqi1uM1qGTioJH9hRcLFRYhT8BRUnWmMKQWK07/KalsoCkIYjmQZ/WlitHu51hQct/Kty304CMFuQBWkEY1ZdA+by4vmVuOlIwCzNuX3wavv8AZ2RURoCy8bQwzUNxBtmHG0MvfpWpzXIVyF3seD0qRWyKqMMxMGDErzgVLuykZwy5qXE0jOxNRUauSSNpAHengjNZuNjVSTHAdqlSPuaZGNzgVYcfMAKcYhKRG52DimEEkDj3pWIeXJBAWlGOvrWiVjO4IcZzTJV8xtvUVI2AM0RDapfHNUIzb+3MpEK4x3FRzW/kwIkQxzVxRlmkYEE8CpHiUgZB45pAczrmlDyhcxj5+4rNtYfPAA+9XYPEr28u7kGszTdM8mR5j68LUtWNIsktbKOG3AbhzRMpAKrzVqT942COakEKxxDKklqpeZLZj/2ZcRqZ4ZPmParltLMluVkGfU1pmNViyvamFSIQCgy1JJWEQrMk3lqDge9Dkbtu3qe1PlgQuiBMEdcUBE84tk4WgB20FwoBwtC4y7k4pobCswbk0rbjGqcEtQAmdsP3sljTj8uE4Jpdv75VK5C0wkEu+08UANIUNS44pgBWPcOSxqQq20YqXFFKQClBqIMe/anK4NZtWNE7kjN8hrk9Q+a9aupkOENcldP/AKW1OG5vR3KkuQ1JnC1LNgmoeMGtTsQkQyxNSRDMwpIhwTToP9cPrTRFT4Teg4iFONJF/qxSmmeYxppppxpp60xDT0php56Uw0xFWese8OWrXnPFY94fmrPqb0zW0Ja3qxNCX5K2xXZDY4avxC0UUVZkFFFFACUUtFACUUUCgBwpaQUtAC0UtJTAQ0004000AMNMNPNMNIBppKWkoASkpaSgAooooAWiiimBh6od10R6VSAywFWb983T/WoIF3XEY9WFcE92fQU9KaO1t12xxr6KKuVXjHIHpViuY4mLgelFKaaTWYDJDVmykwaqPUkJwaqImbqNuWlqvbSZWrGa2Ri0LSUUUwCiiigBKSlpKQIKTFBpaYxKTPbNP2cU1iFxlMk+lUokuQh4GSMChctynA96XaVYfNSEKJeWOW7U0iHIULnOcD6UyLYQyjJxT12q5UKfrQm7LfKAKZIK/wAuSMAcCq8xkkhYnjngZqSJXZMyMOBgYpCkXlHJJFAHL+IHf7OqbhWXBLPC0eJiv0NdHqUNtJt3Jn3rOu7S2VEMaHNDEbMUn2nTo5H+Yt8pYdjWbeW7rKx25wOcVHpl4UlNo4KRucq3ZTWpIHS5OSVOKTVzSMrGPCwV+tXNny5FJJp7TKZYtu4c4HenQ5CYbqKxasdUZKQwoajMZz0q2KXAqSyn5ftUTRkmr5FS2lg05LtxGv600rkyklqxdFtQkjzt/dIX39ayvE99LOyaZaxs7KN8pUd+wroLy5S3i2oQJGXag/rWfawIBI7uS5HWt4qyOOUuZ3OTjgktbTYU5LZNaVnq89q0aAM6f3G5H4VrXFlDNAPnGazLvS3jmjMcoK/rTINlGSRN8ecNnHqPaiMhUyCxZT3qppvyTPEznnnFaSKQHCSfnQMa7SNIwRMZHbvTE3CLcGWpYEl++HXilWFvKZyocZ7UDuLDKqEFwfm6VZbGdyvx0qtJGnlodjpQEKzDY27uM8UWHzFjaPmwM0hUA80wTsQQ3BzzmpcksW2gqooHcjcbnCBuByaWQgpjPWnANs5QbnNLKSqFtowvFAyALl1QMML1qQltjnI54FRjCxcodz0/bGWVOcLyaAI5wVREwOetNS3CkyBuB2qSQkB5AuV6Ckk2bI0Ktk8mkMi8tm28qGY0XLssgGQVWpB5bXBO0/IMCmPG4lVVAO45NMQSlNiKCQWo+Rpwu84QU8b3uC2wYQUxSwikkKAk0vIBqv8ANJJv4HApmGWDJcbnNOfAgRPK5Y0PtadE2EBRRcY11O5ExmlyjT9CNtMQq1w7qxG31pqM4ieThs0AORv9Y4emsdsAy/U0jZECjZyxolC7lTaeBT8xD4gjfOWwBUm8tlVFRr5e3YeD6Uiu5HyjGOtTuMTy+W3N1qq5Mb8GrjBQ3J61XuEDLkCpmtC4sje5HlHca5qchrhiK1b5ttuxrCjfcTmppnZRHSnFRZxSynmmkcVqdKJYW4NPt+Zx9ahj6GprPmcU0RU+E34/9WKDSp9wUhqjzWNptOppoRI01G3SpDUbdKYFS4rGueZK2Lg9axpzmWo6m8NjodEXEVa1ZujjEArSFdkdjz6nxC0UUtUQJRS0UAJRS0lACUopKUUAKKdSUtAC0hpaSgBtNNONIaAGGmGnmmmgBhpKU0hoASilpKAEopaSgBaO1J0pGOFJ9KARztyd1w+PWpLBN19AP9sVC5zIx96vaSm7UoT6c1wT6n0W0Dqo/vVYqGL71TDrXOjgYGmmlJptZgMbtUkfeo2qSPpVIC5bPg4rRU5FZMZwc1fgkyMVomZyRYpaQUtUQJRRRTAKSlpMUgA09VwKQLjk0vWtEiWxHYAc9T0xTW3GMY+U0p557jpQfufMaogjcI0i5blae2PMHy5PrTUKldyr3pzbiBt4pCFO7dxjFIFG4knNLxv5POOlC4ycCgBke0IdoJFB3GI4UA0sbMwbjGKUqTGQWwfWgCjPayyRgYWo2sXZVBUVoFf3QBf8aY64VcSYFAik2m7ZVdVWppbbcpT5S2ODVtgCV+fH9aa8SGQEnBpjMkRmJNrjac9e1OubYrGsifMT1x3q+1uvmEbiQe1VDJGb0RhjE47N0b6VLVxxlyu5nj604mrV7EzzMwXDDrjo1Voo2ndUTktWTjY64zTVyW0hM8oBzsHXFa5HlnkqsfQCoDNb2MAjd9g7H+Jj9Kp/brmZjFbwYX+/Mcn8q0jGxzznzMdLEq37yGPeNo2n2qJGQxv+7NSwtcEtuIYgYzSRCcwtwKsyGMYxAuYz1phCNMhWIn61baG4Nuv3etSfZpQUJK5pAQRRQi93+RhsVbjWEu4MeKeEkFwDxjFL+8Dt8oIpgVGtovJcpuWooFRIWQuQ2avo4MLFo8VDdJH5KyKnI/lQMCJPKjCurU6QbpgHi7dqZiJnj2ErU6K/n8OGGKAKixhvM2Hcv91qapPEZOxWIqwADBKXTDBjyKpyAtgk5Qcg0hlzjzGO/hBimSJvVF39TmmQyo0AUKS7Ng1M7IC+V4UUFDEDtMTkFEFAZxGzED5qYoRbXIYguakZcGOMtuHehjGOj5jjRs55NITI07NhcIMU5djTO4bG0YpI1C4Byxbk0gI04cI7fM3JoUKZ5H8zhacjqZ5WKHAqIMotmbYcsafUYLhbd28w5c0SLhI4xJ1pZCoEUfln1pG2Ncn5DhRSv1ACGa5ADjCCmoX3SybgajUqqSS4bmmExrbgZYFzTAUlktWZkBLVHLsEcaLuUmpJdmY4lduOTTQ4e5+8CqjvSuAp+a4RRJ90U5GdpnYgMBSRMheSVl47EU2MBYmKP8zdqAEcoVEo5YHpTwzOQVGFaol2xTbHX7wqRQ7KynjFACgKqkNyy005YcDg05tihXHJ6Gghj7DtQwMHWgYoGrnI3PNdXr1v5lgzLywrjwcCojGx20XdFhmzzTlbjFQhuKUNVnSmSbscVasBmYGqa8mr9gv70U4kVX7puD7opDS9hSGqPOG02nUwmhCEPSo2PFOY1E7UwKd0flNY7HM341p3bfKaylOZx9ahbm8XodZpQxbitAVT00YtxVwV2rY86W4tFLRTJEopaSgApKWigBKcKSloAUUtIKdQAlJS0GgBppppxppoAYaYaeaYaAG0lLSUAFJS0lABRRRTAKZMcQuf9k0+oL1ttnKf9mk9ioK8kc+fv1qaCC18PZSayl61t+H1/wBKdvRK86ex9BU+A6KHrUy96ih71KOlYo4GMNNpxptZjGtUkfSo2p8XSqQiYVPA+DioBTkODVIk1I2yKfVaF+KsA1aIaFooopkhSikpw6VcRMORTeoyvU0EkZ79qYzYLMvJAxirMwkYEEjqpH86VSmHA+bHWop8tDlOGyCakRlDbR1PNAgRiYcovPpTsEoNx5702Muyvkbey1Ddy+TAAW3OTSAWW6WOYIi73PpTokmMpeT5R6U2CI/K7KA38qs/xdaBiKu0nnrRgbDzxQuOcUZXYTjigQ0hfL9qR9nlDI4pwI8vIXj0oYkIML+FACNs2rkcUOVDrkc0rM2Bhc0OzBlwuaAAlQ4GOagnhguC0U0QbNTlm3gBaTLebjbx60AUreA28ckSuWx03+lVT/o6hoRsMh5du30rQuXYK4C9sZqKUbwgMXDYJ4oDUoPaxLdBjKzOf4m5NTRQbrg4lP1q+YYvPH7vmnII/PbC4NMRVgtOXzKamhgRYT8+fepk2bnwtNjKeU2F4oGIyJ5IG84pzhCEyxoJXyQdn4UOV8tcx5oAVlUzL8xzSbR5pw/alYqJV+XmjKeacrzQBEhkWJiCGGae7ny13JnNMCxssu0laRZJP3Y3BucUAMlZY7pFKfL1FTRBGdnRsUOzNJjZyOhqK2lQpJuXaaAHIXWBzwwJqnzIHH3UQbhU2WzGiSfL1I/GopwWkY9EGV+tJAMt5QlwGx8g45q1vYwOxQfO1UOZEGBtUDFW4JXm8uNTwp5o8ykPk27ooynvUibfOZgOBxSKz/aJZGRSq8D8KjHmfYy/RpD0+tKxRJIERMKuSxoDMZyFXhRRiQPEnHTNNQS77h9/HPajcCJDILeVto5NJIsuIYtq/wB407ZKtnGu/wC8wqRhKbxFDcBPShgQ7pGuicDCcVCsknlTSlAc5xU6ebm4bcOp7VBiZdNIyvOO1HUYxmkFooaPljTpG/eRJ5fSnz+d/o64X72aC0v27lVwq0ugEYkja7bKYAFRDyDDK3TJqUSEJM7JUbFDDGhTG7rTAaY9iIkRyDyRVSWZI5fMB+bsKsyFVdmBOB8oFZ11zIqjBXrSSsM0i7SwiYr81PYMxWQnAbrRAyrCiJyWFJHEzI6SN93kU1oIcuxHMfXPSm4dl+bjbTt48sMgyy0rqXw7HAPWmIhljR42Q87hXB6hAbe7dO2eK78kLwBk1yvie2KzLNjhqVjooSs7GLngUoNR5pymg7UyVDWnpvMgrLU4rU0v/WU4kVfhNo8UwmlZsVC8gFM4Rxao2bFQPcgVWe4J6U0KxaeYCq0tx6VXZyaYTVWASXMlMith5gNKz7RTYbjdOBTikN3sdRZDbAKsioLX/UrU4rpOJ7jqKBRQIKKKKAEopaSgApRSU4UAKKKBS0AFJS0lACGmGnmmGgBhppp5qM0ANpKU0lABSUtJQAUUtJTAKq6icWb+9WqpascWuPU1MvhNaCvUiYa53V0Hh4ZMzfQVgJ1rovDw/wBHlb1avOnse5V+A3YulSjpUUf3alFYnAyM02lNJUFDW6VJH0pjVJHVIRIOlFL2pKoRZgfmriNxWdEcGrsbcVSJZY60U1TS1RAd6cSB1pB3pHbsw4JxWkTOQAFcAcgCoW+ZC0f3ieRUhJAZkOaYW+VcDa57VRAOQ7HsVHNKjfOcLyx4PtUCsZ4Mn5WPB+lTRllYRqPu96QD1D723HjtVS3UzzyNjKo+Nx7kU+5eXzPKgOXb7x/uirEMYjhVFG0CmBIR8w5oGMnFJ8vmf7WKUHg8UDEUjaSBRk7Mgc+lL8233pCGKYzzSADu2+9I4cqNpwacQcDmkdc4+bFAgYNkbTxSMH8wYI20pHzg7vwpjbfOGW59KAH4bf14qP5zK2GGKbtDTHD0QxAMx3k0AQskrCT5s8irDCTYuMVH5XyMPM71I6/Ivz496AFO/wAwcDbQC/nfdG2ldcup349qaVPnZ3/hTAcpfc2VGO1IhkKtlQDQoYSNls0kasA2WzSAX5/KPHzUjGTy16Zpdp8pgX/GmlT5QHmfjQA5y+5NoHvQGbzcFePWkdWymHpcP5w54xTAjUjdKClRMYmg6lGDdanHmea/AxVaVsRZkj/i60ASRvIsjbiG4zUPmlRlk+8eakkVWk/dPtbbUTTOIURl56g0ARu6idQgwWG2mlCyy7m+VG496SSTfcR5XbmQZPtQyhJW3t1XKrQIYVZw+flRTnFOtZRHONvCdM05lLSEynAZelQHLjgbVxTGXj5qWTtv5c9/enSRr+4jV++7rTF2vbQgFsb+R9Kk/dPe46bF9Kk0HbCbpm8w4Rcdah2AWTkycyH19aB5Qhmk3fep0ixBIIvfPSgAkiTdBHv6fN1pqopvJHEnC8daduhN43+yoHSq0Zg+zzyFhzuoAERhZSuJD82e/rSSxyLaxIJDywpskcQsVUOPmIHWlniUywKH9T1o8/UY6RZjdQrv+6uaE877TO24HHtSLFm+YiQ/KoHWo41cQzyeYec0v+AAm6UWZyAdzUrlvOUFRwtIySC2jXfzkU5o3aYhm6LQBWLZTle/NZcgMd4D/Ce1bHl/KmWrM1KMKofd0poC+A32dGj4KmpmVVkSV24brTLTL2aR5wCvWhAnlNGx3MtICQMEnKIvyv3pAhIdHbpSFnkhR1G3aakIWOZXZsh6AI9wKgoPas3WrQ3Ni+eq8itXo7Iq8HpUTRhlZXPtTHF2dzzggq2DTlNWtXg8i/dQODVNelI9GLuiVa1NOYJyaylq/B/q6qKuRV2NKS69KqSTFqjJpNpY8CrUTlGls03JPSrCWrNVqKzA60yWygsLvUotiBzWiIlXgCo5RwaTZKMe6j2qarWK5uh9au3v3TVbTRm6FTH4jZ/Addb8QrUopkQ/drTxXYecOooFFAhRRQKKACgUUlABThTacKAFpaBRQAUhpaQ0AIaaacaaaYDTURqRqjNACGkpTSUAJRRRQAUUUUAJis/V2xAo9TWhWXrR/wBUv1qKnwnRhVeqjNTpmun0NNtl9Wrmk6V1OjDFhH75rz57Hs1/gNRPu1IKYvQVJWKOBkJptONJUDGt0qSKo26VJFVICWkp3am1Qhy9atQtxVQdasRGmhFxTxT6iQ1IOlWiGO7UzcwYBhkYzmlcjGDxx1pvzqWI+YY4rVGLGNyoMRxluRQ7AuFYYYdDTSBJt2/IRzTgzBmMg3KO9MghQs0PlNwwOQakhLIGMhwB82faoA581O8bqR9CKgLs6mIuNpQL+uDSGWrAZllm5/ffMuey9quY+UbjUMcyiHccIijBPpUcIa4CSSoyrn5EJ/U0AizHIHZtq9P4vWnjcVOetALbjnp2pAG2nJ5oGLg7cZ5pGGVxuxS4+XBP40jBdvJ4oAGA4yaR9u5dx57UrbeM019m9dw57UAKdu8ZPNR7YzOT3qUld445pu5fOxt+b1oEIiJvbHXvTV8pd3P1p4dd7DbzTYtjM520AMHlmM8nGafIE2LuJx2pitH5bHbxup0hTamV47UALJs3rk89qCEM4Ofm9KWTZvXIoynngY+bFAAqp5pIPNJEqDftbPrTlKeYcD5qSMp8+0fWgBv7vyn5+XvSfu/IHPy0oaPymIXikLJ5AO3igAlCFUy2B2pWVfOU78HFNlaPYm4fSlYxeauaYgRW81/nzUMvmi2bODg0BUM0jK+DUbq4tTh8gmgYGWMyEkbSFqIyeSqu3zp2pry48xZU+b7oNRSlI/nzuUcbaAI0j3xh2f7oz+fapmQbkIO6TaabbqjRsrHBx09KkgCuvyHG0csaBC/KkkbyNub0qFgWALnavOBUuUWEFV3PnrUZVVClzubd0oGT6flpCVXIFWRmMTSSRkE596p2krxyNzsynAqyzzfZF3KpZ2HTil6loQlFgiQI3ztUhfN2q+WcKtDFzcRJsXCrmhHkNxM21cCl0GQrMMXEhjbqaru8a2GCpy2B0p8kki2TfIpLUk7/ALuFDGeWpjGTGFjDH756UpSFr4DcPlShnQ3iDY3C+lOjkh+1TOQeOOlT0AZEkJlnk3jr61EY0FiBv++R3oEkK2bnHLnjj1pCsReGHsPmPFUBJJEvmRIJOBz1pNqlpX38dKUCD7RKxbhBimZj8kKAfmNIBAkY8sFveqWoIjWj49DV9mjEjfL90YqCYq0RXZ1WhAR2nNjbyF+AKthkS4BRchhVDS2Q27wuDlT0q8HL242J8yUwHqjs0kTHCnkU1QhgK53OlK+d0cztwetKGSO4O1chqQCl2kiV1GMUx0AfJPWnpvJePoKZtUR5JyVpoDmfFFtkLOq9Otc2td3qsRubGVcdBmuEI2sQe1DOyjK8Rw61qWyF0GKy1rodKjBjBNVAdbYZHaMTzVqO1UVb2ikqrnI2R7AKKcRTaCRrVDL92pzVebpSY4mRf9KZpK5uBS35p+ij9/mlTXvG8/gOqQfKKdSL0FOrsPMCilooABRSUtABSCgUtABSikFOFACiloFLQAlIaWkNADTTTTjTTTAYajNSGozQAlJS0lACUUGkoAWjFFFACVlavzKg9BWtmsfVObj6Cs6vwnZgleqU0HNdXpi7bGEf7NcnCNxrrrPiCJfRRXn1D0sQ9EaC9BTwajWn1mcZHSUtJUDEbpT4qY3SpIaaAlFJTqQ1QgFTR1CtTJTEWUqUdKhSpRVIhiyHAIYZHFJghXKN+FDFgfl+YZFNby2jY5281utjBjXy2xXGMg/NTId627Mp3jpTpFk2JtIYVANqAoj4YnoaBBMT5aPENrqc7fWs+QIFMucZ521p3DEKiSL+IrOnTy0kjBWRScrnqKQixBfJcylDGRHBtJB/iatATEg8dGrnLUTfb1WN9m5dxBGQSOmfTvW9F5i/M0iEheFU9aaGWhu3n0pQp2kFqh+csj7gB3qVVAZhu69qQIUAeXjPFI23YM9KRduwjORSkr5ecZFACsVwMikdlDLkZpWYBQcZpHYjGFzQMGbDgYpC373G38aVmYMABxQWbzAAPloEAY+YV2/jTUkPmMu08U75vM6fLSLv81sj5aAIQ7Yf5OQalZ8IhCZzUb+YBLjjpinuT5A2D5iBigBzsQyjbmkLHzgNv40SB98e38aQ+b54xjbQA4N+8I2/jSRtnf8ALjFKvmeYcj5aZG7+Y4ZeO1ACq5MJIT8KTe3kg7PwpyN+7b5cYpgkPkE7TTAJXwqZTrTZWj85dy02abEacHNI8q+cuQelAiIeSzTAHDfWmvCGSJUk6mm74iJsocZqJjFvTh+mTzQAtwsi7nb51U4FUi8e9Cc9eQKkkWVl3RMy85+Y5BqvHJKy7gir8/SgC4kkYZsoQMZ+tPh2/LvO0Y+6KidpPNxsGccmrAKLOnlqXOOpoAcjboCgXam771RcLxEu47+pp4ciTbLyC+dooO9o3KgIm+gZAUZpAm752447VpOku+FFfpyaz0C/aEUc/Ny341oLGrXbHzOEAX71TIqIqiU3kh38KAvSoU80Wk0m/rk9KIgBBNN5v3sn71Nlh2WKR+YcuQvWn1LIp/MWKNNwJLDqKieaRrpFKj5afc28vnxBJO2ajEFwbxtrr8oxmo6DJVkc3j5j6ACo0l/cTuYzk5pkQuVSWYuhP0pp+0pbKmEO807gPZwIoFMZ+9UkcitekmM4VRUTvcGSNPLX5RnrTUuJFaVyq5+6BmhASB1NpI5jO52/rSySqHXbH9wVD5rmNF+UKv6ml2tuO5xxy1MQMXZido9TSqsjdQPWjy2Kj5+XNPCuscjBs9qAM2EtBdsSB8xzWhCWMjoMAMKoX6NGUdj04qWKThWyeKALSophZHO4rQX3RBkXlaQSIsgYDIanx53smMA0ADBy6OTgGl2qkjAc7qFTMbK7crSmRfLDIMlaEwInjLoyngVw+tWf2S8bA+Vq7W7vYYPnlkUAjpXI65qUN4wWJfu96Z0UL3MpOorptJH7muaiHIrqNLGIKcTSvsWzTTTjTTTRyiGm0403tVEjTVefpVg1Wn6VLKiYt/1qzoafvc1VvvvGr+gjL06XxGtX4DpF6UtIKdXWeaJSU6koAKKWkoASlooFABThTRThQA4UtIKWgApppaQ0wG0004000AMNRmpDUZoAbSU40lADTRS0UCAUgpaKYwrD1R/9Jatyuc1E7rt/rWFb4TuwC/eMZE6rya27XVlCrntXP8YGalQr71xtXPVnBS3Oqj1qInpUi6zbHvXKq4H8Rpdwx15o5UYujE7kdKaTTBk0uw1znKDMMVJCaiKVLCMUICcUhpRSGqJFWpkqFamSmBOlSr1qJOlSr1q47kPYTb82Ub+IcU12TyvnXHNLlWf+624USsywjcu6tuhzjdqs6eW+Paqt2iqQW6q3JHpVhnRnQFNvvUciDMjBw2expAG5mkHluHGOhrPuQXMrOmCPSpYJE85w4ZCOlQvIwSSRZM/WkxBpMe6/kcZP7rHP1/8ArVqLFEpAZdrbuDVfSYilo024Mztn6AVbeVOVlXjJFUgQGBVVwW6cipF2blbPUVCk0QIy2dwxzSieDYMD7poGTIUBdQOnWl3jyyQOPSmLMhlwq8sOtSbiEY7elIALHYCFodnAG0U0O5jBC80r79o29aAHNuyMUh3bxj7tDhuNppku7zFw2BQBJht/Xim/N5p54qGRWM6/vMUq2+Ji3mHNAA0TsZMt1xUhT92qhsY700RAFyX64pzqpjXLcDvQAsg5T5sf1phDfaB83GOlE6qTGSehqPYhufv8igCwFbeSW49KZGJFlbJBB6U5ANzENmo4UOXbzM0AO3SeWxwM03fL5OdozSlG8lgJOT0NMZZPs64fnPWgBszy7Yhs6mmtNJ5/3Ogp8ySbovn4FV5TIJJTu6LTAYJZfJP7sfM1Md5WZ8Ko/gpyI+YlMvbdUZ2KNzvu+Y8A80ADLlAWccnpWaUC3MhLFVJDLWiPLCoSDx/DVW4DmcQleuB9KBEqYO5yxk/hwKsO0mIiAEFRQB1hdUCqooGHKZcuy5OKQyXDIyhV3MWPzGiRcQv5j/xdBTnDExeY4Ue1MlKLuVAWOaYDM4lU42oHzVlXgFtPLkZbJ6etVps+YfMPOfuj6VNJIi2Krsf5mUdKOqKiLILdbWOMMvzMq9KdOsDTwR7l4Baknlh863Qq38TdKQvA+oMD/AgH3anoWNKQtfvhx8ihetQxKmJpQ/GSfvVLA1uZLh8jh27elQqYF01jkfMPT1NO2oxojC2OC+N3vSTLl40EmAg3dafIsGYELDr/ACofyRLO2fupiluBCBkI7THLnHXtTxHAGmOfuDAqRTDm3UD36Ub08i4YIeW9KEhCBLcC3GB69KCIcTn/AGqmZkE0HyNjB7U3Kt567Du3UIBoSPzI+f4aYUTyG+fo1OaVQschTAHBqrNPFMJFQ4HWgDK1/U4Yv3e7LVJp94ksS8ryKx/E4hYxMnDd6xI5ZEHyOwpFHoHnr5ZBKgr70j30QCuZRkelcMlzMx5kb86tRliOSaQ1E6abWYYpNyZbNZtzrNwyssfyqaojmmyDimkO1ileSyyHLuzVWBqxcDg1XFM6aWxYh6iuo04YtxXLQH51FdZYjFuKuOxNYnNMNPNMNCOYaaQ0ppDVEjTVafpVlulVJ+lTIqJiX3361dBXmsm8P7ytrQV+Wqpbl1/gN0UtIKdXUeeJRS0UAJSUtJQAlLRRQAopwpopwoAUUtFFACUhpTSGmA00w04000AMNMNPNMNACUlLTaACiikpgLSUtIaAFrmLpt1y59zXSscKfpXLynMjfWuavsj0svWsmSQIZpViXGWrcj0LKjG0mszQ49+qRj0ya7KIfNXDJnXWm07IwjoT9lH50xtFlAxsNdMKdikpMw9pIhWnGmL0px6VBA1ulPh6VXaeMHG4ZqaA56U0IsjpSGlHSmmqEKKmSoRUyUATrUg6Go1p/wDCc9KuJEg53EOv8Q5/CkkZlEaqQc0AlScHcpI/lUVwIgiNuIrboc45ml88AqMVE6x/vNwKkdqdiQzgrJkUvz7pdwDjFICjI5MIdMNt+XmqzsDandH83tU7sTGEMeFzVS52xNsSRsds0hGroXNhtb+F2Ufjz/WrxtlOTyeMcmsPw8xMtzAHJUMsgz+R/kK2WgPzGGRlOc47ZqlsNAbNCT9P1pVtwrduV+aoGnnjHPIbocfpSfbm3EHAz+ho0Atj5Ni45pFeRt424qFZJJFVwRwcGrC795z0pAIgk8rn71KyuUHPNKqsFIJpJQfLwGx70ANmDbRhsVG8AMisz04wBlBZ6kaNTtz2oAaYU80NnkU7avnZ3HPpTzgMKj3KbjGPmxQMGVfnzRIY/LXd92lZl+bI+tNd08sErkUALKV+QMM5NRERrdc91qZmGV+XOajlKidcpnigQ6Py8NtJqOHyljkCsfenxMm5xtxUYkjCPtTigBdsf2c/NwajeJBCvz96kYxmAZXANNk8ry0HNADZURpUHmVBJFHumBk7VMfJFwOD0qF2hWOUhCWPemIY5jwpjzkJ1pqqqhtqZJTOTTzIEwFj6JzTC0rgKSF4C0DFbfK+dgyFzVOVsX7SM53OADirsvmRjdn5eVz61Qld2vGZUG1Wxj0oEPRol8zLM1SwxBYWYnY2eneokLNctu2qo9O9TxtH5LYRmOaQEjeUGi+8570sqs/mMuI1BApWZwse2MCmHY0UjSPls9qY0RSY3tt+Y5+9+FWJpSIbdfJJHmDv7VA2SvHypn86lmklxbDYuN39Kl7lxHyTf6bGDE2BH/WiKZTfzlo36r29qa0sn9oLmJeIh396S3lb7XOWh/j7Gjp9xRHDNF9knfa2SGPSmvJCLBBhuSvakhlxpsh8lslTT5ZR9lhHlNzIv8qfX5sBWkia6iG1uFz0qNpE23LBG+9jpUqyf6cP3TYEY/nUXmsbWUiI/NJ6+9KIEyOv2iECM/coRibedRFzk05ndbiEiMfdNLEZfPnTC80IBskjmKBwg4aiRpFuS2FAdaYBK9gwZsFfSo3Sac5DsGj7UAQSEyx/OfkB5UVVeHYm5BhOxrQIhi2zDndw1VbiXcWGMR5+UUwOW1uJ3USN2rHVeK6nUwZLRhjIBrnCmDUstDoIx1q6i1BCABVhDQWhwprd6VjUbHApiKd1xxUAHFPnfdJTBQdVNWRLbD96K62y/wCPcVylqMzCutteIRVrYxrDzSGnGmmmjnGmkNKaaaYhpqncdDVs1TuDwamRcdzEueZa6DQlxHmudnP76um0UfuaujuPEfAagpaBS10nAFFFFABRRRQAlFFFAAKcKQUooAWiiigBDTTTjTTTAaaaTSmmGgBpNNNKaYaACikooEFFFFMYUUUUAMnO2Bz6Ka5Zzlya6W+O20lP+zXMHrXJX6Hq5evdbNnw0ub9jj7qV10Vcv4WTMk7ewFdRD3rhluaVn7xIOtOFIKcOhoRiysnSqOr3f2aA461ej5FY/iC3eSI7aI7kvY5p9SmM27ccZrr9Fnaa3BauOgspZJQCprtNJt/IgANaysSjUXpRQo4orMsVamSolqVaBEyU/txTEp/ariRIjGCcqdp+U7TUd24EEfyc5pzHAw4z8vDCkuC7QDZhsP1rboc40lHmUspTihFj3viTFEW/wA8htr4HSoUk2ySFoTikBHKjMgcuMA1RfdKW+6xHRqvMYnt2+Vh9KzWKrCy7DuJoJK/h26k/t4of41ZXH933/MV2CIIQeSc1y2gwBtYnliGCsO1vruFdWFVwM9RTWxSFO1lGQCDTWhiZvmjUnHpQCVyQQVz+VSA8nigZRe1URt5YKlfQ9qiLzxsuS5HY1pAnaTig5wOKQFWCcNvUydemamwkkQ+bI9abLbrIx4Az3qstvNEG+XcO+D+tAi9hQgHahiBjIqulwioFdSGHrUxkBQMnzZoAV32sBjOaazbZh8vXvSuWBG1c0yR5A4CrkUAKzYDnZSSyOsSlV5NBdxIw2/w5p4DNEN3DUANdnG3gDmkdnEqgLRL5jIhXg8UjJL5wIYYoAUMwnI2/Ke9QmWUxviOpsS7z0xUIEzRvyOtAD3d/JQFMmlkYBkHl5ppE2IxkU9vN80YwRQBB5pNyf3PQVA7u0Z2x4BarJ84zvtAHFVykrKnzhQW5piGOkhD5IGWAo2IqiR3yQ9BVd+6R8gPSuYl8xFXcT0oAbIzb8Lyuc/SqWpRMrR7c73O07avgMfmHCnk1DcqgiZASSPm47YpgNhV43IRB07mpI/N8lvujmq8ZhMpO9ulKrJ5bDLEZqQLEm7ZHuk49BTkKi3kCxfnTY1DPGIk+6MkmlZnEWHYKrNzTQEL/e5O5v5c1LMZ/MtRuGPm7e1QnJHA2r/OpZY5ftNviRuFapZpEdun/tFvuHCDt7023kmEtwSiH941KiTf2hL+8JwFHT2plv8AaBHctvH3nP3ab/yKI0klGlN+6XO3196lmkfyYB5Q/wBYO/tUbfaP7LxleQvb3p87TjyBhPv0v+CA+JpDfN+7XhFHX61GjSnTn+Rcjn9akQzi/b7vKqelNjWcw3Sbh1fHFNf5ASTNKfs7fKOSKNsovsl+HUGo3WVrOJjIflZTRcJ+/j/eHOw96XQCIbszIhZmVtwFSHezJOTtU/KwFNEyjyZIVyT8rVG6uyTKzfKGzj0pgEjRxGQJhlDAkVTuMIzEjk5Cj9auy+VG+IxuMiGqM+Tgt1IH4U0BRvyF05mz85+8K5o8nNdFdqZo9hRfTJPWqsWmh2GUXFS4lpmZGDU4Nax0yELwn5GqdxFFD/C4p8o+ZFYHg1BM21TUjTxdAaq3LAr8tOw002VTyTSikpVqTtWxZs/9cK6yDiIVytiMziuri4jFWtjmr7jjTTSmkNNHONNIaU0hpiI2qlc9DV1uhqjc/dNRIuG5iS8z11WkDFuK5VuZ/wAa6zSlxbCtaIsTsXxS0lKK6DiCiiigApKWigBKWiigAFOpBQKAFpKKSgBDTTTjTDTAQ0wmlJphNACGmE0pNNoAKCaSigQuaKSlpjClpKBQIqam22yf34rnTW9rLYtAPVqwq463xHs4FfujpfCqf6PM/qwFdHF92sLwyu3TWPrIa3o/uVxS3FU+JjxS9qRaXtSRmVYfumnPGsi4YZFNt/uGpTQIqraRR8heasQChuhp0FNATikpRSGmAq1KtRLUq0IRMlSdqYlPq0QyAffGxu/3T7imOdy7eY9w59M0SdDuGfRh7U1nbYdrKyg5wfStuhzvchUxx3OdxzjrQPn3/vuKeNrfIsf7zsexFVkdl3q8QDCkIYQI4W2zHPpVS4+eEEudw9B1q385gPyKvvVa6+VEBcn2WgA8GoxGoOwKncqjPrzXTJ8nD8kjrVXTbaO0tQmPnb5m/wB41aERCn5t3OeaoY8quzb0BphJjbaDkY4qQEN2puFkBNAx3O0UHO4elQgmJlUtlfeph1JzSAXnJpADt680Y+UjNJt+UDNADZIvMAB6io/KljyImBXqA1SscSDJpVwScNmkAxWd4wcbW70P5m9cYxSlDtO1uc5psykqPmwaBCsrknBHSkMbeTt3c+tDKfMGX6gjFLtHlEFuPWgBdvyKN3QUyYAMjbsEGnYQRj5uKJtm0bhmgBFUl2Ieo4kPlvukzmnNtSUEA/MO1MTy/Kf5jQAjIRECJfumpNpMoO/tVZmjWEDcetSfug0RD80APWFvOY+ZVSSMBhuf+Ori+WJXbf8AWqDNGU3knJbimIkbyEWUHnHIqLzCz/cxkgUSOrzMsacdyaiIdyXZ9o9qACSVmXazY6jAoQOytsT+E5J96dHGN3yL1zyalK7VYu+BswRTAogskoyqdKkjEvP3cFsCqSKcAzdRVtZYVjRRuZvTtUgWkQGZ8TdBzURUsocfMM4BNRqADI7/AClhgU5ggwilnwMY96BoQ4P8WT3PYUpjQ3cJEgz5fZqjnbbGSRwBnatDNbme3ONuVYcipZcSxDC32+fEjfw/xf7NMgjl+yXGJX6PSRJb/b5sOvO3v7U23ii+zXCiQfxj71V1+4oc6S/2ao81v4ecUtwJhJb/ALw9W7VG8SHTV/e8Db/F9KfcQp5tvmXnLfx+1L/ggP8A3xv+ZD/qx296bEHUXW6Vsbm70zZD9ubMv8A/iqAeSLa5Zdz5Zug96fX7gHtt+yQq0pO5l/ipx2m5dkQsY06n86UjD26JDwMnmkMkim43Mq7m28D8KQDRvEcA4Uk7jj/PvUqwpC8y3L8ON3NVJJwmQDxxg9+KoXN+ATlsmmNF+W7VI0VR93vWbNedgfwqi1xLO21M1dtbQR4aTlqEAsMTyHc/AParY2xr6VWuL2KBevNY15qkkxKpwPatFEG7GnealHGCFNYs93JcNgZAqEKXOXNPHHCirtYhyGrGAOaXYCpIbGOxp4Rmz/nFTwWrSYZT8n98jj8BSbHGLZmTAbgR0NNFPnTY+z+7xUYrBno0/hLunDNwK6pP9WK5fSxmcV1K/cFWtjCtuBpppxpppo5xtIacaaaYiN6oXX3TV9+hrOvDhDUSNIbmQozcAe9dfpwxbiuRh5uR9a7Gy/491raiRiSxSiigVucYtFFFABSUtJQAUUUUAFLSUtABSGlpDQA00w040w0wGmmGlNMNADSabQaSgBc0U2igB1LTaKYDqBSUCgRm60f3ca+9Y5rW1knfGPassctXDW+I93CK1JHYaBHs0mH/AGst+tbCjCCs7TE2abbr/sCtIdAK5HuYz+JjulLQKKDMqW3+rNTGoLT/AFZ+tTmkwGN0p8FMbpTrfvQgZYpDTh0ppqhAKlSoxUi0ICdakFRrT6pEsrN8svXaf0NMZlG0yR8/dJWnT58wjP4GmJEjll3MjsOnvW6Od7kDmPB2MyshqKUq6liHfPWrps/lWRpwrdKp3TiBzH5qsD/dpEkDQiOEYYvn+EnmorRZrjV4IDGFiUb2PXp/kU+N2RioQnPc1oaJbEPNK56nav8AX+lCBGqOOtKF6kdTTUY7tpp4wTu9KooYCVJzznpTzwAMdaY6N95T0zTkbcM47UADKrYVhmoVVYm4Y7TxU4PBJGKRgpXkd6QBlSnXigquMGoiUjJVs4zmpjjIpCEZVLDPWmmJS5IJBI7U87d4HegEbj60DIY3BjKlsMDikmhDAFnp5jjk3fLzUYKeWFcng4oEOdF8yM7j3/lSjy/KbnI70N5e5P8APakBjET4HFAAWjEWcZApzuAgbbmmB18jIWllkKxAhcmgBXkb5dqUyRWaXBUbWHNDvKQpwBTXWQzrl8CgAkjRVC7Dx1wM1DMYzMMKcLTzxMczVHFJCN5Zs/hQKxALhAZGZW29M0xp0WNNqDjnmp3lg8gbQevOO9J5atL0OeozQBVDStyqfeOMmpljQIGkbqP1qUINoJcqN35VGywLuySxU/nTARp95wPkXuBzWZq1yfLEVq4DA5YE1oTMuxo4xtGfvd8VnfYY7ciREU46Z5oAZbyTzwmOPlj95scVZWFVUKu5pB1NSJwoddyu9TiOFW5lPy8mkAg3blUxglOSTTCW6swXPOFoyGzl2+Y5wPSlXKjcqhe/zfpQMayFlcIvVSOe/FRmUmO3Zov4sfL7inO4W4jDSHnIzUX71YAFZXCSjrSNET+ZD9v5RhujB+770W5t99wnT52/h9aSR5Vu4SUU5QinQyst9NmHuCMN7Ur/AKDIQbZtJJyOg/h96lnNtm3bP8ePu+tNhk/0GZfJbgN3pXkLW0JER4dWPPtT/wCCBIGT7b8iE5QdsVDiU2jcKmX+YHk/ep8twElEnyrhdtZN7q8ce75sseaLAaE05EofzW+VcelZtzqCJn5qxLrVpJOFqGGGa5fc2cVVhouTag8x+XgUkNtJMcvkD3qaC1SLBbk0XGoRwAheTRYC0ghtUySB7ms+81UnKxcCs+e6kuG5PFRBcda0USXIc7vKfmJoAApyIz9OnrU6Ikf3jz6mqvYSVyNUZuW+UVI0ewBVH7w9EHX8fSp1QynEWVXvIev4elWYIUiGFH1J6ms3I6KdBsr29owUG4bdjonYVbJ+XA70HvTc4BqbnVGmorQwJm3SZ9aYKV+CB7CmioKjsaOkjMwrqB90VzOjj99XTDpWi2OWt8QhpppxpppowGmkNOpppiIn6VmXp+U1pv0rKvvu1EjSG5n2ozcj612NmMQLXIWQzcCuwtRiFa3o7GeJJ6BRS1scgUUUUAJRS0lABRQKWgBKWiigApppaQ0ANNRtTzTGpgMNRmntUbUAMNJQabSAdRSUtMAFLSUCmAtLTaWgDJ1b5rgD/ZqgEIarmospum3dqhh2s6hXPJ6VwVPiZ79BWpRO1tk2wxL6KBV0VXhGCoqwOtchyMdRRS0ySjZ/6s/WrBqtY/6s/WrBpMY1ulOt6Y3Sn29CEyyOlNNKOlIaoQoqRajFSLQgJlqUVEtSCqJK92nRtuR0NVRIUYMp3EfwtWjIm+Nl/Ws5w+4jarHrWsWYzQqqt5lCxU9hTo9L3R4ZgMVXLKrfcZT1WnrcfeVpm2OPyqjMj1EwwQBITvkHU+laWlFBp0BX+IfrnmsWQx7WiQNg96t6DcpGr2R5K5ZPf1FIaNd1KHevYcipAQwFN4ZTngt2NIrbDtb8DVDQ8gHPPtUa70YqORjipMDpTHQ8lW5oAfzgcUE/MBikG47fpzSgt82R9KQCNtZWBHamq4GwEdelOJIXOOaUnkcUgEBBkxjkUoILHimh8yFcdO9Kr5zx0oAEZTnAph2PE2V4705XJBO3GKTefLLbfwoEMBXahI74oWRdj7Up27cikr3H86I2Lbl24FAESyu0JKpikmMzQqFA+tSqrCFkOM9KRlbyAC3I70ARPDKxXdJgD0pGgTzwXkJ4qWUKUUs9MkMQlXJzQA2OGHzWzk1DmIROEj5qx5qLMQq1D5jbZQqUCGNuEKYTAo2zPN1CgClKyskZY7Vpl2dkrbpO1AyDau0b5M5amNKnzbF6nqfSm4BAx8oAzuNIqBiAoLHpk0AIZmcnI754pSvmrtzjn8qcAg5c7upwKUDcACNi46etCAWF5VY5VWVO9IzEj7igvySfSmbY1VUDsM8tSKA7fcZs+vpTESfMVyXUbuOPSmu8SLvckgfMSegp3UkrGBn5RmsPxUl9PYCK2jLxZzJs64HTikUjSSWN5Njt8h+ZGpJQm59pI3/xL2IrztJ7iA7UlkTnoGxXf6yZYvDen/Y0H2uZEywUEkbcmixaLE5G6BxI+c4P409XZb0lZfvIDyKw7L+0tqi7mXy+Dt2/Nmrs18kK7nbkUuUZfWR40lWRxhi3QdjWde6xFbpsU7iOwrDv9YkmyqEqtZLOztyc1dgNG71eeckBsLVAeZM2FySakgtS/LcLV5TFCuFAFOwhttYKmGlOTVp7hIlwDVGW69DVV3LdaaiHMWbi+d8qnAqpjcctQKkjiZuvAqthbjFGThRk1Zht843fMfSpY4lRdzEKvqf881LF5knEQMSf3j94/wCFS5FwpuQ1gI/3YG6T+6O31NSRWu4h5yGI6KOgqaOJIhhRT6hs7qdFR3F7Ug4BpM0dqRvYOlMkbbE5/wBk06org4tpf91v5UA1oYkn3/wFNFOk/wBYabUkR2NbRh+8zXR9q5/RB89dBWnQ46vxDabTqQ9KoxGmmGnmmGgRFJ0rKvj8taknSsm/PFRI1gV9O5uK6+AfuxXIaaMz12EH+rFdFLYwxPxEgpaSlrU5gooooAKSikoAWgUCigBaKKKAA0004000wGGmGnmozQAxqjNPNRmgBhptKe9JSAWgUlLTAKUUlApgOpBS0nSgDAvmJupPrS6eu+8hH+2P5024ZTM+fWrmiIr6lAM5G7P6V5s3qz6GPuwOxj+/VgVDF941MOlc5wjvWkoHSl7UxGfYf6o/WrJqrp/+qP1qyetSxoa3SnW9MbpT7amgZZHSkNKOlIaokUU9KjFSJQBOtSCo0qQVRI7tWfeIEfcA3rkVfFR3EZeJsH5hyKqJEldGZ5nHySA45AamSNnIwh3cj60hYBhgq4/zxTRGGJVUPPK1qYDZWkljw7KpXsKqh9kiyQj94hzmrMilBvaE5HWqxVt+7IRG7YpAdPBOlxbpPjCkflUhUON6nJ7Vj6JNieS3X5oyN+fQ9K2Mru2dMUxocrhs9iOKCo456VG6YYuOR6U4lXVsN7UDHAMCOeAKQbwG9e1LtO8Hd26U1Q+w/NzSEDM4VcDnvSGRhIF28Uj+YEXB570pV/MB4xQAIzmRgy4FKGbBytIpk8wg9KUeZtbPXtQAIzlCSvNLuby845pqmQx9OaHLrDn+KgAd2WNTt5JFRxGZpXyABRiXYpJHHNSIrbmyaAGqkjByzdW7elRtF/omGenIu1Hy9NxGbdgW4pgDrF5C5amSSRB48LmgvD9nHemySqDFsSkBJ5h8/wCVO1NjaTEnygUPJKZxtTBxUIEuJSz4oAgmldgqO+AOcColjaRiwH8Ocmn4VD/fYrT8Ox+Y7V2UARsqJncdx2CnAFnz9xQRTcqMhBubYOaVlPzmRvQ4pgIOhSMexP409lSP7x3PuIpgyyNt+VcHn8aZK64baMjuaAGszZO5ly3p2FAIVCVkbc3AphYxgY2Av79BSRv5jYLhe3A7UCJlXA3FX54XNSbVUAdCKhlu1hfBbftHygVSnvHl+8cL6CgtDdS0+wu8+fCjP/fXhvzp092Wjij42RIEX2AFUpbjg81mXd+QCqmhFFy71BYlOG5rDubp5myx49KZJIzmmqnrVpBcaqM54HFWERI/c0zOKaXqrCuTNL6VEz5phNNJoEKTzQoLHAp8cLOcnpV6G3RE3u21fp1+lJyGo3K8NvyCanDoDti/ev7fdH+NPCNPwi+XH79TVmKFIhhQKzbOqnQb3I44Gd99wdx7DsKsjAHSkFBpHZGCjsFBpuaM5oNBRSMaKQ0AJnmoLo/6LL/uGpjVe9/49pPy/WgmfwsypP8AWN9aSlf77fWkFSRHY29EXmt2sXROlbRrU4qvxDaaadTe1MyG01jTiaidsZoAilPFY981aM8uAeaybht5xUPU0joSaSMzV18X3AK5jSYSJa6hPuiummrI5q7vIdS0UVoYCUUUUAFJS0lABRRS0AFLRRTASmGnGmmgBhphp5qNqAI2qNqkao2pAMNJSmkoAWikpaYC0UUUAFIxwhPtS0yc7YHP+yaGOKvJI5yU5katTw4udSB/uqTWU3LVt+GEJu3b0SvMmfQT0gdXD1NTdqih71NWKOEKKXvSHrTEZ2nn5Wq0ap6d9xquGpYIjbpT7emt0pbfrQhstjpSGlHSmmrJFFSJUYp6UAWFp4qNakFUSPFQXk3k27Mp+btUwqvPLblgkvOOaqJEtjnYbkksku3eDz/Q1KsvB5K4PrWi8OnSTb/J+rZPNDWNi7BUE4J7gcfrWphYomUhcrM2G6iq7I5byyN2ela7WNvbcNcYU/3iKZaC0kumfLv5Qxk9OaAJNNtGtUZmI8zbnA7e1au75AWHJ9KhXCvkDocY/lUgOxwByp7+9MY4IApUHrULR7C2fukdRU+FJ3UFdytz1FIY1QpKkN2pFT5WAbvUaDynUPx71IEQq21uppCFZGZAN3IpSrbxg8UhU4X5ulOK/ODmgBoV95OeKAJNpyRShW3sd3BpNrBG+bmgBP3gi6/NTHWVohzzml2u0X3qSVWEI+agBJw6wcNg460oQ+a2XqG4iLQDMnGKcip57ZegACRAPl6B5XkPnOKYhhDOOWpVdPJbKc0AN82PyBtTvRLK+2PbHSLKfIwkfeklaUxpwBQA5jM1x1xxUBQESl5O9StG3nfO/aq/7pY35LHNAAGVZEEa5OKaFZthkbA5FODt5iBEx8tRqAGTzGyeTimAqvwFiX+AjNNOAw3nc2F4oLkoNvyrtNNUZzt4Hdj+VACZeTjr1+UVBfHycI3zdzzj6Vc8xIRheoP5is6VVaZpW+Y9s9qYiMh2PIUA8nv+FPeZsYFQyzqneqU156UFKJZklRMnOTVKa665NU5rqqckzP3o5blbE9xdZ4U1TJLHmkJoJ5rRKwXDilJpuaSgQuaaTQT+dPjgZzz0ouFiMAseKuW1m0jcLk9/arK2sdsge5bYOyD7zf4U/wDeXaiML5MA/hX+L61Dkawg5bEAkCyGO3CyuOrY+UfT1qxFbE/PKdzVLFCkI2qOKkqLnbToqO40e3SnEelJSggUG9gApGppf0o3UFCGgdaO9IKBik0lKeaSgA61Wvj/AKOR/tL/ADqyKqX3+pA/2x/Ogip8LMxj8x+tIOtB6mlFT1JRv6J92tg1k6IPlrWNaHDU+IYaaacaYzYFO5mMZsVTnlxT5pQAazZ5dxpDSGTzFj1qJV7tTkXJ5FMuGKKRTWhW5paYwMmBXRIOBXL6JlpATXULXTDY5KqtIfRSUVRkLSGlpKAEFFLRQAlFFAoAWloopgNNMNPNMNADTUbU81G1ADGqM081G1IBppKU0lMApaSloAWigUUAFQ3hxayfSphVXUT/AKGwzSl8JpRV6kUYR610XhWPi4f/AHRXO966rwyuLKVvV/6V5ktj3avwG9D0qYVHEPlqQVmjhAUd6BQOtNCMvTfuNVw1T037jVcNQwRG/SnW/Wmv0pbfrQhstjpTT1pw6U01ZI4U9KiqRKALC1IKjSpBVEju1QS2iSEt/FU/agVQmjPFnJBl4W+YdBWddaheh9k7mP8A3Vroap31vHPgOtPmIcTD3r5G7YzM54ZjWlpsJh0sl2AMrk7uvTj/ABouNKaY/IyqMcZ7VpQ2wh05IGwwVcE+9WmZuIkZwoBJYgcmpV+QbT0HT3NRQTK3yH7w+7x1FPX5Tsf+HkGqJHq6ldjcHOMVJtwAF4qrJJtC5XJVhzViORXJ2nkdqRQSIX47VBEQm5Xyu7mrIyAe9MkXcFyueaAAqjBfm+lLs+fO78KhTZ8oJIIOKkIXzfvUhAqHc/zU0KfLb56RETe/z0ixp5bAvQAqpmHG+mSQ7oQC9IEiELYc0h8v7Py5oGOlhQwAb+BTV8oTt1NK7RG3HNClfPbamaBDI5UEj7EyaEkfy3wlLG7LJIAgBpU80xvjFAxgaXyWwgFMkSVoVJbHNPCzGA5fimSJ+5G6SgQjogmG581XDoEfauTmpnEKuDndUO5jG4RMfNQAuZHkGcKNtMUom3aNzbTT9nzsZX/hqJpFQcccYpgLjkFznGOKjeTC4z2xVae9VO9Zs9+ecGmFi/PdKves2e+znBqhNdknrVV5i1OxVizLdk5qo8zN3qNmphqrBcVmpvvS000wFpCaTNNLdhyaVwFJx1oUM5woxT4rdpGAIJJ6KK147SG0jEl2ee0a/wBaVykinZ6c83zAfKOrt0FXN8VuNtoPNn6b8cL9Ka7XF5gf6qAdEXip44kiTagxUNnTToOWrII7XnzJ2MknqatLwOKQ4xR2qTtjFR0QE5pKb3o60y7C54plGaM4NIYnelopuaBjqFNA96QCgQtFApKAAHiql/8AcTH96rVVb88RL6k0zKr8JmUopKUdakDo9FH7utQ1m6MP3VaLGtDgn8Qxjiqs0oANPmkAzzWZdT9hUiSGXE+eBUCpuNNVSxyauW8OeatIGNji4zVK9GDitpk2rWLffeqWy4F/QxyK6QVz+hDpXQiuqn8Jx1viFFLTRS1ZiLRRRQAUlFFABS0nagUALRRRTAQ0w080w0AMNRmpDUZoAjao2qRqjagBtFJRQAtFFFAC0UUUAHQVT1Q4tQPU1cqjqv8AqVHvUVPhZvhlerExu9df4dTbpan+8xNckv0rs9FTbpUHuCf1rzpHsVvhNeP7op3amp92n+lZnGFFFKKYjH037jVdNUdMPyNV01LBDH6UsHWmtTY5NrUluMvL0oqNZARS7hVEj6ehqLNPSmBaXpUgqFDxUoqhMfRTacKYhagm+8KmqGb7woAmH3RUiH8qiH3RTo2w2D3q4kSIZl8qbeGwGPPHSpXUSrjPIHWpJI9yFQcVFG2Pkzt29CR1rQxIZXZPlyN3v3pHJYh9mN69RTpmbCpIoDH8jUW5ozhSQB8wBpAWoZSyJhs9jU4J3kY4rPcMGJ24/iDLUkc7hjghwRkUrjJXKbgWXB3U4+V5nvUbXClELJ1NOLp5o+SgQiCPzmxmhBF83Wl3xCVht5pscse58JQA2IxbH4NOBi8k/LxSQuNrHy6C7PCypHQA13ia3IWIt7KKeku652lCBinrmG26fMO1RxtI4jbpkYP16UAOLKsrYKg7eajTcY3w+BQII1nbe2c81D8iRyZ3UBcAgEDb5M80ybyhEnJJpQYvIAwxJNNlmjVFGACKAB3TzBtj6VFLOAW6YNU7rUVXOWxWLc6qWJCGqSA17m+ROrVk3Gos54OBWbJcsxJzUDSE9TVWHYtS3WelVHlJphYmmE+9OwxzNmmGgmkz70xAfrSE0maSlcdhSaaTSFuw5NSRQFzyCT6UrjsRqGc8cCrlvZkoZDhUHVmpSi267m2sw/hqdYZrwq0vyRgcKKVy4QcthwuFU+XYo2/vIetSwWmTvmO9vep4oUiTCDFPAwKm53U6KjuJ04HSmk04mmmkboaaTOeKQmikWkJnnrSg4pv0paAA0hPNKaQ80DDtSUpooASlpMUpoAKOtFA70CAdKo6gf3kf4n9Kvdqzr7/XL7IaZlV+EpU4feFNFPHLCpQHSaTxDVuV8CqmnfLb027uNoNU2cLWpXurjFUMl25pzMZHqxDblscU0gYW8JcitKOIItLBAEWpG6VZmVpulYd6fnrcm6GsG8/1lZ9TambOhDgVvCsXQx8orbFdkNjhq/EFAooqjIUUlLSUAFFFFABQKKKAFoopKYCGmGnGmmgBhpjU81G1AEbVGakaozQA2lpKKQC0tIKKYC0UUUALWbqh+ZFrSFZWqN+/Ue1ZVfgOrBq9VFEKN1drpybbC3X/AGBXEg/NXe2y7YYl9EA/SvPmenX2RcXoKdTVp4qDlEFL2pBSnpTQjE0z7jVdJqhph+RqusalghhNM/ipHkVR8xxVU3sZk2qaSQGmn3RT8VBHJlRzUob3qhD8mnq+KiDU4GmBbjlFTowPeqAqRSwpoRe60tUxKwp6z+tMCzUM33hSidTTJGDMMUXFYnU8CgfeB9KQdBS00JosKcgYPHemyRrKvJI+lYusTzWapcwOyt90+hqvp3iy3mkSG8/0d+m7+Bv8K2jqjJqxvbiCVmTI7Go5ISi70G9c/dx2qdWjnjzwyGoijRPlQuz1oIKqSBT8pI2nGD6UpAHLDhT95fSrWI5V+YLubutRNbtGcpkrjmiwXIssu5UfI+8M1Mt0xKlkHNQq20jcqvs/lR5cTSMArANyBSGW1fMxGzmnR53t8uKppJNGSRubb1BHapY70FsOAAelAizHuwcilUEL70yKUyIx4yKJHdYWbuKAEm3+V8pwagmRgFAb5etMkZ5LfG+oZZECDcxJFMZZk8tZUYtu9ahnuYl3YAFZt1qKIOoFYN7rWSQhp2CxuXeqLGD8wFYV3rJbIQ1kT3UkpyzVAPm707DsWZbqSY/Mxpm6mBcUpNUAE5pDTSaQ0wFJppNGaQmlcLATSE0hOBTS2eFFAIVmApFRpD6Cpre1eVhgFmq2xgtVGcSS/wB0dBUlWI4rTZD5spEcfXLfxfSlMpkzFapx/epRaz30gkuWIX0rTggSKMBFApNnRTot6sq2unpF88h3v79quDA4FO7YpOgqTsjFR0QdBTc0daaTzQXYUnFN3D1pDyaaeKRdhT1pueKUmm9KBh3p3amjrSmgBO9GaKSgB3WikooAKKDRQAq0GjtSGgQVm33/AB88/wBytKs2+/4+D7LTMquyKop8XMg+tNAp8P8ArV+tSg6G9E4jgGfSs+4kMj0s8pACilt4i5yapI5GOtbfcwzWzBAFXpTbW3wAauYAFFzJsjIFRMKmaomp3JKc/wB01gXX+trfufumufuP9dU9TaB0OiD92K2aydFH7oVrV2w2OCp8QZpasRQbxTja1PtELkKtFTG3IpphYU1NC5WRUUpGKSrJCkopaACiikpgIaYaeaYaAGGmNTzUbUARmozUhqM0AhKKBSUALTqSigBaWkooAKyNSObo/StgVkXpzcvxWNb4TtwK/eFaNAzoB3IrvUADACuJs0DXcI/2x/Ou2jzvrgmd1foWV6UtIKcag5wFDUDpR3pknM2F3HFG240y51bqErOt7KY8Zq/DpijluTVcqJTKTTT3B4zUtvZSBt7mtWO1VBwtTLD7UwI43ZQBU6yU4Qn0pfJqbBcQP709X96Z5B9KUQv6UWHcso9TqQapojjtUyEjrRZgWdoNIUpit709WoATYaTkGpN1LwaYxBKRTxOO9NKU3y+aBMbqlsbvT3Rev3l+teczqUlZWGGB6V6dG/zlWBGzp71k674ei1MGe3YRXXr/AAt9a3i7GT1OX0fV7nTXAjctDnLRE8fh6V3mm6pBqMJkhJwv3lPVfrXnFxbXFhN5V3C0Z7MehqWGd0H7qQqexXg/nWlrkWPTFVWXMbbR7UZKLyMe+c1xOlalq0t0lvHKJk5Y7xyB9f8AGt2XxGlk8a36GIOcBuorN6Csa5jjlIO3P+1TBbbPmjcgjpmoINTtrv54T5oHdGBp6X0JB8zcuOxFIRMvnCTthqVY8Rssihhn0oS5gcbllU/jUUt8kYwp3tTAfJGkTBk+TPBxVSa4Gwqz7qp3eoluXb8BWLd6rgELTsM1p75Y1xnArFvdYHIQ1k3N68h5aqbOSc5plJE9xeySnljVcn35pAC1PVQtMY0ITyafjaKQn0puaBC5pCaQmmk07gOzTc0maTIHNIBaazgU3JY4FWbSxluJMRruP8qAsV1RpD6CtK203CebORFH1ye9Sv8AZNO+Vv8ASLnso6ChLW4u8PduVj6iMGlcuMHLYgaWS5YwWKeXH0Lnv+NW7awigG4/PJ/eNWljSNAkahVHYUpqWztp0lEFGBSk0lJmkbWAnFITSMRTM5FBaQE5pM0h60UigzSHmjvzQaBjaDS0GgBBSnrSd6DQAlFGaO1AC0UUlAAaKWkNABS0dqKBBWXef8fEn0FanQYrJuuZ5T9KZjV6EINSQ/60VFnipYOZBSQPYv8AlGSQelalnb9OKrWkRdhWxGuxatnE5D1G0YFBNJupCamxAjVE5qRjUTUCKlyflNc/NzOK3ro4RqwH5uB9aFubR2Oo0gYhFai9aztKH7gVor1Fdi2PPn8RoW33alqOD7lS1yy3N47CU0gYNOqJ3IBqUBSn+/TRESMinScyCrkWNmDXTzWRly3ZnsjjtTee4rU2qe1NMKHtSVQPZmbu9aM1da1U9KrTQ+Wa0jO5LjYiNMNONMNaEDTUbU81G1IBhqNqe1MNIBKKQUo6UxiilpKWgQopaSigBaxrk5mf61sc1jS8yN9awr7HoYBe82WNKUNqEA/2s12Ef365TRFzqUftk/pXWQ8tXBLc6q+5Yoo9aKk5xw6U3vTj0popiZVi0lvSrKaXWsAPSlrflMbmcunAU8WSirtJijlC7KLWygcCmLakt0rQ2Cg4Wiw7lZbVaPsy1OTmgUWDUg8lfSmm3B7VY707ApWC5T+zCmG2Yfdq/tzSCPmiwXKBikXtSDI6itMLSFFPUA0co+Yzw1KH55FXGto27Y+lQSW2xS2/I96XKHMAkGzmhcN91qhHBxT4xtBPrVIQ6e2huYTFPGsqH+FhmucvvCCHL6fMYm/55vyPzrpQ+KdnNNOwjmfDmm3dndXLXcW0qgVT/eyf/rVV8VTI15bQuPuxl+f9o/8A1q6l5lXIYda5/UbK1vrxp7gMWxtG09qaeomc3DbP5w+xmRJOzRkg10+lnUrZD9rvBKp/gZQT+dJGYbSPbEiotVLnVFXITk1RBqy3KqueFrMudVVchTWPPeySk5biqrSUDSLk968hPPFUZJSaYXJNJtzTKGk7qUJ60vApM0CF4pCaKbQAuabmim5oAUmkppYL3pmWfgUAOL44pFjZuW4FWrWweZvlXjux6Cr8flRERWafaLjvIfurSuNRMqOWGCZRcK6x9zjmujgu47i0aOwAh+XIJ71haxayRQb5W3uepqbQdwHttpGsIXlZmnbWUNsdwG+U9Xb+lTmgUE8UjsjG2w1jxTc0pNM3cc0jVIUtimF+KQmm9BzSLSEJ4py9KQjikB7UDFopDRQMKTPNKKKAExzQRRQaAEHWgmikoAKKKKAClpKKAFoo7UlAC0UlFAhayJ8+bN/v4/nWt3rHl+/N/wBdDQYVd0R1NbczCoamtB+/FC3CWx1VlEFiBqwTTLf/AFIpxqzz2JSZoppp2EDGmN0pxpjdKQFG7PyGsPrcj61s3h+Q1jJzdD61K3N18J12mDFuKvL1FU9PGLcVbrtWx58viNGBhsqXIrMWRlqQXDVg6ZamX6ZIBtNVluaVrjKmp9myudFdv9bVxFO2qJb581cimGzFaSREWTUUwSKe9OBB71nY0uITiqV02Wq8QKz7n79aU9yJ7EJpjU40w10GIxqYac1MakBG1MNPNRmkACikFLQA4UtNpRTAdS0mPeigAP3TWOw+Y1rScRN9Kya5q/Q9LAbM0tAX/TifRDXTw9TXO+H1zPK3oldFB0NcUtzar8RMKUUlKtSYimgUGkFNAatIaCcVC7Gug50TZpN4quCxaplWgB4OaQjNFJuoATFNNPJpp5oGFKKFWnhaAEFOFFA6UxBQTRS0AIKgvDiH6mpzVO+k2KN/14pDKxJGKnGNvFVwwYgnp1pJrtU+7yaVhlhiAOaqy3yx5CfMaz7m+/vvgelZVxqYGQlUkTc0ri7LEs74FZs+oqgwnJrNluJJT8zGo88VQiWW7kl6mqzPihmqI80DAsTSYz1paSmAYxQTRmm5oFcDSGkJpM0ALmkNITTGcCgLDs+tMZ+y9aQbn47VZt7UMCzEIg6s1AWII4WkYZGT6Vd8iO1UNcdf+ea9amh/eZj09Nx6NK3ar1vYxWzeY7edN3du30pGsabkQRwz3aKJSYLb/nmOrVcXy4EEcCBFFEjk00eppHTGnYzPEB/0EZ6lxSaEMJ/wEVH4gb/RlHq9TaGB5Z55CjikTFfvTWH3T60xjilJwOlRu1I60hGbFMJ4pGNDZxSNEhFPPNKeRSLRQMO1HelPApBQAGj1oNNoAXvQfagdKU0AJ1FJ/DSig9KAEpO9FLQAnaig0UAKeKSiigBaKSlNAhKDS9qQUAL3rFb7rH1cmtmsUn92v1NBhU+KI2rFkP8ASFqvVqwH+kLQtwn8J1cHEIpxpIv9UKDVnnsSmmlppoEIajfoaeajc8UDRn3x+Q1k2/N0K0r8/KazrPm5FTHc2+ydhZcQLVmoLX/UrU1dy2PNluOpaaKWgQUUlGaAEpQT60UmaAF3H1pyysO9MoFFguSmdsVCxLHJpaaaFGwN3GGmGnmozVAMNRtUjVGaQiM0w05qjNIY4UU0U4UAOFOptLTAUUtJS0AMmOIX57Vl4rSueIW96zj0rlr7nq4Fe4zY8Pj/AF7fSugh+7WFoI/cyt6kCt2L7griluVV+Il7U5elM7U5elIyA0LSGlBpi6F/LMacqU5Vpa6TATaBRS000hgaYSKUmjaO9ACU4UoApcUAKKM0UYoAM0A0gXFLimIXNHFJwBmqVzqcUPyqd7e1IaLx4GSayNTnSRgqNnHWqN5qbMCZH2r/AHRWNc6qeRH+dG4GrPdrGvzNWTc6qSSsdZ0k7yNliTUeO9VYRJLM8pyzVF9aUmml/SmgFLUwtSFqYTTEKWppakoNABTSeaCaQ0ABNNNBpDQAUhbA5pGYCmbXegALk8LUkNs8jcAsavWumZi8+d1hhH8Td/oKlieactb6coSEfekPU0ikrkOyC0UeaRJL2jX+tW4NOluwJL0iOP8AhjXrVi0s4bKPIAebu5qRnyKVzohS7jz5cMYihUKB2FIis2Rmo4xvPPQVK0yrwtI6FGw148Yyaiduw6UNISahJoLsZevf6iP/AHqm0kbVD+wqDXDmFP8Aeq7pYH2cg+1HQxiv3xdLcc00mlI4pi9aR2CHOeaDSnoab1pDHD7tIenFO6AU09aYC9qF5NA6UCkAGm4pTRQAnal60lHb3oAKTHFBoxQAnalopO9ABRRRQAUnelBpDQAtLSUooAQdKKT1petAmNc7Y2PoM1jHiNM1rzj/AEeX2Q/yrJYfKv4/zoMJ/Ghoq3pw/fiqlXtNGZxQhT+E6iP/AFYoNKv3BTSaLnCJTaU03NVcQ0mon6VITUMh4pXGjMvj8tUbDm5q3fHiqum83NENzX7J2NuP3K1LUUPES1Jmu1HmvccDS0wGnZouIdSUmaKYC0lFAoAU0lL2pKAEpDTqaaoBhqNqeajagBhqM09qjakIjY0zNOamUhiinDpTB1pwoAcKWminUwFFOFNFOFAEF2cQfjWeelXr0/ugPeqNclb4j18Gv3Rs6PMkdsVJAJat2GQFBg1x6SkDkVMl66H5XYfjXM4lyptu514YHvTga5ePU5Qfv/nVuPWXH3gppcpm6cjdzSislNXjPDKR9KspqVuw+9j60rEOLOjopKCa6DmCkOKFX3p22kFyLqaXZT8CmkmgAAxS8UmR3pRimAuaMUh4FU7vVbe1HzNub0FAF3OKoXepW9ux+fe390VgX2vSz5VDsT0FY8l6TyDk0DN671aWbO5tiegrIuNSx8sX51nyTPKeTTAKLBcfLM8rZZjTB60U0viqSEKTjtTS9NZ+aiLUASF6YWppNJTAdmkopKBBmkNJmkJoACaTNBxUZY9qAHlgvWoyxbpQqM5wBk1ox2KW8YlvH8tf7v8AEaAKUFs8p+Vc+9Wt8EGEiXz7j2+6KlUTX52WyeRa+p71eitreyUCFcv3c0jaFJyIYbCSdlm1GQt6RA9KtKVjXZEuxOwHamGXPXrTd2TSZ1wpKJIW4pu7IxTSc03NI1SJd2yPav40zOeKbnFIG9KBoVmx2phPFK/amdsUhmVrR+SP61paaP3JrN1ZWkMaopY56AVq2KMkRRlKsDyCKZzU/wCMyftSY5pTQBipO0Q0UnNFAB1ppNO70hFACr0pB3pTSUAB9qKSigBaSlNJ+NABSGlpKAEooPWigBaDQOlJQAdqSil6UAIaWkooAUUL1oPWlAoER3PFpN/uH+VZDfdT6Vr3f/HpJ9KyG+6n+6KDCXxiVf0v/j4FUO9aOkjNwKQp7HSr90U1qd0FNNRc4xhptONJVXERNUMh4qZ6glHFFwRlXx4qHS/9fT740aOu6b8auG5pL4Tq4z8gp2aaFwopDkV1cxwOJIDS5qMHFO3U7isPFLTA1OBFVcmwuaM03NLQIdmim0tMBe1MNLSGmIY1RtUjVE1MYxqiapGqNqQETU2lbrTDSAdSimjpS0APpQabThTAUU6miloArXp4UVS6Grd6fnH0qoetcVX4j2sMrUkSrGzKGCnBoKFeowa6W3tY/s0KlB90VO2mwsK5+cftTkhTs+ldHJosbcjbVWTQ2H3Qfwp8yK9rFmNvPrThM471ck0qZP8A64qs9nMv8FWpIOaLOxPiGH/nk350f8JBCf8Alk351zBcVG06D+MA/WtbHmHYR69bMfmVlq3Df20/3JBn3rgvtUY6uPzpwvkH3ZB+dKwHoY9aQ8jFcxoeu7rhbaRwwfhT6VvXN7DbLl259KQEjJVW61G3slJeQE+grB1LxE7EpD8ornprl5WJZiTTsM3r/wARTT5WI+WtYkl07MSWJNVuWPNOVKdhXFLlu9IB60vSkLe1FgFzikLYqMtTCxP4UxDy9MLU0mkoGBNJRnikoAWkzSd6Q0AKTSZozTScdaBC5prPjpTSWPTpTo4mdgqDcTQAw5Y81ZtbKSc5HyoOrHpUv2dLYBpzufsoqWC2ub0/MxhgHb1oKjFvYVZEt3EVknnznjfjgVYTTvn86/k82Q87M8CrEaQ2cflwLg927mms5bk1Nzqp0OrHSTZ+VRtA9KhJOKU03ig6krBSZxQeaB6Uh2HD7ue9N3e1KeKaeaBh1oHtSUo5PFIYMeaYWPalag4oEzK1V2SaEoxU56g10FwwN1Mw/vmud1Tm4hX3roJP9bJ/vt/Omc1L+KxDSE4FHU0Gkdg2lNJ0oFIYZoz60GkoATtRmgUUAApaTvS0AJSU402gBM0UUUALSGiigQtNpaDQAlFJS0DDtS0lLQIMU9ehpppT0oAgveLWSspu30Fal82LR6zGHzUGD+MaBzWno4/fVnDitXRl/eUhT+E3jTTTjUZqDkAmm5pDTSaBCMagmPy092qCZxigaMi/PNT6EuZBUV0garWjIEcVohyeh04AwKXZTA4p4bIquYwsMKUbKfnNB6UczFyjMUm2nCjNVzsXKN2kUgLCpAafgGmqguQiDmlD07aKUIpq1URDpjMikJqXyQelMaA1aqInkZEajNPdWFQOxHaq5kTysGqFqcXHeo2YVVxWGMaZmhj70gNADhThTBTh0oAcKUU0U4UwHUoNNpwoAp3h/ej6VAgy4FSXX+uNJbLvuY19XArgqfEz3aOlJHXxjG0elWqgj++KsVzHKwpaQU6gQ0gGmPBG/VBUlFMR5ibmRifnb86ZvJqHJJ4BrU0/RLy8wShij/vNXXc57FLk960LHRrq6wxXy4/7zVu2+nWGmLuf97J6tVe71hmysQwKV77DsPitrTS8Pu3yjuaqXmpyTngnBqkzvI2WJNATimguJktyacqU4cUFsUAGAKRmpjPUbNTAkL0xmPrTC1NzmgBxamGjNIaBC0maQmkoAXNJmkyBSZoAM0GkLCk+916UABYngUm3PWpIomc4QVM3kwDH+tk9KASI44c8yNtWrMMkkreTYRH3apbXS5br95eMYoh0Xua1EeO3h8q3VVX2pXNqdJyK1vpyW58y5Ill9+lSPKSCF4HtTWfPemdc1NzthBRAmkBz1pM5NHFBqgyelGMUoODQx9KAG96F65pDSgcUgEz1pM5opDQAdqFozSimAjdaQ0EZNFIGZWo/NewD/aH8633J3OT13t/OsG7+fVbVfV1H61usck+m4/zpnNR/iyEz6Uh96DSH3pHYBo6Ck6ClpDEpCOKUHFGaAENFFJQAtGaSgUALnmkNBpKACijtRQAtNNL9KSgQtApBSigBKKKUUAFLQaUDNABSGlNJTEVb8/6KR7iqJHzGr2of6gD/AGhVM9TSZi/jGYrY0YfPWQBW1ow60mTU2NdqjNOamVByjTTDTzTGPFAivKaoTyEZq1O/Wsm6lxxmmiiCadmfAq5YSOhBqnBEZHres7MBBkVoTItRXJIGatJMKgWFRTxDSMyysgNOLVW2EdKazMtMC0DQRVaOU96k83FICWlVqjWUEUB+aAJC1Aemmm4NAE3m4p3m5qvSZppiJ2YGoWwTTS1R7+aOYLDpIlYVWe2z0NTGUetMMo9arnYuVFR7aQfdNR7JV7VcMo9aaZBVKqyeRFQOwPKmnrIDUxYHtRsQ9Vq1VJdMaGBpwNN8hT0JFKIHH3XzWiqIhwY6nA0zbKP4M/Sk3kDlTVcyFyspzHMrfWptOUm/gwP4warOcsTV3R+dRi9s/wAq4Z9T3doHVRffqeoYvvVNXOcYDrSnrQOtHemIO9B60go70COctNIsdMUNKRLKO7U281jjbFwKzJ7mWYksaiCV1cphcdLPJKcsTTAnc04CjdVAGMUhNMZuaYXpASFveoy9M3U0t6UAOJppb0ppNB6UxCmk7UmaKAFNNozSZoAM0lJTS3NADicdabknpSAZ+9UscRbrwKAGIvPHJqwkAVN8p2in+bFBxEPMk7fWrVrpskv729Yqp52dzSKjFy2KkXnXLmO3TC92rTtbKCyG4gSzf3j2qdpEij8uJFRB6VXLc5zk0rnZToKOrJZJmc81Hk00H160p7ZqTpsKTxmkzjjvQSMcGm9OlAw70d8UgPNLTAD0oxR0o7UAIo5pCad2pvagAPA60mM0d6Q0gFFAHNHWnLQAn86QinE8UhPFMRk3Py6vbE9nU/8Aj1bSkMgx3yawrwFtViHfI/nW5DgQr9KGc9D+JIcTRQKKk7BDRjig0goADQKKToMUABptLRQMUdKSjNFABSHrQaKBC0lHaigA5pKU0nagApcUnQUtABSikFLigA7UtIelFAC0lFFAipff6tB/tVTPerd9/wAsv96qtDMPtsBW1o4+U1iCt3Rx+7qWTU2NFqjp70wVJyiNVeVsDrUznAqlcScGgdincyYBrLYmSSp7qUlsA0tnBvcVokMu6fa5IJFbca7VxUdpBsjFWlWmYtjcUq0/bxSYxQAUxgDTiajwxakIkSMYpTCDSoDT8GgZF5WKbsIqzim4oEVyXFCyHuKmIpuygY3fSbxTigqF4z2NACu3FVS5zSyBxUDMy0gHNIaiaWmPNVd5M0ATmQ+tAk96qB6eHoAuCT3p4lHrVLfRuNAF8SipVkrNVjUyOapMDQV6Vn+U8CqiyU5pf3TfSnzAldlBuSa09ATdelv7qGsv3rZ8Or+9mb/ZArOWx6c/hOhhqbtUUPSpayOMUUtIKX1oENFHegUDrQI4QDHB60hNIzUxnrrMBxbFML8VGSaQmgQpbNMJ4pCabQA6kzSUlMBSaSkPWigApKCaaeOtADqQtgU0v2FCrmgBMlqfHGWPAqaOD5d8h2p6mjzSX2Wy5JoANixY3nmnQW1xesRGuI8/ePSrtppYUiW+bceuwGtJ5AECxgKg7Clc6KdBy3ILazgsRlfnk/vGiWVmfPUUMciojxUnZGCjsLnNHFMNOBFBdhMZpWGMUnTmnMfSkMYKQmgdKKADvS0dDzQetMAoox3o7E0AIx5ppoJ9aXNACCilIpKAFpR0pvO6nUANNApTmkoEzKkDHXIdmNwYEZrZjz5a564rF3Y1uM9cZ/ka2ox+7X/doZz0PikKetFL9aKk7BrcUc5oPWigBO1FHakoGHSkzTjSdaAEP0pRSUZ5oAO1FFFABRmigdaAEopaOtAgopaAOaABRzzS9TQOM0g60wA0CiigQGgUlL2oAp3v3ox71VFWb3/WR/jVYUjD7TAferf0oYhrBA5FdBpgxBSZnU2LTUwU81GxwKgwIZ3wKy7qXCmrd1J+lY1zJvfAqkikRKDJJmtzTbboSKoWNvuYcV01rAEjFWRNj1XipFWl20CgyGkUhWpKYxpgRsKcq4pFGTUgoAVRS4oApaBBSUppmaLDFxSYpc01jQAEUxloDGgtSAhdKryRZq4TUT0hlB7fiqcluRWsRUTgUAY5hNJ5TCtFkBqMxiiwikdwFIshzV5osiojbigYxJBUolWozDimGFuxosBZEvFBfKn3qsEcU4M3QiixdPWSHCt7w8uIpm9SBWBXR6CMWLN6vUS2O+r8BtRfdqTvTI/u06sjkHLQe9IKKYgHSgUUg6UxHnhfJzTd1NNITXWc4pNNJoJ4pp6UALmkLUgpKAFopM0lIBaSkJFMLE9KYhxam43daVFyeOatRW4C75DtWgZFFCznCrVrFvaL+8IeT+6Kb57yYhtY+vcdau2mlJB+9uzvfsuaC4wctipHbXOoMHOUi9T6e1alvDBZptiXL92NSSzbwAvAHGBUNTc7adFR3HsxYnNNz2pM8UD1pHQkDH0pjdRSng00nApAAOT0pDSZpRz1oAA1B9qVf0oPFACYxR1FJmkoAWlpPrSqe9MB2OOab39qdnOeKZzQAnegUhooACOKUcfSk7UUALjuKBmgUtMBpz60hpxwaaKQmYz/APIXH4/yNb6f6pfpWA3Or8+h/ka31HyD6UMww/xSFPIptL1oPWpOsaetApaD3oAaaXrSUUDCiiloAbRRRQAUUGkoAO1FLSGgQtOA4pgp+aAEpRSUp4pgGabTh60fSgQYoIpO9LQAnSiilxzzQIo3v+vX/dNVh0q1f/65f9w1UFIw+0xyferotOH+jiueT7wro7EYtxUyM6hMRVad8CrEhwKzbqTANSZFK9mwCKowRmSTNEzGSTGa0NOttzDitEhvQ09NtAFBIrWVcDFNhiCRgCpKZg3cQ0lBNN60IQrNUTNTyKavzNTEC9KcKkVeKMCmA3dSFqdSEUAJupKUrSYpAFNOafQcUDIqQ1IQKMUgIMUhFT7aaVoArFaidatMKgYHNAFcrULKQankz2qNhmgBF6UfhQoIp3WgBuFNBVaeEpdgpAQ7BUcq7RVkpVe44wKZrR+NEArqNHGNOj9yTXLDius0sYsIR7VlM7Kvwmmn3RTqRego71mcw6g0UGmID0pO1KaT+GhCPNc0maTpSd67DnHE0mcUhooASjNFNLUALmmlvSk5Y+1PSMscCgBgGetTw25c4AqYWuxC7kLSRma5PlW6H60CsHmR2wwoDyVLbWFxet5kx2R1ftdMhtcSTnzJPTsKmknLdKTZ1U6DesgjjgtF2Qr9W70x5C55NNJz1pODUnZGKiB5pw460lKDkUiwzxQvekPTilFADWNRnHapGqPGDxQAAetKOKKTPpQAvSik5o7UAIMGlA9aB9KBQADk0o+770i9eKd70wBuPpTGNPPNN69aAG55pSfWkx6UDk0AL26UD1o60tIBtOPT3ppxmlpgJ+FJS0lAMxuDqzfQ/wAq3x90VgKM6q/0P8q3+wpM5sNvIOlIeWpe9J3oOwO9HeiikAneiigCgAPWjHNJQKADFJRRQAY4pCKUUE0AKKTvSiloAb0pRS0UAJ3p2KSimIMUh60tIaAD1oFAoNIApyihecilJ44piM++P74/7v8AWqoqzef6xv8AdFVaDn6ski5cV0tpxbiuah++K6OJttuPpUS2M5iTvWNezdhV27m2qTWMzGWWiKIQ6CIu2a6TS7faoJFZ+nW25hxXRRRhEAFWZzY+m0+mEUEDDzSgYpcUGgBjmo1b0pWO6lRMUxDg5peacFp1ADaQ06kpiG5pCKVhSK2aQDeaGHy0+lxTAij6U4igDBp9IBmKMU/FIVpDIWSo2TipytMIoAqMlRMlW3WoTTEVwmKXb6VIVzTVjK0AIme9KSBSkYo2560AA5qleH5hV4DFUL7/AFg+lJm9D4yuOTXY2IxbxKRjCiuPjGXH1rtYB8qAelYzOqsWhSik7UDrUHOPpO9FJ3oEONJ2oNJTQjzI0dqQmm5rsOcdmkJppekAJPNAC7ielKqc+tPjiLGraxJDHukIz2FAEMNuzfSpGnjhysS7n6UL5122yFSB61pWthBajdJ88nr6UXNIU3Ip2+nzXTeZcsVQ9BWxF5VtDsiULUTS7utMY54qWzshSURZH3k1EPWlNJmkboTNKeOhpMYozQMd1py9KYOKcBxSAOaUZxQB3NOWmAw0w8HOKeRnpUZpAGRmgigCgjigBMnaKQU4+1IetAAelFLnimmgBRxSk8Ug5oIwKYC8nvTW4pe2RSHJPNACEUDmgj0pwH50gE4zSUvegUABoHWg0fhQAHpmm560ueKTGc0CZjwjOqy/T/Ct0dKxbLadSucnnZx/30tbOeBTZhhuo40g60UgqTrDnOKDR3ooABR0pe1JmgA6mkoNJQADtRS0h60AFJS0UAOxxSU7tSUAGaKKUUxDSacab3pRQAZ7UGigUgEHSgUGkFAh3TFFKT60lAGfe/fb8KqVZveZG/Cq4pnP1JrcfvBW5vxCPpWHa8yitOeUJFjPNS0ZzKN/NlioqO0i3NmomQvLnOa19NtdzDimiG7I1dOg2Rgmr5HFMjTaoFPpmDEXpQaaWINJuzQApaomehqj/ioAkQZqUCmx4qUCgQgFLiloNFwIzSZpxFNoAQ80qrS0mcUwApRnFLuyKbtyaBCnmkAp2KWgY0g05feigdaQAVqNqmI4qMr60AV2xUTIKsyKBUBpgQEEGk5qQ0hFAhlFLimmmAdqzrw5mNaNZtzzK1Szpwy94S3QtMijuRXZQjBArkLM/wCmRf7wrsIfvVjM3rFigUlOFQYCmkHWlNIOtAgpKU02mgPMWamZLUoBJqeKAseldtjmIkTn1q1Db55bhfWpAIbdcyHJ7CiKO4vjiNdsfrQCVwedFwkCb29asW2lNKfNu2Kj+7V20t4LBePnk/vGklnaQ81LZ1U6HWQbkiXbCoUCm7iSM0wUD71SdijYeTR04FIGOMUlA0HtRxSE0lIYpGQMUAHOO1N5Bp65zQAoHNOAwKBS0xC8Gk6d+KQt6Uh6UDEPf1qM08+9NxxmkAA0UAUY+bmgAPXmk70OaDQAn8qUdaQ0YyKAA9KXtzQKQ0ADcCl7Zpccc00e9MABoPWg0opAJg0pNJjBpcZ6UAJn1pM0p96SgA/hpG+7kUEdaONpxQJmTZEf2jcDHUYH/fQrb61iWHOpTH3/AK1tqPWhmGG6hikFKTSUjrFpBRQOlACGg8Gl60h64oAKSg0o5oASlpKDQAUd6XtRigBaDS9aaetAC0HikoPWgAxSjpSUtMQlKaSjFIA70UHrSselAgppoo70AZ1399vrUSrk1Jdn52/3qhB4pnOWbYAOTUNzOXfANPgPDVAq7pKDORasoizDNdPYwiOMetZWmW+SGNbsfAoMJslpCaQtio2egga0hDUBhmmnmm0APZqYvLUYyakVKQh6DFSA00CgHmmBJSUUuKAG0AUMcCozIT0oAccCoZCx6U4qTT0h9aYDI8gVMBTggpdtADaMUpHFMQOW56UAKVpQKfto28UgEFDClC7elL1FAFdxUDKBVtlqF1zTAqMtNxU7LioyKBEdNNPKmmE4oGNPSsyTmQ1oyMMGs1vvE1J14Zass6Wm6/jHoc11kPWuZ0Vd17n0Wumh6GsZ7l1tyanUwdadUmIUCg0goAU0neg0000I8/itAE3SHbQbhm/dWycnjNSQW1zfne3yReprSjit7NNsXzP3auy5nCk5FW10kJiW8OT121dmuVChIlCr7VA0rOfmam45qbnbCkoiht2aKQDiikaig4pPfvQQaBSGODetJmgkUmD6UAFHUUUd6AFx+dKnp3pM804DB4oAUZ6Ud6QmgHrTAN3OBSmm/Sg9KAFOCKZ7U4mmt2FADR15p3ejg0g60gEPpR9KU4pB7UAL2waOAtGP0pVoATtRS0UAHpSdqCOaM4pgB6Un0oz6UvakAnNJR1FFABmjr2oooASg/dP0oPtSN9w0EvYy9OGb6c/7X9a2+3FZGk8y3Jxz5i8/99Vrg02Y4X4WGM0lBNJUnWH0pKWkNAC0lFIKAClFHek70ALSGl7UUAA6Uo96QUooAdn0pppTxSHk0xCUd6KSkMU0Cg0LzTEKKBRSUCCjrR2FBpAJ2p0cbSOqIpZ2OABTa6Pw3pxCm8kXluI/p60EVJ8qucXfo0c7I4wyuwNV81f1kbdSmGersf1qg/DUznUrk0X+raltoi7getEGPLNamlwDdvNBnKRetI/KjAq4jVGEFSdBQYjzTSKaXxRvzQIQmmnpS7hQDSAIwc1OOKjBxUvUUIBc05RTFHNPDUwFNIuc07r2pcUCGsm6kVAOtSUwr81MB20U3pUmQKCAaAGIxJ6U/FOVQKXFICPaaXGKfQaYDcUlPNJSGNpcUmcUhfDYNAgK1Gy1NnIprCgCo4qu47irbjNQMKYEIbIpp5p5WmLHtPXigCtKmFNZxPJrRumKg8cVmZ+Y1LO3D7GvoQ/fufaujh6VgaCvDtW/FwtYy3FU+IkFLTR0p1SZiGlHSkpe1ACGkPelNIaBHKyz8bFG1PQVXOS1Obrk0mfSuo7YqwHtSg5HNNzk4pQflpFWFz2FBpoNOHNACUFPlzmlyoNG7dmgBoGPvU8UmKUdaADFGKBnFIevvQAoGTzTs4NNH60mcmmApyaVelHQ5pRzQAe1GefSjPGaQc0AGfWkNIeTSmgBuKN3rS5ppFIBTzTc4paKAHHGKT6UpPymkXpQAo68mjdzSGigBc004xT+tNPpQAg+nFHfFJ0NLjjPSgAwaKCaQDjigA70Gk6GloASmufkNONMb7poJlsyjoxGbkdy6/8As1aw6Vj6KcvLz/F/jWuKGZYb4Q60CjtR6UjqEHWl7Ug70lAC5o70lFAB3paPpQaADHekNO7UhoAWgUlFMBSaSj8KQ9aQBRRQaAF60o4pBQKBC5pvelFGKYAaKQ0qqWOBzQItaXZNf3qwjIT7zn0Wu5RFjRUQbVUYAHaqOi6cLCzG5cTScv7eg/Cr0jBI3Y9lJqTgq1OZnlt8Gmui57nP61AYWwxxXRQCNrNFKbmI9KaNNRuRxVEqdjFt4W2jite1OxQOlT/YgijbUexlPTpQRJ3LaSCnbwarJ0oGR1oJJ2bK8VEHNIDjvQOtIBxY05CTUZ5NTRjFADgSalUmm7aelCAkXmnBcmhFGOtSrgUwAcCm96UnmlXFAhBTgKQ+1AJHWgB22jbSluOKBTABSkUUUAKvNB4pBxSkbhQIOtNIpUXb3pSuRRYY3aKa6ZqRRgc0uKLAQ7Sq8CmqxbhhUzcUwFW6UAQMmDVeQEdKuuKryLQBWxkc03FSmmE0AVrxR9nasY8NWzen/RzWQRzUs7cP8JvaJ/qGPvW0n3ax9GXFt9TWwv3RWEtyZ/ESCikFKaRAUUlLQIDTTTjTDTA5FjuPpTDnPy1Ix70zndXSdyE59aQU4ryKMc0h3EPFPHuaaR6UbSRmgAI+alQe1KOKUnigA9qUdaMgU3cKAF3Z6U3vS5z0o5oAcT+FN70pJ4owc80AO7c0Ac03vS8dqYAT27Uvakx1pBzxQAmfajPHNHOaRjxQA3PtS54pPSlOBSAKVhxkcUHp7UZ4GelACqPpTfpSnvSZoAWl7jpTT1pwoAKa1K1IaAAUU3OKd2oAAe+KYaeelNPSgBaKbzS5oADx0pjfcNOPU0x+Iz9KZM/hZQ0QcyHturYHSsfRPuSf71a+ecUmZYb4BDzR+FLSUjqCkBob3oxQAtAoIooAKKAKWgApDSnk0daAENApKUUAFOXbn5+lNpDTEKTzSCilFIYlFFAoAUnAozSHrRTEGOnFb3hvT/NnNxIvyRnj3b/63X8qyLO3e6uEijHzOcDPQe9d3a26WtukMY+VBj6+9JnLXqfZRMelVdQbbp9y3pE38qsms/W226Pdt6RGpOQwLOAfYYfXbUyrjiltBi1iH+yKeRzVkjSnFQlOTVvbTHSkBX8oCo3iFTnNM2UgIljDU5o9o4pUBUmlYGgCBMluasKnNEac1Nt5oAYVNPRDipVTIp+w00BGB8tJipShxijyzTEIq5pcYqQJtFMblqAHpjFDDmhExUmKYDFApSKQtij7w4oATBpOaC4AOabEwY0APxQMilLYNPGKBCDkUUuKcBQAwilHSn4pKYxhGRiq0lucfIcGrXeloArIr7fn61HKtWyKilXigRQYVGRUzjBplIZQ1BsRCstq0NVOFUVmhgRzUs7qHwHS6OP9DWtZegrN00YtY8elaQrne5nLccOlBooNBIlLSClNABTe9L2pO9NCORYZFNHFDnjigD1rpO7oKfal4xScLTgM80gGgd6figkUZGKAuJ2oBoPqKTpQMQ/eoI5pO9OJ4oAMjtS9RTSeOKFNACjP5UZ4oY0cBaAHY+XmgU3dTgaAEBzkUUn9aUcfWgBB1FBAp31ppNMA203I9KXJx7UlIA7UopBmk70AB6+1KKKcKYDB1p2KMetKaQAecU08GlDYpDQAmMig5xigUUAHagUdqSgAPFHWkooATFMl/wBUx9qfTLg/uH/3TTIn8LKehf6l/wDe/pWqKzND/wCPZv8AeNadJkYb4A7Uho70hpHSHtRiig0AA60UDpSigAFAooWgBTRQeaQ8UAJS0UUAJ1NFAooAKKBRQAHrS0UlAhaAMnFIK0tE077deBXX91Hh5ff+6v49foKZnUnyq5t+G9P8m2+0yDDyj5Af4U/xPX8q2xRQKg89u+ohrL8Qtt0W59wB+taTnArI8TPjR2x/E6j9aaEynANsKD2qXFJEMIv0pzttXJqiQ7UwnNKvI5phjxnHekwE28UmynAEUH7tAFcffOelPC7ulKq/LzT4k4zQAqR4pxWnx1IyZoQDQuBUkfNKEwKkRMCmBERlqeOlKRioyCTQIlIGKj8rml5qRelAEBR93y9KlxxTlNKxAoAqyId3tTo0x3qUjdTUj2seaYCmJT1FRNCynK9PSpmjJIwcVIF4oEV/LLKD3pUJP3hg1ZApCopgQlT2pwFD7l+7zSpkjnrQMKQinUUAMxQelOIppoAZTXGRT8U1l4oEUZhUBq3LFgGqT8GgZm6ryyis1BlgKvakczAelVYh+9XHrUM76WkTqrJdsMY9qviqVrwi/Sri1zmLHikNFJQIUUppopaBAaTvS02mgORYU3JzTz3zSADNdJ3dA61YsbGe/maO3UMyrkgtjioMgVveD/8AkIzf9cv6ihEVJcsW0V/+Ea1P/niv/fwUv/CN6lt/1K5/3xS32u6lFfXEaXRVElZVXYvABPtUC+IdVJ5vD/37X/CjQzTqtX0Jv+Eb1IdIV/77Wj/hGtS/54r/AN9ioz4h1Tp9rP8A3wv+FIfEGq54vD/37X/Cgf73yH/8I1qf/PFf++1oPhrU/wDniv8A32tXNC1e/u9WihuLkvEwbK7FHb2FS+I9UvrLUFitbgxoYg2NinnJ9RRZEc9VS5dDN/4RnVM/6lcf9dFpw8NamP8Algv/AH8Wov8AhINW/wCfw/8Aftf8KP8AhIdV/wCfw/8Aftf8KDT975Eg8Nan3hX/AL+LQfDWpn/lgv8A38Wov+Ei1X/n8P8A37X/AAqzYa5qc1/bxyXRZHlVWGxeRn6UaCbqpX0KF7ptzp/l/aUC+Znbhgen/wCuqrda6bxocfY/+2n/ALLXMdaC6UnKHMxQ1KKmsIBc3sMDHAkcAn2ra1DWJtMu3tLOCGGKLjBTJb3NASk0+VI580x+G2lSK0J7xL2/glW3SFsqG2dGOetW/EVtPc63KsELyEKudi5xxRYOfWzMPt1pQMjinSwyQMUmjaNu4YYNTW1jc3SEwQSSAdSBxQXdWuVwKOAcc1LPbTW7bJ43jb0YYzWnb6XI+hTzG1kM+9RGNh3Y4yQPxNFhSmkrmP8AjRTpYnhcrNGyOP4WGDSxRySuI4kaRyOAoyaCrq1xvBpB0qzNp15bJvmtpEX+8RwKhiikmDeVGz7F3NgZwPWgSate4ykNW49OvXiMyWspTGQQvWqhz3oBNPZie1LgDqa09D06O9llmuTttrdd0h9farEniWWJilhbw28IPyjZkke9BDm72ijDzwOaWuhtnt/ECSQzQxw3yruSRBgP9awVtp3aRFidmjBLgDOAOuaLDjO909CLpQOtWo9MvXi85LWUpjOQvWqp/WgpNPYTFRXP/HvL/umtEaVftF5n2Sbb1zt5qPTlzqtmGH/LeMEH/eFBnOScXYzNFGLY/wC8a0etdDqut3Nlq1xbQxW/lR7du5OeVB9feo71bfUNEOpRwLBNE+2QIOG6f4ihmdCfLFXWhhlSBnBAphrd1Q/8U9ph/wB6seWCWFUaWNkDjKkjGRSOiM+ZakWKBz1qVbeVoWmWNjGnDOBwKfbWdxdE/Z4Xkx1KjgGlYq6tuQ9KTrUk1vNbybJ42jb0YYqOgaaaugpRSUDtQMU8U2nH7xptABRRRQAUtJ1ozQAd6XpSUUCF7UlHagDJAFMTdiSBGd+ELkkBUH8THoK7rTbFbCzEWdzk7pG/vMeprF8L2G8/bnX5FysAPc9Gb+grpaTPPqz5mNpaXtQKgyIz0rH8St/xL0X+9KorZNYniNysdqgx88tUhsYBgCh13Cn7aUCqJIsMG9qU08rRtoAiI5pCny8VJjIoPCmkBAw6CpV44piZY5NS7aABOKnHUU1FxUqrQADrThSCkEg3le9NCHMM0m2ngUtAEe3Jpsh2ipajlXcKBkYJHSpPvUqLhRTyMUCG0tA6UUAA604U3FKKYDqKNwA5pMgimAhFGKGcL1oyCKQhCKZupxNRtQMeDSNUanBp5GVoAarBulKRTAmzpSlqAI5BlazpyFbFaJ5rNvlX7xPSi4GJfHM7e1JZgGdRiknYO5I6UsGVlUjtSZ2xfunUQcYqyrVk21/GThjir8cyN0YVztGRZDUnWo91OBpAPXvS01TS5oAKSikoEcmelNIIp5B70xsnPHFdLO5bBmug8Hf8hCb/AK5f1Fc8Ohrf8Gf8hKbB/wCWX9RTRlW+BmTqhxql3/13f/0I1v6B4fje3S7vV3lxlIz0A9TWBqgB1S7/AOu7/wDoRr0aMKEXZjbgYx6U0ZVpuMIpEJ0+zKbDaw7fTyxXM+INCS1iN1aAiMH50649xXXVV1QL/Zlzv+75TZ/Kg5qc5RldHH+F/wDkOQ/Rv/QTU/i//kLJ/wBcV/mah8MnOuw/Rv8A0E1P4u51ZP8Ariv8zR0Ox/xl6GD1rdsPDzXOjS3DAiZ/mhHsP8apaLpp1G/WMgiJPmkPt6fjXfqqqoVQABwAKBV6vL7sTzAgg4xyPWrelcapaf8AXZP51o+J9N+y3guYx+6mPPs3/wBf/Gs7S/8AkK2n/XZP50uppzc0HJG740GTZ/8AbT/2WuZGa6bxp1sv+2n/ALLXMGhiw/8ADQ9JDGwdGKupyCOxrbGt214qrqtishAx5sfX/P41kWf2dLlDdKzw5+YKea1DpulM3mR6sqRHnYy/MP8AP0oQ6nLf3hmoadFZXVpNbSGS3nIZC3Ucj/GpvEt/dxao0EM7wxqqn92dpJx3I61BqOowXF1aRW2Ra2uFVm79Mn9KZr80Vzq0kkMiyIVX5lOR0pkRTbjzFrVC17oenXEpzOzmMv6jJ/wpPENzLZzRWFrI8MEMY4Q7cn3xUN5cwt4dsYUlUzJIxZAeV5aprqSz1pY55LpLS7VdjiQfK3uD+dIlKzV9tQtZX1Lw7eLcsZHtsPG7cke2fwNOgu7g+FbiQXEodZgqtvOQPl4zUE9za6fpUljaTfaJJ2zLKowoHoKXTZrafRbmwmuEt5GkDqz/AHT0/wAKYNaXtpcx5JZJZC8sjyOepdtxrcs3Nh4alu4MLPPJ5Yk7qP8AINY91FFDO0cE4nQY+cLgE1oabe20lhNpt85jjdt6SgZ2H3pI1mrxVth2hahc/wBpRwyTPLFMdjpI24HP1q7o8Itda1KJACscbYB54yKrWcen6VMLuW9S5dM+XHEOp96bo9/Et9e3F1IsbTRt19SelMymr3cSCx1G9bV4He5kbfKoZS3ykE4xjpTPEMaxa1dIgwNwP4lQf61XsXCXtu7naqSqSfQZqfXriO51i4mhcPG23DDv8opGqilPTsXtJw/hjU0T74O4/wC7gf4GsE9KvaRqbabcl9nmRONskfqKuSWGj3L+Zbamturc+XKvK0CT5JO/Uh8MBjrtvt6LuJ+m01paS4Ov6qygFdshAPQ/NVVb6w0aCRNPkNzdyDaZiMBR7VB4fuoLae5aeVUDwMoLdzmmZzTmnIZp+p3r6rA7XUrb5VDKW+UgnpjpWtBZwt4yutyjZCvmgHpuwP8A4rNc5YuI763djtVZFLH0Ga159Wjt/FE15CwlgYBW29xtGf1H6UkVOGvu9hGa9a6+0f25Zh92QouG2j2xjpTL9rZ/FFpJayI6ySxMxQ5G7cM02XTdMkkMsGqwxwsc7XU7l9sd6pb7O21e3eGZjbRzRsZJBjgMMn6UzNpWujd1ez0x9XuJbjUTFK23dH5ZO35R3qnqGo2q6eun6eHMO7c8j8FzVXWbmC71e4ntpFlifbtZTkH5QKp44pM0oQvFNm1qg/4p7TP+BU3xD/x5aV/17/0FM1C4ik0TT4UlRpI87lB5WppHtNU021SS6S2uLZdhEnRh/kUgXu2b7sbYf8ipqH/XRf5rU1qftuiw2ljdrbXCMS6Ftpk/GovPsbfQryzguDJKzKdxG0Ocjp7cVUhsbK4tkYahHFN/Gkw2/kaAsndvuM1JNQhaOLUGkOwHZvbd+RqjWtq93C9ra2cMxuPIB3TH+InsKyqT3N6b93UbThQKT2oNAJpKU0UCG0o6UcUcetAXDNBo70UDACiijvQICau6dYveXEdsuQ0vMjf3I+5+p6VS3IvzP91ecf3vau10HTmsrQyT/wDHzP8ANJ/s+i/hQctep9lGlFGkMSRxqFRAFVR2FOopDUnGFL2pKXtSAYawvEHzXWnp/wBNCf5VumsHV/m1iwXGcZaqQy2UpAntU5FIQB1qiSEpTfLwKnK03FAECptJpkvoO9WCuKhVdzE0AIsfA4p+3GKkQcdKcRx0pANApwxjijFIi4HFMAPHSjbnnvT8UoWgQnak+90p+2kI9KYEflndncfpTs0bGGcGlI4osAq9KDQOBSMc0gAdacBSIKkGKAGmjFOK0mKYhkkYdcUwJsXAqakIoGUnfIORT4nytSvGKYqhelFwA0hpTUbZFIB2KkHAqJc0O+xc0wHtUMi5qRW3KDTGoAgJNUb5WaFsVolajeMMMEUho5JVIODVhY2K5XpWjPppyWj/ACquI5Yl2slNG/tCm0DDuachnT7rmrRYYO4VVZmHQ0WGpFhL25RvvZq2msMo+dKzVmK/eGalBjeocUO6NmDVIJOp2n3q4kqvyrA1zJhA5WlSWWL7rNUuAaHT76N1c8NQmUAsc1ag1ZMYcYqORktGWTzTCx6U5/emHrXQdqENdD4L/wCQlN/1y/qK58dDXQ+C/wDkJTf9cv6ihGVb4GZGpj/iaXfP/Ld//QjXRaBr8S26Wt6+xkGEkbgEeh9Kzr7Q9Slv7mRLUlHldlO5eQSfeohoGp/8+jf99L/jR1IfJOCTZ2xvLYJvNxEF/vbxiuZ8Q67HcxG0s23Rk/PJ2PsKzj4f1T/nzP8A30v+NO/4R/U8c2p/76X/ABpkQp04u7kO8Mf8h2Hjs38jU3i/jV0/64r/ADap9C0m/tdWimntykahstuB7fWpPEul3t7qKS20BkQRBcggc5PrRbQrmj7a9yHSNcsNMs/LMM7SMcuwVeT+dXx4vsv+fe5/75X/AOKrBPh/U/8An0b/AL6X/GlGgaoB/wAep/76X/Ggbp0pO7Zqah4h0++s5IJILkbxwdq/Kex+9WFpR/4mtoCMfvV/nVr+wdUx/wAejf8AfS/41NYaJqUOoW8j2xCJIpJ3LwM/Wga5IRaiy940+9Zf9tP/AGWuYWun8aHBsv8Atp/7LXMZ5BpMrD/w0L3GKDV7Q1V9VhVgGB3cMOPumrEqF9NlkujA7b0CyQKrFB3yU7fWguU7OxlAHFN5zV6SxaKS6SRwFt1zvx97P3cfXNOtilvpr3QRHlaXyk3rkJxknHryKLA5roZ54NB6VoPHfXiwCSBMSOFSURhN2e2RUMlnGIJXt7nzjD99dm3jOMj1GaLAprqU85FL2rQl0tI5p4Vug1xEpfZsIDADJ59cVFeGSGa2dtjERRuuEAGMZAPrRYFNPRFP6UueK1TZQvrCuBizdftPsE6kfnkVBFbx6jOxWUJPKSyxLGdq+gJ7UWEqiKI+tJ2q2bKJLOOeS5CmVC0cYQksQSMe3TrVy3s4raW6jecPOltJuTZwp29m9RRYbqJGOM44pT0rYms4bk2MXniOaS3UKuzgnJ6ntVGztIrp1jNxsmdtqp5ZPPbJ7frRYFNWKf1peMc1eWwiEUMlxc+V5rMgAQsQQcflVS4ia3uJIW5aNipx7UWGpJuxF396U9KB+tBpFAKKKOKACq1+f9Cl+lWOvSq2ocWUv0/rTMqvwMfpXFjH+P8AOroqppg/0CP6Vb9aTHQ+BAOtIelL2pOopGwnvR3oNL2oGGOtA5NJS0CE70HrS000DCijtRQAdqSiigApRSetKDQIB0NJSk1NbW0lzLHFEP3kzbU9vU0IznLlVzT8OaYLu8W5kGYbc8cfek/+t/hXYVBZ2kVjaR28I+RBj6nuampM89vmdxaQ0tN71IgpaSjtQAhrBv5XPiG3iDfL5fI/Ot01z05z4p/3I/6CqiJ7GsBigqG60DkZpaokQ0lOxSHigCCY4GO9Ea4FMO55hjoKtBKBjQtLg0/FKBQBHikAp7A03FAAKcKYKfQAtNNKGBprpvxg4xTJHLQRQOKUmgYw0zGTTmPFC9KQDlFHSmNIqDk09WzQAoalppGaUcUwHU00jOB1NGaAGMOKjXipTURpAJupOSaWgUDDFNPPWnnpSYoENGOlIy0/FNY0ARbDmjbUooIoAhZMiojCD1FWsUbKBlCS0Rh92qr6ah6CtjZTSlAXOfm09yeKrPbvH1zXTGIFqYYFbqooK5jm/MKgU8SBq230+J/4BVSXSFBOzjNBSkZoKM2KbIEHSrbaXKucc1VktZUbDKaCuYRjUZ5zTj2NIehoO4aau6XqkulzPLCiOzLtO+qQPPSl4zzTE0pKzOh/4TC9/wCeFv8Ar/jSjxden/lhB+v+Nc+BxSrx3oM/YU+xvnxdeg/6iD8j/jTh4svMZ8iD8j/jXPZzS9unFAexh2N0+Lr3/nhB+v8AjR/wl19/zwt/1/xrBPSjb70B7Cn2Oh/4Sy9/54Qfr/jR/wAJbef88YP1/wAa5/BHejPzUB7Cn2N//hLb3/nhB+v+NH/CW3v/ADwg/X/GsFuKBQHsKfYvapq02qCIzJGhjzjbnvj/AAqiBkUh5/CjjNJmsYqKsizYXIs7xJym8JnKg4zkEf1p/wBqhjtpYLaGRfOwHaRwcAHOBgCqfTg0vPFFxOKbuaF7dbtPtLcFSwXMjKc9MhQfoP51Xt7pUgkt5o/NhchsBsFW9QagOOlNp3EoK1i8moR20caWcTqVmEu6RgckAjGAOnNMkvIVhnS1geNpuHLSbsDOcDgVUGabRcPZxNjUb6BL+6ktoy8rgoJRICuCMEgY6496zrq4+0GIhNuyJI+T1wMZqvzSrz1ouEYKJeGoldKNns+YnAkzyEJyV/MVNZ6wtslviOYGHgoku1H5PzEY5PNZh603BoB04ssTXHm29tFtIMKFc565Yn+tWn1OFpZ5xbMJ542Rz5nygkYyBis00o5xRcfImXU1ALd2Uxj/AOPZFUjP3sEn+tS2mrLbRQgJKGifcRHIFWTnPzcc+lZnOaT2ouJ04stzXvmRQRiMjynd8567iD/So7uf7RdzT42+Y5bB5xk1APrS0hqKQEc0h5pTSYoKEzSjmiigA71V1Pixk/D+dWu1VNT/AOPGT8P50zKt8DLGncWMX+7VjpUNgMWMX+6KnqXuVR+BCUvbFCjmkNBsJiloo7UAB60UUGgANJ3paDQAh9qPxpaSgVxKO1Heg0AJ1ooFKOW2jvxQJj4Yt7cnCLyTXV+G7ApEb6VcPKMRKf4I+359fyrK0vTVvLlLcjMMWHnPr6L+P8q7Htihnn1anMwNJQaSoMhKQ0tJ3oGLR2opDQAw1gL83iq4P92Nf/QRW+1c9and4lvCeygVUQZtUtNzSg1RAHpULFgnzdalJqAndJ7CmBJEmBU1MXpTqAEpaQmkzTAfmkK0A0tIBuOKQIcc08U6gCpIrIh20+M5UZp8nSooW5IoAkNNJNPpMUgGDrTqUCloQEEkW9gT2pkjSRkbRkd6t00gUwCNtyinUijFOoAryxrIQGzTwMDFPamZ5xSAQ1UZ3E2MfLVwionXNIZHmnA0mMUUAPDUGmFfl4qPLDrTETUhGaaDmo2m2yBT3oAnAoIoU0ooAbilXgc0tIKBgaQ0ppKVwI1HzGpAtNA+apBQAgWnGLNPVaeBTAqNFjtUMkSnqtXyKjZAaAOKIPQ0jcLgUh44pOooPTQgNCn3pFz3pxAoHoOzRjmigdKYDwMHHagjApBSk8GgBuAKXBxmkJ6Gl9KADjqetL3oxSUWGBFHeg8DNIDQK4uBSnFIWIFGaBi5zR0pv409c4pWC4hyBSdqDzQT0phcSkpcmk60AHejtSGl6UguIWyOKUHim475pw6daYAaSjpQaQICeaB1xR05oHWgLjj+lN70d6TnNAC96Tml702gBaUUUd6ADBxVPVOLF/qKuE1S1T/jyb/eFMwrfAy7ZD/Qov8AcFS980y0H+iRD/YFSDpUs0o/Ah1MPXFBNFBqIaWkooGLSUA0ZoAKOnWlzg0E5NFhAxwOKTtR1pOlAXA0dqKQmgQE1JbkhgUXfKzBY0/vMah/HFdJ4U04yOdSmX5QNluvt3b+n50znrzsrG7pdiLCyWHO6Q/NI/8AeY9TVs0tFScQ00lDUUhiGkpTSetIAoNFBpDGNXOWQL6zfuP75H5E10ZrnNHP+l6hJ1/en/GriSzQEj5I64qRZR34pyDIzjGaa6CqELJLhc0Q9MnrVZx82KsoCooAnBpai8xV+9xTw2aoQppku7YdnWlYZxS0guMRsJ81PR1dcqciggEc0iqFXC8CmAxw5mUg/KKnBplGaAFYZFQIrLJjHy+tThqKAEctxtGaUUClpAGKMGlzRmgQmKay8dafR1oAjXgVHJN5ZG7oamxUUsQcEGgY/ORTTxQgKrg0jUhoXzBimyNhSRzTWQMuDRjApAMjkEg6YqQgUwAZp9ACAgimsM0h+XpQrZpiGqpBpHiDkE9RU1GKAGAU4GmtxUaTZyO4oAlLUgbIqJnpM1Nxk9FMU08c0AJ3qRaYaVWoAmXpS0xWpxNMAzTSaQmkPFMR/9k=";	    
	    
	    //data.append("file", multipartFormData("data:image/jpeg;base64"+fileData));
	    
	    //data.append("file", dataURItoBlob("data:image/jpeg;base64"+fileData));
	    
	    data.append("imageData", fileData);
	    
	    data.append("keywordData", JSON.stringify(getJsonData(isEdit,element)));
	    
	    //url = domainUrl+"/Business-core/file/upload";
	    
	    $.ajax({
            url: url,
            type: "POST",
            data: data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false
          }).done(function(data) {
        	  if(data.status){
        		  buildOutKeywoards(JSON.parse(data.keywordList).outList);//buildOutKeywoards(data.keywordList);
        		  $('#keywordsContentOut').click();
        	  }
        	  else
        		  bootbox.alert("Entered Keyword is already available");
          }).fail(function(jqXHR, textStatus) {
              //alert(jqXHR.responseText);
              alert('File upload failed ...');
          });
	    
	}else{
		if(!isEdit)
			validationErrorMsg(false);
	}
}

function onChangeEventInMiles(elem,isEdit){
	if(!isEdit)
		loadMap(lang, lat,"mapdiv",$('#miles').val());
}

function validateAddKeyword(isEdit,element){
	
	var valid = true;
	if(!isEdit){
		if(valid && ($('#outKeyword').val() == '' || $('#outKeyword').val() == undefined))
			valid = false;
		if(valid && ($('#milesOut').val() == '' || $('#milesOut').val() == undefined))
			valid = false;
		if(valid && $('#messageTextarea').val() == '' || $('#messageTextarea').val() == undefined)
			valid = false;
		if(valid && 30<parseFloat($('#milesOut').val()))
			valid = false;
	}else{
		if(valid && ($('#milesHeaderOut').val() == '' || $('#milesHeaderOut').val() == undefined))
			valid = false;
	}
	return valid;	
		
}

function validationErrorMsg(inscreen){
	var message = "Please fill the required fields.";
	if(30<parseFloat($('#milesOut').val())){
		message = "Miles should not more than 30.";
	}
	if(inscreen && 30<parseFloat($('#miles').val())){
		message = "Miles should not more than 30.";
	}
	bootbox.alert(message);
	
}

function keywordSaveSuccessMsg(data){
	
	if(data>0){
		$('#keyword').val('');
		$('#radius').val('');
		$('#message').val('');
		bootbox.alert("Your Keywords is received by -"+data+" Users");
	}
	else
		bootbox.alert("Your Keywords is not received any users");
}

function getKeywordData(){
	
	var data = {};
	data['userId'] = userId;
	
	var url = "";
	if(callLocalData)
		url = "JsonData/inHomeKeywordData.js";
	else
		url = domainUrl+"/Business-core/keyword/list";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: JSON.stringify(data),
		success: function(data) {
			if(callLocalData)
				buildKeywoards(JSON.parse(data));
			else
				buildKeywoards(data);
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}


function getHotTagList(){
	
	var data = {};
	var kilometer = $('#milesOut').val();
	data['userId'] = userId;
	data['kilometer'] = kilometer;
	
	var url = "";
	if(callLocalData)
		url = "JsonData/inHomeKeywordData.js";
	else
		url = domainUrl+"/Business-core/keyword/getHotTag";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: JSON.stringify(data),
		success: function(data) {
			console.log(data);
			var list = "";
			var tags = JSON.parse(data);
			var size = tags.hotTags.length;
			console.log(size);
			var i = 1;
			 $(tags.hotTags).each(function(index,item){
			 	
			 		console.log(item);
			 		if(i != size )
			 		{
			 						 	list  = list + '<li>'+i+'.<span>'+item.tag+ ' </span> <button type="button" style="float: right" class="btn btn-link hottagbutton">Select</button></li><hr>';
			 		}
			 		else
			 		{
			 						 	list  = list + '<li>'+i+'.<span>'+item.tag+ ' </span> <button type="button" style="float: right" class="btn btn-link hottagbutton">Select</button></li>';

			 		}

			 		i++;
				  
				 });
			$('.modal-body-hottag ul').html(list);

			 $('#hottagModal').modal('show');
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}



function getOutKeywordData(){
	var data = {};
	data['userId'] = userId;
	
	var url = domainUrl+"/Business-core/keyword/outList";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: JSON.stringify(data),
		success: function(data) {
			//buildOutKeywoards(data)
			buildOutKeywoards(JSON.parse(data)['outList']);
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}

function getMessageData(){
	var data = {};
	data['userId'] = userId;
	var url = domainUrl+"/Business-core/msg/list";
	$.ajax({
		url: url,
		type: "POST",
		contentType:'application/json',
		data: JSON.stringify(data),
		success: function(data) {
			buildMessages(data)
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}

function buildMessages(data){
	var html = [];
	
	$(data).each(function(index,item){
		var arraows = 'fa fa-angle-double-up';
		var rel = "messageUp";
		if(userId == item.sender.userId){
			arraows = 'fa fa-angle-double-down';
			rel = "messageDown";
		}
		html.push('<div class="panel panel-default" rel="'+rel+'">');
		html.push('		<div class="panel-heading">');
		html.push('			<a href="#faq-3-'+index+'" data-parent="#faq-list-3" data-toggle="collapse" class="accordion-toggle collapsed">');
		html.push('				<i class="ace-icon fa fa-plus smaller-80" data-icon-hide="ace-icon fa fa-minus" data-icon-show="ace-icon fa fa-plus"></i>&nbsp;');
		html.push('				<img src="avatars/avatar.png" class="msg-photo" alt="Alexs Avatar">');
		html.push('				<span class="msg-body">');
		html.push('					<span class="msg-title">');
		html.push('						<span class="blue">'+item.receiver.userName+'</span>');
		html.push('						'+item.keyword.keywordName+' <span class="blue ace-icon '+arraows+' bigger-120"></span>');
		html.push('					</span>');
		html.push('				</span>');
		html.push('			</a>');
		html.push('		</div>');
		html.push('		<div class="panel-collapse collapse" id="faq-3-'+index+'">');
		html.push('			<div class="panel-body">');
		html.push(				item.message);
		html.push('			</div>');
		html.push('		</div>');
		html.push('	</div>');
	});
	
	$('#faq-list-3').html(html.join(''));
}

// added by sri
function float2int (value) {
    return value | 0;
}


function buildKeywoards(data){

	var html = [];
    
	$(data).each(function(index,item){

		var keywordName = item.keyword.keywordName;

		if(keywordName.length > 8)
		{
			
			keywordName = keywordName.substr(0,8);
		keywordName = keywordName +"..."
		}

		if(item.kiloMeter)
		{
			var miles = float2int(item.kiloMeter);
		}
		else
		{
			var miles;
		}

		if(miles > 1)
		{
			var mileKeyword = "miles"
		}
		else
		{
			var mileKeyword = "mile"
		}


		html.push('<div class="col-xs-12 inHomeMsgStyle" style="background-color:white;">');
		html.push('		<div class="col-xs-11" onClick="buildChatDetails(this,true)" radius="'+item.kiloMeter+'" matchingCount="'+item.matchingCount+'" userKeywordId="'+item.userKeywordId+'" userKeyName="'+item.keyword.keywordName+'">');
		html.push('			<div class="col-xs-2 multiUserIconStyle" style="background-image:url(images/group.png)">');
		if(item.matchingMessageCount > 0)
			html.push('				<span class="badge badge-notify-in">'+item.matchingMessageCount+'</span>');
		html.push('			</div>');
		html.push('			<div class="col-xs-8 keywordHomeStyle"><strong class="bigger-120">'+keywordName+'</strong></div>');
		html.push('			<div class="col-xs-2 milesHomeStyle">');
		html.push('				<span><strong>'+item.kiloMeter+'</strong><br><strong>'+ mileKeyword +'</strong></span>');
		html.push('			</div>');
		html.push('		</div>');
		html.push('		<div class="col-xs-1 keywordHomeStyle" style="margin-left: -23px;">');
		html.push('			<input type="checkbox" class="faChkRnd" rel="inRadioButton" onClick="deleteKeyword(this,true)" userKeywordId="'+item.userKeywordId+'" userKeyName="'+keywordName+'" >');
		html.push('		</div>');
		html.push('</div>');
		
	});
	

	$('#inContentDiv').html(html.join(''));
	$('[rel="inRadioButton"]').hide();
	
}

function getKeywordMonthAndDate(date){
	var dateObj = new Date(date);
	
	var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
	return monthNames[dateObj.getMonth()]+' '+dateObj.getDate();
}

function buildChatDetails(element,status){
	
	keywordDeleteRadioButtonToogle('others');
	setCurrentKeywordDetails(element);
	
	var data = {};
	data['userKeywordId'] = $(element).attr('userkeywordid');
	data['userId'] = userId;
	var url = "";
	
	if(callLocalData)
		url = "JsonData/inHistoryMatchingUsers.js";
	else
		url = domainUrl+"/Business-core/message/getMatchingInUser";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			if(callLocalData)
				constructMatchingUsersList(data.matchingUsers,$(element).attr('userKeyName'));
			else
				constructMatchingUsersList(data.matchingUsers,$(element).attr('userKeyName'));
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
	
	$('[rel="profileIcon" ]').hide();

}

function setCurrentKeywordDetails(element){
	//header text
	var name = $(element).attr('userKeyName');
	if(name && name.length > 8)
	{
		name = name.substr(0,8);
		name = name +"..."
	}
	$('[rel="inKeywordName"]').html(name);
	$('#rangeInputHeader').val($(element).attr('radius'));
	$('#milesHeader').val($(element).attr('radius'));
	$('#milesHeader').attr('oldMiles',$(element).attr('radius'));
	$('#milesHeader').attr('userKeyName',$(element).attr('userKeyName'));
	$('#milesHeader').attr('userKeywordId',$(element).attr('userKeywordId'));
	
	//hidden variables
	$('#inActionDeleteBack').attr('userKeywordId',$(element).attr('userKeywordId'));
	$('#inActionDeleteBack').attr('userKeyName',$(element).attr('userKeyName'));
	$('#inActionDeleteBack').attr('radius',$(element).attr('radius'));
	
	//keyword history in screen
	$('#keywordHistoryIn').attr('userKeywordId',$(element).attr('userKeywordId'));
	$('#keywordHistoryIn').attr('userKeyName',$(element).attr('userKeyName'));
	$('#keywordHistoryIn').attr('radius',$(element).attr('radius'));
}

function setCurrentKeywordOutDetails(element){
	//header text
	$('[rel="inKeywordName"]').html($(element).attr('userKeyName'));
	$('#chatOutBackButton').attr("onClick","backButtonOutEventStep2('outHeaderThird')");
	
	//clear old message and values
	$('#countMatchingMsgHeader').html("");
	
	//hidden variables
	$('#outActionDeleteBack').attr('userKeywordId',$(element).attr('userKeywordId'));
	$('#outActionDeleteBack').attr('userKeyName',$(element).attr('userKeyName'));
	$('#outActionDeleteBack').attr('radius',$(element).attr('radius'));
	
	//keyword history out screen
	$('#keywordHistoryOut').attr('userKeywordId',$(element).attr('userKeywordId'));
	$('#keywordHistoryOut').attr('userKeyName',$(element).attr('userKeyName'));
	$('#keywordHistoryOut').attr('radius',$(element).attr('radius'));
}

function buildOutKeywoards(data){

	var html = [];

	$(data).each(function(index,item){
		
		var keywordOut = item.userOutKeyword[0];
		var fileName = "images/blank_out_keyword_image.png";
		if(keywordOut.fileName != null && keywordOut.fileName != "")
			fileName = keywordOut.fileName;
		html.push('<div class="form-group outHomeMsgStyle">');
		html.push('		<div class="col-xs-3" style="background-color:white;padding-top: 8px;margin-bottom: 7px;">');
		//html.push('			<div>');
		//html.push('				<strong class="outTimeDetails">'+getOutFormatDate(keywordOut.insertedTime)+'-'+keywordOut.kiloMeter+' mi</strong>');
		//html.push('			</div>');
		html.push('			<img src="'+fileName+'" class="ChatImageStyle" onclick="openImageZoomPopup(this)" id="chatImageDiv">');
		html.push('		</div>');
		html.push('		<div class="col-xs-9">');
		html.push('			<div style="width: 100%">');
		html.push('				<div>');
		html.push('					<div onClick="buildOutChatDetails(this,false)" radius="'+keywordOut.kiloMeter+'" userKeywordId="'+keywordOut.userOutKeywordId+'" userKeyName="'+keywordOut.keyword[0].keywordName+'" class="row col-xs-12" style="padding-left: 20px;">');
		html.push('						<strong class="pull-left">'+keywordOut.keyword[0].keywordName+'</strong><br>');
		html.push('					</div>');
		html.push('					<div class="col-xs-12" style="height: 36px; width: 100%;">');
		html.push('						<div class="row col-xs-11" style="font-size: 12px;" onClick="buildOutChatDetails(this,false)" radius="'+keywordOut.kiloMeter+'" userKeywordId="'+keywordOut.userOutKeywordId+'" userKeyName="'+keywordOut.keyword[0].keywordName+'">'+getCharacterRestriction(keywordOut.keywordDesc)+'</div>');
		html.push('						<div class="col-xs-1 pull-right" style="margin-top:-33px;width:20%">');
		html.push('							<input type="checkbox" class="faChkRnd" rel="outRadioButton" onClick="deleteKeyword(this,false)" userKeywordId="'+keywordOut.userOutKeywordId+'" userKeyName="'+keywordOut.keyword[0].keywordName+'" style="margin-top:-20px;">');
		html.push('						</div>');
		html.push('					</div>');
		html.push('				</div>');
		html.push('			</div>');
		html.push('			<div class="col-xs-12">');
		html.push('				<div class="row" style="padding-left:10px">');
		html.push(					getUsersFromKeywordOut(keywordOut.messageMaster,keywordOut.keyword[0].keywordName, keywordOut.kiloMeter, keywordOut.userOutKeywordId,keywordOut.keyword[0].keywordName));
		html.push('				</div>');
		html.push('			</div>');
		html.push('		</div>');
		html.push('</div>');
		
	});
	

	$('#outContentDiv').html(html.join(''));
	$('[rel="outRadioButton"]').hide();
	
	$('#outDeleteAction').unbind('click').bind('click',function(){
		outKeywordDeleteRadioButtonToogle();
	});
	
	//out content
	$('#saveNewKeywordOut').unbind('click').bind('click',function(){
		keywordSubmit();
	});
}

function getCharacterRestriction(keywordDesc){
	if(keywordDesc.length > 59){
		return keywordDesc.substring(0, 58)+" ..."
	}else
		return keywordDesc;
}

function getUsersFromKeywordOut(data,keywordName,radius,userKeywordId,userKeyName){
	
	var html = [];
	var dataLength = data.length;
	$(data).each(function(index,item){
		
		if(dataLength <= 5 || (dataLength > 5 && index < 4)){
			var profilImg = "images/blank_avatar.png";
			if(item.chatProfileImg != null && item.chatProfileImg != "")
				profilImg = item.chatProfileImg;
			if(item.matchingMessageCount != ""){
				html.push('	<div class="container col-xs-3 outUserProfileNewNotification" onClick="callDirectChatAccess(this,false)" keywordName='+keywordName+' messageMasterId='+item.chatMessageMasterId+' UserId='+item.chatUserId+' userName='+item.chatUserName+' insertDate='+item.chatInsertDate+' style="background-image:url('+profilImg+')">');
				html.push('		<span class="badge badge-notify-out">'+item.matchingMessageCount+'</span>');
			}else{
				html.push('	<div class="container col-xs-3 outUserProfile" onClick="callDirectChatAccess(this,false)" keywordName='+keywordName+' messageMasterId='+item.chatMessageMasterId+' UserId='+item.chatUserId+' userName='+item.chatUserName+' insertDate='+item.chatInsertDate+' style="background-image:url('+profilImg+')">');
			}
			html.push('	</div>');
		}
	});
	
	if(dataLength>4 && dataLength != 5){
		html.push('	<div class="container col-xs-3 outUserProfile" onClick="buildOutChatDetails(this,false)" radius="'+radius+'" userKeywordId="'+userKeywordId+'" userKeyName="'+userKeyName+'" style="background-image:url(images/blank_avatar.png)">');
		html.push('		<span class="badge badge-notify">More</span>');	
		html.push('	</div>');
	}
	return html.join('');
}

function getOutFormatDate(inserterdTime){
	
	var date = new Date(inserterdTime);
	
	const MONTH_NAMES = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
		];

	const d = new Date();
	return MONTH_NAMES[date.getMonth()]+ " "+date.getDate()+"th" ;
}

function buildOutChatDetails(element,status){

	setCurrentKeywordOutDetails(element);
	
	var data = {};
	data['userKeywordId'] = $(element).attr('userkeywordid');
	data['userId'] = userId;
	var url = domainUrl+"/Business-core/message/getMatchingOutUser";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			constructMatchingOutUsersList(data.matchingUsers, $(element).attr('userKeyName'));
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});

	$('[rel="profileIcon" ]').hide();
}


// added by sri for capitalize words
function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function constructMatchingUsersList(data,keywordName){
	var html = [];

	if(data != null && data.length>0){

		html.push('<hr class="col-xs-12" style="margin-top:1px;margin-bottom:1px">');
		$(data).each(function(index,item){
			var messageDescription = "";
			var originalMessage = item.messageDescription;
			var userName = jsUcfirst(item.userName);

			if(originalMessage && originalMessage.length > 25)
			{
				messageDescription = originalMessage.substr(0, 25);
				messageDescription = messageDescription + "...";
			}
			else
			{
				messageDescription = originalMessage;
			}
			//$('[rel="inKeywordUserName"]').html(''+userName+'');

			html.push('<div class="form-group col-xs-12 inHomeMsgStyle" style="background-color:white;">');
			html.push('		<div messageMasterId="'+item.messageMasterId+'" keywordName="'+item.keywordName+'" userName="'+item.userName+'" insertDate="'+item.insertDate+'" onClick="startScreenStep(this,true,true,false,true)">');
			var profileImage = "images/blank_avatar.png";
			if(item.profileImg != "" && item.profileImg != null)
				profileImage = item.profileImg;
			//html.push('				<img src="http://i.imgur.com/GNdhDFK.png" class="ratio img-responsive img-circle " alt="example"/>');
			if(item.status == "new"){
				html.push('			<div class="container col-xs-3 outUserProfileNewNotification" style="background-image:url('+profileImage+')">');
				html.push(' 		<span class="badge badge-notify-new">New</span>');
			}else if(item.matchingMessageCount !=0 ){
				html.push('			<div class="container col-xs-3 outUserProfileNewNotification" style="background-image:url('+profileImage+')">');
				html.push(' 		<span class="badge badge-notify">'+item.matchingMessageCount+'</span>');
			}else{
				html.push('			<div class="container col-xs-3 outUserProfile" style="background-image:url('+profileImage+')">');
			}
			html.push('			</div>');
			html.push('			<br><label class="col-xs-8"><font color="#808080">');
			html.push('			<span><strong style="text-transform: capitalize;">'+item.userName+'</strong><span><span class="pull-right">'+getKeywordMonthAndDate(item.insertDate)+'</span><br><br>'+messageDescription+'</font></label>');
			html.push('		</div>');
			html.push('		<div class="col-xs-1" style="margin-left: 14px;">');
			html.push('			<input type="checkbox" class="faChkRnd" rel="inHistoryRadioButton" onClick="deleteKeywordInHistory(this,true)" userName="'+item.userName+'" keywordId="'+item.keywordId+'" keywordOutId="'+item.keywordId+'" keywordName="'+item.keywordName+'" messageMasterId="'+item.messageMasterId+'" style="float:right;margin-top:13px;">');
			html.push('		</div>');
			html.push('</div>');
			html.push('<hr class="col-xs-12" style="margin-top:1px;margin-bottom:1px">');
		
		});
	}else{
		html.push('<p style="padding:35px">No one in your area has pinged for "'+keywordName+'". Try increasing your radius</p>');
	}
	
	$('[rel="chatScreenStep1"]').html(html.join(''));
	
	//show hide out history page
	showHideOutHistoryPage();
	//refresh initial screen load event
	loadRefreshData();

}

function showHideOutHistoryPage(){
	$('[rel="inHistoryRadioButton"]').hide();
	
	$('[rel="chatScreenStep1"]').show();
	$('#addNewkeywordContent').hide();
	$('#inContent').hide();
	
	//header
	$('#inHeaderThird').show();
	$('#inHeaderSecond').hide();
	$('#inHeaderFirst').hide();
	//footer
	$('#addNewKeyword').hide();
}

function getFormatedDate(timestamp){
	var date = new Date(timestamp);
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'pm' : 'am';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = hours + ':' + minutes + ' ' + ampm;
	return strTime;
}

function deleteKeywordInHistory(element,inAction,callChat){
	if($(element).prop("checked") || callChat){
		var body = 'Are you sure you want to delete "'+$(element).attr('keywordName')+' - '+$(element).attr('userName')+'" ?';
		bootbox.dialog({
		    title: "Delete Keyword",
		    message: body,
		    buttons: {
		        confirm: {
		            label: '<i class="fa fa-check"></i> Delete',
		            className: "btn-success",
		            callback: function() {
		            	deleteCurrentHistoryKeyword(element,inAction);
		            }
		        },
		        cancel: {
		            label: '<i class="fa fa-times"></i> Cancel',
		            className: "btn-default"
		        }
		    },
		    callback: function (result) {
		        console.log('This was logged in the callback: ' + result);
		    }
		    
		});
	}
}

function deleteCurrentHistoryKeyword(element,inAction){
	var data = {};
	data['messageMasterId'] = $(element).attr('messageMasterId');
	data['userKeywordId'] = $(element).attr('keywordId');
	data['userOutKeywordId'] = $(element).attr('keywordOutId');
	data['userId'] = userId;
	
	var url = "";
	url = domainUrl+"/Business-core/message/deleteConversation";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			if(inAction){
				$('#inHeader').click();
				//$('#inActionDeleteBack').attr('userKeywordId',$(element).attr('keywordId'));//messageMasterId
				buildChatDetails($('#inActionDeleteBack'),true);
			}
			else{
				$('#outHeader').click();
				buildOutChatDetails($('#outActionDeleteBack'),true);
				//backButtonOutEventStep2('outHeaderThird');
			}
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}

function constructMatchingOutUsersList(data, keywordName){
	var html = [];
	
	if(data != null && data.length>0){
		$(data).each(function(index,item){
			html.push('<div class="outHistoryMsgStyle form-group col-xs-12" style="background-color:white;">');
			html.push('		<div class="row col-xs-11" messageMasterId="'+item.messageMasterId+'" userName="'+item.userName+'" keywordName="'+item.keywordName+'" insertDate="'+item.insertDate+'" onClick="startScreenStep(this,false,true,false,true)">');
			html.push('			<div class="row col-xs-12">');
			var profile = "images/blank_avatar.png";
			if(item.profileImg != "" && item.profileImg != null)
				profile = item.profileImg;
			if(item.status == "new"){
				html.push('			<div class="container col-xs-3 circleoutchat-notify" style="background-image:url('+profile+')">');
				html.push(' 		<span class="badge badge-notify-new">New</span>');
			}else if(item.matchingMessageCount !=0 ){
				html.push('			<div class="container col-xs-3 circleoutchat-notify" style="background-image:url('+profile+')">');
				html.push(' 		<span class="badge badge-notify-outhistory">'+item.matchingMessageCount+'</span>');
			}else{
				html.push('			<div class="container col-xs-3 circleoutchat" style="background-image:url('+profile+')">');
			}
			html.push('				</div>');
			html.push('				<label class="col-xs-9"><font color="#808080"><br><strong style="text-transform: capitalize;">'+item.userName+'</strong><span class="pull-right">'+getKeywordMonthAndDate(item.insertDate)+'</span><br><br>'+item.messageDescription+'</font></label>');
			html.push('			</div>');
			html.push('		</div>');
			html.push('		<div class="col-xs-1">');
			html.push('			<input type="checkbox" class="faChkRnd" rel="outHistoryRadioButton" onClick="deleteKeywordInHistory(this,false)" userName="'+item.userName+'" keywordId="'+item.keywordId+'" keywordOutId="'+item.keywordOutId+'" keywordName="'+item.keywordName+'" messageMasterId="'+item.messageMasterId+'" style="float:right;margin-top:38px;">');
			html.push('		</div>');
			html.push('</div>');
		
		});
	}else{
		html.push('<p style="padding:35px">No one has replied to your "'+keywordName+'" ping</p>');
	}
	
	$('[rel="chatScreenOutStep1"]').html(html.join(''));
	$('[rel="outHistoryRadioButton"]').hide();
	
	//body content
	$('[rel="chatScreenOutStep1"]').show();
	$('#addNewkeywordContentOut').hide();
	$('#outContent').hide();
	
	//header
	$('#outHeaderThird').show();
	$('#outHeaderSecond').hide();
	$('#outHeaderFirst').hide();
	//footer
	$('#addNewKeywordOut').hide();
	
	$('#outHistoryDeleteAction').attr('src','images/Trash unselected.png');
	//refresh initial screen load event
	loadRefreshData();

}

function sendCanversationMsg(inChat){
	
	var messageMasterId = "";
	var message = "";
	if(inChat){
		messageMasterId = $('#inChatText').attr('messageMasterId');
		message = $('#inChatText').val();
	}
	else{
		messageMasterId = $('#outChatText').attr('messageMasterId');
		message = $('#outChatText').val();
	}
	
	var url = domainUrl+"/Business-core/message/addConversation";
	var data = {};
	data['userId'] = userId;
	data['messageMasterId'] = messageMasterId;
	data['message'] = message;

	 $.ajax({
		 url: url, 
		 
		 contentType: "application/json; charset=utf-8",
			method: "POST",
			data:JSON.stringify(data),
			dataType: 'json',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			success: function(data) {
				if(data.status){
					appentConversationData(inChat);
					if(inChat)
						$('#inChatText').val("");
					else
						$('#outChatText').val("");
				}
				else
					console.log("No conversation yet.");
			},
			error: function(data) {
				console.log("fails"+data);
			},
			failure: function(errMsg) {
		        console.log(errMsg);
		    }
	 });
}

function appentConversationData(inChat){
	var html = [];
	var messageContent = "";
	if(inChat){
		style = 'inChatSendStyle';
		messageContent = $('#inChatText').val();
	}
	else{
		style = 'inChatSendStyle'//'outChatSendStyle';
		messageContent = $('#outChatText').val();
	}
	
	html.push('<div class="form-group col-xs-12">');
	html.push('		<div class="col-xs-8 '+style+' pull-right">'+messageContent+'</div>');
	html.push('</div>');
	
	if(inChat)
		$('[rel="chatScreenStep2"]').append(html.join(''));
	else
		$('[rel="chatScreenOutStep2"]').append(html.join(''));
}



function backButtonEventStep2(id){
	$('#'+id).show();
	$('#inHeaderFourth').hide();
	$('[rel="chatScreenStep2"]').hide();
	$('[rel="chatScreenStep1"]').show();
	$('[rel="chatscreeninputDic"]').hide();
	
	getReceivingMessageInChat();
	
	if(id == "inHeaderThird")
		$('#keywordHistoryIn').click();
}

function backButtonOutEventStep2(id){
	$('#'+id).show();
	$('#outHeaderFourth').hide();
	$('[rel="chatScreenOutStep2"]').hide();
	$('[rel="chatScreenOutStep1"]').show();
	$('[rel="chatscreeninputOutDic"]').hide();
	
	getReceivingMessageOutChat();
	
	if(id == "outHeaderThird")
		$('#keywordHistoryOut').click();
}

function backButtonEventStep1(){
	
	callUpdateKeywordTrigger();
	
	$('#addNewkeywordContent').hide();
	$('#inContent').show();
	$('#saveNewKeywordButton').hide();
	$('#addNewKeyword').show();
	$('#inHeaderSecond').hide();
	$('#inHeaderFirst').show();
	
	$('[rel="chatScreenStep1"]').hide();
	$("#inHeaderThird").hide();
	
	//refresh initial screen load event
	loadRefreshData();
	
	$('[rel="profileIcon" ]').show();
}

function callUpdateKeywordTrigger(){
	
	if($('#milesHeader').is(':visible')){
		if(parseFloat($('#milesHeader').val()) != parseFloat($('#milesHeader').attr('oldmiles'))){
			saveOrUpdateKeyword(true,$('#milesHeader'));
			getKeywordData();
		}else{
			getKeywordData();
		}
	}
}

function backButtonOutEventStep1(){
	
	//callUpdateOutKeywordTrigger();
	
	$('#addNewkeywordContentOut').hide();
	$('#outContent').show();
	//footer
	$('#saveNewKeywordButtonOut').hide();
	$('#addNewKeywordOut').show();
	//header
	$('#outHeaderFirst').show();
	$('#outHeaderSecond').hide();
	$("#outHeaderThird").hide();
	
	$('[rel="chatScreenOutStep1"]').hide();
	
	getOutKeywordData();
	
	//refresh initial screen load event
	loadRefreshData();
	
	$('[rel="profileIcon" ]').show();
	
	$('#outDeleteAction').attr('src','images/Trash unselected.png');
}

function callUpdateOutKeywordTrigger(){
	
	if($('#milesHeaderOut').is(':visible')){
		if(parseFloat($('#milesHeaderOut').val()) != parseFloat($('#milesHeaderOut').attr('oldmiles'))){
			keywordSubmit(true,$('#milesHeaderOut'));
			//getKeywordData();
		}else{
			//getKeywordData();
		}
	}
}

function addKeyword(isEdit,element){
	var body = buildAddKeywordPopup(isEdit,element);
	bootbox.dialog({
	    title: isEdit?"Update Keyword":"Add Keyword",
	    message: body,
	    buttons: {
	        confirm: {
	            label: '<i class="fa fa-check"></i> Add',
	            className: "btn-success",
	            callback: function() {
	            	if(validateAddNewKeyword()){
	            		saveOrUpdateKeyword(isEdit,element);
	            	}
	            }
	        },
	        cancel: {
	            label: '<i class="fa fa-times"></i> Cancel',
	            className: "btn-default"
	        }
	    },
	    callback: function (result) {
	        console.log('This was logged in the callback: ' + result);
	    }
	    
	});
}

function validateAddNewKeyword(){
	var valid = true;
	if($('#inputInKeyword').val() == "" || $('#inputInKeyword').val() == null){
		valid = false;
	}
	if(valid && ($('#miles').val() == "" || $('#miles').val() == null)){
		valid = false;
	}
	if(30<parseFloat($('#miles').val())){
		valid = false;
	}
	return valid;
	
}

function saveOrUpdateKeyword(isEdit,element){
	var data = {};
	if(isEdit){
		data['userKeywordId'] = $(element).attr('userKeywordId');
		data['keyword'] = $(element).attr('userKeyName');
		data['kilometer'] = $('#milesHeader').val();
	}else{
		data['keyword'] = $('#inputInKeyword').val();
		data['kilometer'] = $('#miles').val();
	}
	data['userId'] = userId;
	
	//var url = "ajaxResponse/getTextkeywordsData.js";
	var url = ""; 
	if(isEdit)
		url = domainUrl+"/Business-core/keyword/update";
	else
		url = domainUrl+"/Business-core/keyword/add";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			if(!isEdit){
				if(data.status){
					buildKeywoards(data.keywordList);
					$('#keywordsContent').click();
				}
				else
					bootbox.alert("Entered Keyword is already available");
			}
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}

function buildAddKeywordPopup(isEdit,element){
	
	var keyWord = '';
	var radius = '';
	if(isEdit){
		userKeywordId = $(element).attr('userKeywordId');
		keyWord = $(element).attr('keyword');
		radius = $(element).attr('radius');
	}
	
	var html = [];
	
	html.push("<form class='HorizontalPositionCache'>");
	html.push("<div class='form-group'>");
	html.push("<label class='col-sm-3 control-label no-padding-right' for='form-field-1'> Enter the Keyword </label>");
	html.push("		<div class='col-sm-9'>");
	html.push("			<input type='text' id='popKeyword' value='"+keyWord+"' placeholder='Enter the Keyword' class='col-xs-10 col-sm-10'>");
	html.push("		</div>");
	html.push("</div>");
	html.push("<div class='form-group'>");
	html.push("		<label class='col-sm-3 control-label no-padding-right' for='form-field-1'> Enter the Radius </label>");
	html.push("		<div class='col-sm-9'>");
	html.push("			<input type='text' id='popRadius' value='"+radius+"' placeholder='Radius' class='col-xs-3 col-sm-3'>&nbsp;km");
	html.push("		</div>");
	html.push("</div>");
	html.push("</form>");
	return html.join("");
}

function deleteKeyword(element,inAction){
	if($(element).prop("checked")){
		var body = 'Are you sure you want to delete "'+$(element).attr('userKeyName')+'" ?';
		bootbox.dialog({
		    title: "Delete Keyword",
		    message: body,
		    buttons: {
		        confirm: {
		            label: '<i class="fa fa-check"></i> Delete',
		            className: "btn-success",
		            callback: function() {
		            	deleteCurrentKeyword(element,inAction);
		            }
		        },
		        cancel: {
		            label: '<i class="fa fa-times"></i> Cancel',
		            className: "btn-default"
		        }
		    },
		    callback: function (result) {
		        console.log('This was logged in the callback: ' + result);
		    }
		    
		});
	}
}

function deleteCurrentKeyword(element,inAction){
	var data = {};
	data['id'] = $(element).attr('userKeywordId');
	data['keyword'] = $(element).attr('userKeyName');
	//data['keyword'] = $('#popKeyword').val();
	data['userId'] = userId;
	
	var url = "";
	if(inAction)
		url = domainUrl+"/Business-core/keyword/delete";
	else
		url = domainUrl+"/Business-core/keyword/deleteOut";
	$.ajax({
		url: url,
		contentType: "application/json; charset=utf-8",
		method: "POST",
		data:JSON.stringify(data),
		dataType: 'json',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		success: function(data) {
			if(inAction)
				buildKeywoards(data);
			else
				buildOutKeywoards(data.outList);//buildOutKeywoards(data);
		},
		error: function(data) {
			console.log("fails"+data);
		},
		failure: function(errMsg) {
	        console.log(errMsg);
	    }
	});
}

function isValidEmailAddress(emailAddress) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return regex.test(emailAddress);
};

function openGallaryOrCameraPopup(){
		//var body = 'Are you sure you want to delete "'+$(element).attr('keywordName')+' - '+$(element).attr('userName')+'" ?';
	bootbox.dialog({
	    title: "Upload Image",
	    message: "Please select camera or Gallery",
	    buttons: {
	        confirm: {
	            label: 'Camera',
	            className: "btn-success",
	            callback: function() {
	            	takePhotoFromCamera();
	            }
	        },
	        cancel: {
	            label: 'Gallery',
	            className: "btn-default",
	            callback: function() {
	            	takePhotoFromLibrary();
	            }
	        }
	    },
	    callback: function (result) {
	        console.log('This was logged in the callback: ' + result);
	    }
	    
	});
}

function onkeypressDescription(element){
	
	//console.log("adasdad"+$(element).val().length);
	var maxrows=4; 
  var txt=element.value;
  var cols=element.cols;

 var arraytxt=txt.split('\n');
  var rows=arraytxt.length; 

 for (i=0;i<arraytxt.length;i++) 
  rows+=parseInt(arraytxt[i].length/cols);

 if (rows>maxrows) element.rows=maxrows;
  else element.rows=rows;

  if(rows == 2)
  {
  	$('#saveNewKeywordOut').css('top','30px');
  }
  else if(rows == 3)
  {
  	$('#saveNewKeywordOut').css('top','45px');
  }
  else if(rows == 4)
  {
  	$('#saveNewKeywordOut').css('top','60px');
  }
  else if(rows == 1)
  {
  	$('#saveNewKeywordOut').css('top','22px');
  }

	$('#keywordDescCount').html(1000-$(element).val().length);
	
}


function loadRefreshData(){
	
	//in screen
	clearInterval(refreshIntervalINScreen);
	if($('[rel="inScreenContent"]').is(':visible')){
		refreshIntervalINScreen = window.setInterval(function(){
			getKeywordData();
		}, 15000);
	}else{
		clearInterval(refreshIntervalINScreen);
	}
	
	//out screen
	clearInterval(refreshIntervalOutScreen);
	if($('[rel="outScreenContent"]').is(':visible')){
		refreshIntervalOutScreen = window.setInterval(function(){
			getOutKeywordData();
		}, 15000);
	}else{
		clearInterval(refreshIntervalOutScreen);
	}
	
	//IN history screen
	clearInterval(refreshIntervalInHistoryScreen);
	if($('[rel="chatScreenStep1"]').is(':visible')){
		refreshIntervalInHistoryScreen = window.setInterval(function(){
			$('#keywordHistoryIn').click();
		}, 15000);
	}else{
		clearInterval(refreshIntervalInHistoryScreen);
	}
	
	//OUT history screen
	clearInterval(refreshIntervalOutHistoryScreen);
	if($('[rel="chatScreenOutStep1"]').is(':visible')){
		refreshIntervalOutHistoryScreen = window.setInterval(function(){
			$('#keywordHistoryOut').click();
		}, 15000);
	}else{
		clearInterval(refreshIntervalOutHistoryScreen);
	}
}

function oninputdescription() {

    	  	var description = $('#messageTextarea').val();
    	  	var tag = $('#outKeyword').val();
    	  	if(description && tag)
    	  	{
 
    	  	
    	  		$('#saveNewKeywordOut').removeClass('sendimg');
    		$('#saveNewKeywordOut').addClass('sendimg_purple');
    		}
    	  	else
    	  	{
    	  		$('#saveNewKeywordOut').addClass('sendimg');
  				$('#saveNewKeywordOut').removeClass('sendimg_purple');
    	  	}	
    	  	
    		
    	}

    	

function oninputinkey() {

	var inputkey = $('#inputInKeyword').val();
	if (inputkey) {
		$('#saveNewKeyword').removeClass('saveimg');
		$('#saveNewKeyword').addClass('saveimg_purple');
	} else {
		$('#saveNewKeyword').addClass('saveimg');
		$('#saveNewKeyword').removeClass('saveimg_purple');
	}

}


function openImageZoomPopup(element){
	//Get the modal
	var modal = document.getElementById('chatImageModal');

	// Get the image and insert it inside the modal - use its "alt" text as a caption
	var img = document.getElementById('chatImageDiv');
	var modalImg = document.getElementById("img01");
	modalImg.src = element.src;
	modal.style.display = "block";

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() { 
	    modal.style.display = "none";
	}
}
